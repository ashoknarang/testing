<?php return array (
  'dimsav/laravel-translatable' => 
  array (
    'providers' => 
    array (
      0 => 'Dimsav\\Translatable\\TranslatableServiceProvider',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'flugger/laravel-responder' => 
  array (
    'providers' => 
    array (
      0 => 'Flugg\\Responder\\ResponderServiceProvider',
    ),
    'aliases' => 
    array (
      'Responder' => 'Flugg\\Responder\\Facades\\Responder',
      'Transformer' => 'Flugg\\Responder\\Facades\\Transformer',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
);