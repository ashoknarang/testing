$(function() {
    $.validator.addMethod("emailRegex",
        function(value, element) {
            return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        },
        "Please enter a valid email address."
    );

    $.validator.addMethod("otherEmailRegex", function(value, emement) {
            if ($('#other_email').val().length > 0) {
                return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
            }
        }, "Please enter a valid other email address."

    );
    jQuery.validator.addMethod("nameRegex", function(value, element) {

        return this.optional(element) || /^[^-\s][a-zA-Z0-9_\s-]+$/i.test(value);
    }, "Special Characters not permitted");

    //    jQuery.validator.addMethod("emailRegex", function(value, element) {
    //     return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);
    // }, "Please enter a valid email address.");

    jQuery.validator.addMethod("mobileRegex", function(value, element) {
        return this.optional(element) || /^((?!(0))[0-9]{10})$/.test(value);
    }, "Please enter a valid mobile number.");

    jQuery.validator.addMethod("titleRegex", function(value, element) {
        var hindicheck = value.split("").filter(function(char) {
            var charCode = char.charCodeAt();
            return charCode >= 2309 && charCode <= 2361;
        }).length > 0;

        if (' ' == value.charAt(0)) {
            hindicheck = false;
        }

        return this.optional(element) || /^[^\s](\w.*)+$/.test(value) || hindicheck;

    }, "First character not space");


});

function capitalizeWord(id) {
    jQuery('#' + id).keyup(function() {
        var str = jQuery('#' + id).val();


        var spart = str.split(" ");
        for (var i = 0; i < spart.length; i++) {
            var j = spart[i].charAt(0).toUpperCase();
            spart[i] = j + spart[i].substr(1);
        }
        jQuery('#' + id).val(spart.join(" "));

    });
}

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});


