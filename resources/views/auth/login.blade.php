@extends('layouts.web.layouts')

@section('content')
 
<br>
      
<br>
<div class="container log_form" >
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @if(!empty($successMsg))
        <div class="alert alert-success"> {{ $successMsg }}</div>
        @endif


      @if (\Session::has('status'))
      <div class="alert alert-success">
      <ul>
      <li>{!! \Session::get('status') !!}</li>
      </ul>
      </div>
      @endif


 
     @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <?php 
       

        
        if(URL::previous() == URL::to("/").'/')
        {
            // Session::put('url.intended',URL::to("/dashboard"));
            Session::put('url.intended',URL::to("/"));
        }
        else
        {
            Session::put('url.intended',URL::previous());
        }
        ?>

            <div class="panel panel-default login-form">
                <div class="panel-heading">{{__("login")}}</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        @if (Request::has('previous'))
                        <input type="hidden" name="previous" value="{{ Request::get('previous') }}">
                        @else
                        <input type="hidden" name="previous" value="{{ URL::previous() }}">
                        @endif

                         
                      
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">{{__("mobile")}}</label>
<input id="email_or_mobile"   class="form-control" onkeypress="validate(event)" pattern="\d*"  maxlength="10"   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"   name="email_or_mobile" value="<?php if(isset($_COOKIE["email_or_mobile"])) { echo $_COOKIE["email_or_mobile"]; } ?>" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">{{__("password")}}</label>




                                <input id="password" type="password" class="form-control" name="password" required value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>" >


                            <div class="col-md-6">
                               
                                <input name="password" type="hidden" id="encode_password"/>    


                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group">
                           
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" <?php if(isset($_COOKIE["email_or_mobile"])) { ?> checked <?php } ?> id="remember"> {{__("remember_me")}}
                                    </label>
                                </div>
                         
                        </div>

                        <div class="form-group">
                            
                                <button type="submit" class="btn btn-success">
                                    {{__("login")}}
                                </button>

                                <a class="btn btn-link" href="{{ url('/otp')}}">
                                    {{__("forgot_your_password")}}
                                </a>
                                
                                
                            
                        </div>
                        <div class="pull-left">   
                            {{__("new_user")}}?<a class="btn btn-link" href="{{ url('user-register')}}">{{__("click_here")}}</a>{{__("to_registration")}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
$(document).ready(function(){
    var loadpass = $('#password').val();
    if(loadpass != '')
    {
        var encodedload = btoa(loadpass);
     $('#encode_password').val(encodedload); 
    }
    $('#password').on('keyup',function(){
     var pass = $(this).val();
     var encoded = btoa(pass);
     $('#encode_password').val(encoded);
    });
});
function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9\b]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}


  function loginCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
      document.getElementById("code").value = response.code;
      document.getElementById("csrf").value = response.state;
      document.getElementById("login_success").submit();
    }
  }
</script>


<script>
  // initialize Account Kit with CSRF protection
  AccountKit_OnInteractive = function(){
    AccountKit.init(
      {
        appId:"{{Config::get('constants.FACEBOOK_APP_ID')}}", 
        state:"{!! csrf_token() !!}", 
        version:"{{Config::get('constants.ACCOUNT_KIT_API_VERSION')}}",
        fbAppEventsEnabled:true,
        redirect:"{{Config::get('constants.REDIRECT_URL')}}",
        debug:true, 
      }
    );
  };

  // login callback
  function loginCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
      var code = response.code;
      var csrf = response.state;
      // Send code to server to exchange for access token
    }
    else if (response.status === "NOT_AUTHENTICATED") {
      // handle authentication failure
    }
    else if (response.status === "BAD_PARAMS") {
      // handle bad parameters
    }
  }

  // phone form submission handler
  function smsLogin() {
    var countryCode = document.getElementById("country_code").value;
    var phoneNumber = document.getElementById("phone_number").value;
    AccountKit.loginCallback(
      'PHONE', 
      {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
      loginCallback
    );
  }


  // email form submission handler
  function emailLogin() {
    var emailAddress = document.getElementById("email").value;
    AccountKit.login(
      'EMAIL',
      {emailAddress: emailAddress},
      loginCallback
    );
  }
</script>
@endsection
