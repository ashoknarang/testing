@extends('layouts.web.layouts')

@section('content')
        <section class="banner-2">
            <div>
                <h1>REGISTER</h1>
                <p>
                    Registering for this site is easy. Just fill in the fields below, and we'll get a new account set up for you in no time.
                </p>
            </div>
        </section>

        <div class="content-ads">
            <div class="container">
                <div class="row content-ads-inner">
                    <div class="col-lg-9 col-md-9 col-sm-12 content">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   <div id="out">
     @if (session()->has('success'))
    <h6><center style="color:green;font-size:18px">{{ session('success') }}</center></h6> @endif 
</div>
<p>Already Registered User? <a href="{{url('login')}}">Click here</a> to login</p>
                        <div class="user-regis">
                            <div class="big-heading heading-1 heading-4">
                                User Registration
                            </div>
                                <form method="post" action="{{url('user_register')}}">
                             {{ csrf_field() }}
                            <div class="row form-row">
                                <div class="col-lg-5 col-md-6  form-field">
                                    <label>Type:</label>
                                    <div>
                                        <div class="col-lg-6 col-sm-6">
                                            <div class="row">
                                            <label class="radio-button" for="post-1">
                                                <input type="radio"  name="role" checked="true" id="post-1" value="3">
                                                <span></span>
                                                <div>Guardian</div>
                                            </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6">
                                            <label class="radio-button" for="post-2">
                                                <input type="radio"  name="role" id="post-2" value="2">
                                                <span></span>
                                                <div>Student</div>
                                            </label>                                                    
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 stu-hs">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-2 form-field">
                                            <label>Title:</label>
                                            <div>
                                                <select name="guardian_title">
                                                    <option value="Mr">Mr.</option>
                                                    <option value="Miss">Miss.</option>
                                                    <option value="Mrs">Mrs.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-sm-10">
                                            <div class="row">
                                                <div class="col-lg-7 col-sm-7 form-field">
                                                    <label>Full Name:</label>
                                                    <div>
                                                        <input type="text" name="   guardian_name" value="{{ old('guardian_firstname')}}">
                                                    </div>
                                                </div>  
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-sm-6 form-field stu-hs">
                                    <label>Mobile Number*:</label>
                                    <div>
                                        <input type="text" name="guardian_mobile"  value="{{ old('guardian_mobile') }} ">
                                    </div>
                                </div>  
                                <div class="col-lg-6  col-sm-6 form-field stu-hs">
                                    <label>Guardian e-mail ID*:</label>
                                    <div>
                                        <input type="text" name="guardian_email" value="{{ old('guardian_email') }}">
                                    </div>
                                </div>
                                
                            </div>
                        </div>




<div class="user-regis">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
                            <div class="big-heading heading-1 heading-4">
                                
                                Student Information
                            </div>
                            <div class="row form-row">
                                
                                

                                
                                        <div class="col-lg-3 col-sm-2 form-field">
                                            <label>Title:</label>
                                            <div>
                                                <select name="title">
                                                    <option value="Mr">Mr.</option>
                                                    <option value="Miss">Miss.</option>
                                                    <option value="Mrs">Mrs.</option>                   </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-sm-10">
                                            <div class="row">
                                                <div class="col-lg-7 col-sm-7 form-field">
                                                    <label>Full Name:</label>
                                                    <div>
                                                        <input type="text" name="name" value="{{ old('name') }}" required>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    

                                <div class="col-lg-6 col-sm-6 form-field">
                                    <label>Mobile Number*:</label>
                                    <div>
                                        <input type="text" name="mobile" value="{{ old('mobile')}}" required >
                                    </div>
                                </div>  
                                <div class="col-lg-6 col-sm-6 form-field">
                                    <label>Email ID*:</label>
                                    <div>
                                        <input type="text" name="email" value="{{ old('email')}}" required >
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 form-field">
                                    <label>Student's Academic status</label>
                                    <div>
                                        <select name="institute_type" id="institute_type">
                                    <option value="1">School</option>
                                    <option value="2">College</option>
                                                </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 form-field" id="school">
                                    <label>Class</label>
                                    <div>
                                        <select >
                            < <option selected="selected" value="1">6</option>
                            <option value="2">7</option>
                              <option value="3">8</option>
                                <option value="4">9</option>
                                  <option value="5">10</option>
                                   <option value="6">11</option>
                                    <option value="7">12</option>
                                    </select>
                                    </div>
                                </div>
                                    <div class="col-lg-6 col-sm-6 form-field" id="college" style="display:none">
                                    <label>Year</label>
                                    <div>
                            <select>
                            <option value="8">1</option>
                              <option value="9">2</option>
                                <option value="10">3</option>
                                  <option value="11">4</option>
                                   <option value="12">5</option>
                                  </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 form-field">
                                    <label>Stream*:</label>
                                    <div>
                                        
                        <?php $streams = Helpers::stream();  ?>         
                        <select name="stream_id" id="stream_id">
                        <option value="0">Please Choose a Stream</option>
                    <?php foreach($streams as $stream):?>
                    <option value="<?=$stream->id?>"><?=$stream->title;?></option>
                    <?php endforeach;?>
                                                </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 form-field">
                                    <label>Specialization*:</label>
                                    <div>
                                                
                                        <select name="category_id" id="category_id">
                                            
                  
                                                </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 form-field">
                                    <label>Country*:</label>
                                    <div>
                                      <?php $countries = Helpers::country();  ?>            
                                        <select name="country_id">
                                             <option value="0">Please choose a country</option>
                    <?php foreach($countries as $country):?>
                    <option value="<?=$country->id?>"><?=$country->name;?></option>
                    <?php endforeach;?>
                                                    
                                        </select>
                                    </div>
                                </div>                              
                                <div class="col-lg-6 col-sm-6 form-field">
                                    <label>Password*:</label>
                                    <div>
                                        <input type="password" name="password" id="password" required>
                                    </div>
                                </div>
                                <div class="col-lg-6  col-sm-6 form-field">
                                    <label>Re-Enter Password*:</label>
                                    <div>
                                        <input type="password" id="confirm_password">
                                    </div>
                                     <span id='divCheckPasswordMatch'></span>
                                </div>
                                
                                <div class="col-lg-12 col-sm-12 form-field">


    <label class="adv-Checkbox" for="post"><input type="checkbox" name="is_accepted_terms" id="post" value="1" class="check-posts" required>&nbsp;&nbsp;<span></span><div>I agree the <a href="#">terms and conditions.</a></div> </label>

                                    
                                </div>
                                <div class="col-lg-12 col-sm-12 form-field ">
                                    <button class="btn-primary" onclick="checkData()">Submit</button>
                                </div>
                            </div>
                        </div>



                    </div>
                <div class="col-lg-3 col-md-3 col-sm-12 ads">
                        <div class="row row-no-gutter">
                            <div class="ads-container clearfix">
                                <a href="#">
                                    <img src="{{asset('frontUI/images/ads-2.png')}}">
                                </a>
                                <a href="#">
                                    <img src="{{asset('frontUI/images/ads-3.png')}}">
                                </a>
                                <a href="#">
                                    <img src="{{asset('frontUI/images/ads-2.png')}}">
                                </a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Content Area and ads section -->
</form>
        <script type="text/javascript">

 function checkData() {
        if(!document.getElementById('post').checked){
            alert('Accept terms And conditions');
            return false;
        }
}
</script>
<script type="text/javascript">
        
        $('input[type=radio][name=role]').change(function(){
            if( this.value == '3' ){
                $('.stu-hs').show();
            }
            else if ( this.value == '2'  ) {
                $('.stu-hs').hide();    
            }
        });
    </script>
    <script type="text/javascript">
        
        $('#institute_type').change(function(){
            if( this.value == '1' ){
                $('#school').show();
                $('#college').hide();
                
            }
            else if ( this.value == '2' ) {
                $('#college').show();
                $('#school').hide();    
                
            }
        });
    </script>
    <script type="text/javascript">

 function checkData() {
        if(!document.getElementById('post').checked){
            alert('Accept terms And conditions');
            return false;
        }
}
</script>
<script type="text/javascript">
$(document).ready(function(){
      $('#stream_id').on('change',function(){
       var  stream_id = $(this).val();
       $.ajax({
            url: APP_URL+'/admin/getcategories',
            method: 'POST',
            data:{stream_id: stream_id,_token: '{!! csrf_token() !!}'},
            success: function(data)
            {
                $('#category_id').html('');
                $('#category_id').append(data);
            }
       }); 
    });
     
});
</script>
@endsection