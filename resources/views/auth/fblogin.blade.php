    @extends('layouts.web.layouts')

@section('content')

    <div class="form">
        <ul class="tab-group">
            <li class="tab active"><a href="#signup">Phone</a></li>
            <li class="tab"><a href="#login">Email</a></li>
        </ul>
        <form action="/login" method="post" id="form">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="code" id="code" />
        </form>
        <div class="tab-content">
            <div id="signup">
                <h1>Account Kit</h1>
                <div>
                    <div class="top-row">
                        <div class="field-wrap">
                            <label>
                                &nbsp; Country Code<span class="req">*</span>
                            </label>
                            <input type="text" id="country" value="+" required autocomplete="off" />
                        </div>
                        <div class="field-wrap">
                            <label>
                                Number<span class="req">*</span>
                            </label>
                            <input type="text" id="phone" required autocomplete="off" />
                        </div>
                    </div>
                    <button onclick="smsLogin()" class="button button-block" />Get Started</button>
                </div>
            </div>
            <div id="login">
                <h1>Account Kit</h1>
                <div>
                    <div class="field-wrap">
                        <label>
                            Email Address<span class="req">*</span>
                        </label>
                        <input type="email" id="email" required autocomplete="off" />
                    </div>
                    <button type="submit" class="button button-block" onclick="emailLogin()"/>Log In</button>
                </div>
            </div>
        </div>
        <!-- tab-content -->
    </div>
    <!-- /form -->
   <script type="text/javascript">
       
       AccountKit_OnInteractive = function() {
  AccountKit.init({
    appId: '262781057775860',
    state: document.getElementById('_token').value,
    version: 'v1.1'
  });
};

function loginCallback(response) {
  console.log(response);

  if (response.status === "PARTIALLY_AUTHENTICATED") {
    document.getElementById('code').value = response.code;
    document.getElementById('_token').value = response.state;
    document.getElementById('form').submit();
  }

  else if (response.status === "NOT_AUTHENTICATED") {
      // handle authentication failure
      alert('You are not Authenticated');
  }
  else if (response.status === "BAD_PARAMS") {
    // handle bad parameters
    alert('wrong inputs');
  }
}

function smsLogin() {
  var countryCode = document.getElementById('country').value;
  var phoneNumber = document.getElementById('phone').value;
  AccountKit.login(
    'PHONE',
    {countryCode: countryCode, phoneNumber: phoneNumber},
    loginCallback
  );
}
// email form submission handler
function emailLogin() {
  var emailAddress = document.getElementById("email").value;
  AccountKit.login('EMAIL', {emailAddress: emailAddress}, loginCallback);
}
   </script>
    <script type="text/javascript" src="https://sdk.accountkit.com/en_US/sdk.js"></script>
  

    @endsection
