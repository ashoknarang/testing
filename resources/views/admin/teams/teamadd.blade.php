@extends('layouts.admin.layouts')
@section('content')
<head>
<link rel="stylesheet" type="text/css" href="custom.css">
</head>
 <div class="content-wrapper">
    <section class="content-header">
        <h1><?= $title;?></h1>
        <?php if((count($errors)>0)){ ?>
        <div class="alert alert-danger">
        <ul>

        <?php 
        foreach ($errors->all('<li>:message</li>') as $message)
        {
        echo $message;
        }
        ?>
        </ul>
        </div>
        <?php  } ?>

       @if(session()->has('warning'))
       <center style="color:green"> {!! session()->get('warning') !!}</center>
       @endif
      
       <ol class="breadcrumb">
       <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
       <li class="active"><?= $title;?></li>
    </section>
     <section class="content">
        <div class="box   box-primary">
           <!--  <div class="box-header with-border">
                <h3 class="box-title">Add Vendor</h3>
            </div> -->

        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
        
          <form  action="{{url('admin/team/add'). ($id!='' ?  '/'.$id : '')}}" method="post" enctype="multipart/form-data" id="myform" autocomplete="off">
                 <input type="hidden" name="id" value="<?=$id?>"/>
             
                  <div class='box-body' >
                        {{ csrf_field() }}                     
                        <div class="row">
                           
                            <div class="form-group col-md-6">
                              <label for="inputState">Team Name*</label>
                              <input type="text" value="<?= isset($_POST['team_name'])  ? $_POST['team_name']  : ( isset($team_details->team_name) ? $team_details->team_name : ''); ?>" class="form-control" id="" name="team_name" placeholder="Enter Team Name" required/>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Club Name*</label>
                              <input type="text" class="form-control" id="email" name="club_name" value="<?= isset($_POST['club_name']) ? $_POST['club_name'] : ( isset($team_details->club_name) ? $team_details->club_name : ''); ?>" placeholder="Club name"/>
                              <span id='error'></span>
                            </div>
                             
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Description*</label>
                              <textarea class="form-control" placeholder="Enter description" id="description" name="description"><?= isset($_POST['description']) ? $_POST['description'] : ( isset($team_details->description) ? $team_details->description : ''); ?></textarea>
                              
                              <span id='error'></span>
                            </div>
                             
                        </div>

                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Status</label>

                                 
                                <select class="form-control select2"  name="status" style="width: 100%;">
                                <?php foreach ($user_status as $key => $value) {
                                    $sel="";


                                    if(isset($team_details->status) &&  $key==$team_details->status){
                                        $sel="selected='selected'";
                                    }

                                    echo "<option value='".$key."' ".$sel.">".$value['title']."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>
                      
                        
                       
                        <div class="row">
                      
                          <div class="form-group col-md-6">
                               
                               
                             
                              
                                  <label for="">Profile image <small>(Size:500x500)</small> </label>
                                  <input type="file" class="form-control size-check-image" id="image" name="image" placeholder="image" />
                                  <div class="image-pic1 rmv{{isset($team_details->id) ? $team_details->id :'0'}}">
                                    <?php  if(isset($team_details->image)) { ?>
                                       <a href="javascript:void(0);" class="btn" onclick="imageRemove('<?php echo $team_details->id ?>','user')" >Reset File</a>
                                       <br>
                                      <img id="image_view" src="<?php echo URL::to( (isset($team_details->image) && $team_details->image!='') ? $team_details->image : '/images/profile/vendors/user_profile_demo.png')?>" height="100px" width="150px" />
                                  <?php } ?>
                                  </div>
                              
                          </div>

                          
                        
                        </div>
                       
                         <!-- fix image size -->
                        <input type="hidden" id="image_size" height="500" width="500">

                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>

                  </div>
            
                </form>
           
        </div>
     </section>

 
</div>

 
<script type="text/javascript">



 $(document).ready(function(){
    
    
    $("#profile_image").change(function() {
     
        readURL(this);
        });
    });
 


 function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.image-pic1').html('<img src="'+e.target.result+'" height="100px" width="150px">');
    }
    reader.readAsDataURL(input.files[0]);
  }
 }



</script>
<script type="text/javascript">
   


$(document).ready(function (){


    
            
    $('#myform').validate({ // initialize the plugin
        rules:{
        }
    });




  </script>
@endsection
