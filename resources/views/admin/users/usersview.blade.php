@extends('layouts.admin.layouts')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
    <center class="message" style="color:green; display:none;"></center>
        <a href="{{url('admin/vendor/list')}}" class="btn btn-info">Back</a>    
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/vendor/list')}}">Vendors List</a></li>
            <li class="active">vendor View</li>
        </ol>
    </section>
    <section class="content">
        <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <div class="profile-pic ">
                            <img src="<?php echo URL::to('/'.$details->profile_image)?>" height="150px" width="150px" class="img-circle" />
                        </div>
                        <h3 class="profile-username text-center"><?php echo $details->name;  ?></h3>
                    </div>
                </div>
              </div>
              <div class="col-md-9">
                <div class="row">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?=$details->name?>'s Details</h3>
                        </div>
                        <div class="box-body">
                            <p><b>Name</b>: <?=ucfirst($details->title)?> <?=ucfirst($details->name)?></p>
                            <p><b>Organization</b>: <?=ucfirst($details->organization_name)?></p>
                            <p><b>Mobile</b>: <?=$details->mobile_1;?></p>
                            <?php if($details->mobile_2):?>
                            <p><b>Another Mobile</b>: <?=$details->mobile_2?></p>
                            <?php endif;?>
                            <p><b>Contact Number</b>: <?=$details->phone_1;?></p>
                            <?php if($details->phone_2):?>
                            <p><b>Another Contact Number</b>: <?=$details->phone_2?></p>
                            <?php endif;?>
                            <p><b>Email</b>: <?=$details->email_1;?></p>
                            <?php if($details->email_2):?>
                            <p><b>Another Email</b>: <?=$details->email_2?></p>
                            <?php endif;?>
                            <p><b>Address Line 1</b>: <?=$details->address_line_1?></p>
                            <p><b>Address Line 2</b>: <?=$details->address_line_2?></p>
                            <p><b>Website</b>: <a href="<?=$details->website?>" target="_blank"><?=$details->website?></a></p>
                            <p><b>Country</b>: <?=$details->country_name?></p>
                            <p><b>State</b>: <?=$details->state_name?></p>
                            <p><b>City</b>: <?=$details->city_name?></p>
                            <?php if($details->status ==1):?>
                            <p><b>Status</b> : <span class="status" style="color: green;">Active</span></p>
                            <?php else:?>
                            <p><b>Status</b> : <span class="status" style="color: red;">Inactive</span></p>
                            <?php endif;?>
                            <p><b>Created At</b>: <?=date('j-M-Y h:i:s A',strtotime($details->created_at))?></p>
                            <p><b>Updated At</b>: <?=date('j-M-Y h:i:s A',strtotime($details->updated_at))?></p>
                             <?php if($details->status ==1):?>
                            <a href="javascript:void(0)" id="btn-0-<?=$details->id?>" class="status-btn btn btn-danger">Inactive</a>
                            <?php else:?>
                            <a href="javascript:void(0);" id="btn-1-<?=$details->id?>" class="status-btn btn btn-success">Active</a>
                            <?php endif;?>
                            <a href="javascript:void(0);" id="vendor-<?=$details->id?>" class="delete_vendor btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
              </div> 
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
 $(document).ready(function(){
    $('.status-btn').on('click',function(){
       var id = $(this).attr('id');
       $.ajax({
             url: APP_URL+'/admin/changevendorstatus',
             method: 'POST',
             data:{id: id,_token: '{!! csrf_token() !!}'},
             success: function(data)
             {
                if(data == "Active")
                {
                   $('.status').text(data);
                   $('.status').css('color', 'green');
                   $('.status-btn').removeClass('btn-success');
                   $('.status-btn').addClass('btn-danger'); 
                   $('.status-btn').text('Inactive');
                }
                if(data == 'Inactive')
                {
                   $('.status').text(data);
                   $('.status').css('color', 'red');
                   $('.status-btn').removeClass('btn-danger'); 
                   $('.status-btn').addClass('btn-success');
                   $('.status-btn').text('Active');
                }
             }
       });
    });
    $('.delete_vendor').on('click',function(){
         var id = $(this).attr('id');
         $.ajax({
            url: APP_URL+'/admin/deletevendorbyid',
             method: 'POST',
             data:{id: id,_token: '{!! csrf_token() !!}'},
             success: function(data)
             {
                if(data == 'Success')
                {
                    $('.message').show();
                   $('.message').html('<b>Vendor Deleted successfully</b>'); 
                   window.setTimeout(function(){
                    window.location.href = APP_URL+"/admin/vendor/list";
                    }, 5000);
                }
             }
         });
    })
 });
</script>
@endsection