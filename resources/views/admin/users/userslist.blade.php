@extends('layouts.admin.layouts')

@section('content')

<link href="{{ asset('public/scripts/jtable/themes/metro/darkgray/jtable.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ asset('public/scripts/jtable/jquery.jtable.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/js/jquery.blockui.min.js')}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>

<div class="content-wrapper">
    <section class="content-header">
        <h1>User List</h1>
        <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li >Users</li>
   
        </ol>
    </section>
     <section class="content-header">
     <div class="row">

            <div class="col-md-12">
                 
                <form action="" method="post" accept-charset="utf-8" class=""> 
                           
                    <div class="row">
                            <div class="form-group">
                                <div class="col-xs-3">

                                  <label for="inputCity">Search</label>
                                  <input type="text" class="form-control input-sm" value="{{isset($_GET['name'])? $_GET['name']: ''}}" id="name" name="name" placeholder="Search">
                                </div>
                                <div class=" col-xs-2">
                                    <label class="" for="status">Status</label>
                                    <select id="status" class="form-control input-sm" name="status" placeholder="status">
                                    <option value="">All</option>
                                    <option value="activate" <?php echo isset($_GET['status']) && $_GET['status']=='activate'? 'selected="selected"': ''  ?>>Active</option>
                                    <option value="deactivate" <?php echo isset($_GET['status']) && $_GET['status']=='deactivate'? 'selected="selected"': ''  ?>>Deactivate</option> 
                                    </select>
                                </div>
                               
                                
                               
                                    
                               
                                <div class="col-xs-4">
                                <br/>
                                <label for="inputCity"></label>
                                <button type="submit" class="btn blue " id="LoadRecordsButton"><i class="fa fa-search"></i> Filter</button>
                                <button type="reset" class="btn default" id="reset_button"><i class="fa fa-refresh"></i> Reset</button>
                                </div>
                            </div>
                    </div> 
                   
                   
                </form>
                    
            </div>
        </div>
        </section>
    <section class="content">
        
        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
       
        <div class="row">


            <div class="col-xs-12">
                <div class="box">
                   <!--  <div class="box-header">
                        <h3 class="box-title">Vendors List</h3>
                    </div> -->
                   
                    <div class="box-body">

                        <div id="PeopleTableContainer"></div>

 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
  
<script type="text/javascript">



$(document).ready(function() {

    var Messages = {
        Delete: {
            Confirm: "Are you sure you want to delete this user?",
            Success: "User has been deleted successfully.",
            Failure: "Unable to delete user, please try again.",
            CanNotDelete: "Can not delete this user.",

        },
        Enable: {
            Confirm: "Are you sure you want to Enable this user?",
            Success: "User has been Enable successfully.",
            Failure: "Unable to Enable user, please try again.",
            CanNotDelete: "Can not Enable this user.",

        },
        Disable: {
            Confirm: "Are you sure you want to Disable this user?",
            Success: "User has been Disable successfully.",
            Failure: "Unable to Disable user, please try again.",
            CanNotDelete: "Can not Disable this user.",

        },
        Approve: {
            Confirm: "Are you sure you want to approve this user? Once approved you will not be able to change the vendor approval status again. ",
            Success: "Approve has been Enable successfully.",
            Failure: "Unable to approve user, please try again.",
            CanNotDelete: "Can not approve this user.",

        },
        Unapprove: {
            Confirm: "Are you sure you want to unapprove this user?",
            Success: "User has been unapprove successfully.",
            Failure: "Unable to unapprove user, please try again.",
            CanNotDelete: "Can not unapprove this user.",

        },
        Enablestatus: {
            Confirm: "Are you sure you want to activate this user?",
            Success: "User has been activated successfully.",
            Failure: "Unable to activate user, please try again.",
            CanNotDelete: "Can not activate this user.",

        },
        DisableStatus: {
            Confirm: "Are you sure you want to Deactivate this user?",
            Success: "User has been Deactivated successfully.",
            Failure: "Unable to Deactivate user, please try again.",
            CanNotDelete: "Can not Deactivate this user.",

        },
    };

    //Prepare DataGrid


 $('#PeopleTableContainer').jtable({
        title: 'Manage Users',
        paging: true,
        pageSize: 10,
        sorting: true,
        defaultSorting: 'id desc',
        pageSizeChangeArea: true,
        gotoPageArea: 'combobox',
        pageSizeChangeLabel: 'Row count',
       
        actions: {
            listAction: APP_ADMIN_URL + '/users/xhr?cmd=list',

        },
        ajaxSettings: {
            data: {

                '_token': '<?php echo csrf_token();?>',

                name:"<?= Request::get('name');?>",
                status:"<?= Request::get('status');?>",
                
                },

            },
        recordsLoaded: function(event, data) {
           

            $("thead tr th:last").css("text-align", "center");
            ///$("a, .badge").tooltip();


            $("a.del").on("click", function() {
                var ele = $(this);

                var user_id = $(ele).data("id");

                if (user_id != undefined && user_id > 0 && user_id != "") {
                    bootbox.dialog({
                        title: "Please Confirm!",
                        message: Messages.Delete.Confirm,
                        buttons: {
                            success: {
                                label: "Yes",
                                className: "btn-danger",
                                callback: function() {

                                    $.ajax({
                                        type: "POST",
                                        url: APP_ADMIN_URL + '/users/xhr?cmd=delete',
                                        data: {
                                            user_id: user_id,
                                            _token: '{!! csrf_token() !!}'
                                        },
                                        dataType: 'JSON',
                                        beforeSend: function() {
                                            $.blockUI();

                                        },
                                        success: function(res) {
                                            if (res.Result == 'Ok') {
                                                    $('#dt').jtable('load', {
                                                    name: $('#name').val(),
                                                    group_id: $('#group_id').val()

                                                    });
                                                 $.unblockUI();
                                                toastr.success(res.Message);
                                                $(ele).closest('tr').fadeOut();

                                            } else {

                                                toastr.error(res.Message);
                                                $(ele).closest('tr').fadeOut();
                                            }

                                        }
                                    });
                                }
                            },
                            danger: {
                                label: "No",
                                className: "btn-default",
                                callback: function() {

                                }
                            },

                        }
                    });


                } else if (user_id == -1) {

                    toastr.error(Messages.Delete.CanNotDelete);
                } else {

                    toastr.error(Messages.Delete.Failure);
                }

            });
              $("a.enable_disable").on("click", function() {
                var ele = $(this).attr('user-data');
                var admincommision = $(this).attr('com_data');
                var cancel_panlty = $(this).attr('cancel_panlty');
                var result=ele.split('_');
               
                var user_id =result[2];
                if($.trim(result[0])=='disable'){
                    var messsage_data=Messages.Disable.Confirm;
                  
                  
                }else if($.trim(result[0])=='enable') {
                  var messsage_data=Messages.Enable.Confirm; 
                  
                }

                if($.trim(result[0])=='unapprove'){
                    var messsage_data=Messages.Unapprove.Confirm;
                  
                  
                }else if($.trim(result[0])=='approve'){

                    if(admincommision != undefined && admincommision==0 || cancel_panlty!= undefined && cancel_panlty ==0)
                    {
                      var messsage_data= "<b>The commission of this vendor is: " +admincommision+ "</b> <br><b>The cancellation panalty of this vendor is: " +cancel_panlty+ "</b><br>Are you sure to submit it. If yes then approved otherwise go to edit option to add the vendor commission.<br>"+Messages.Approve.Confirm; 
                    }
                    else{

                      var messsage_data= Messages.Approve.Confirm; 
                    }
                
                  
                }


                if (user_id != undefined && user_id > 0 && user_id != "") {
                    bootbox.dialog({
                        title: "Please Confirm!",
                        message: messsage_data,
                        buttons: {
                            success: {
                                label: "Yes",
                                className: "btn-danger",
                                callback: function() {

                                    $.ajax({
                                        type: "POST",
                                        url: APP_ADMIN_URL + '/users/xhr?cmd=user_status',
                                        data: {
                                            user_id: user_id,type:result[0],type_val:result[1],_token: '{!! csrf_token() !!}'
                                        },
                                        dataType: 'JSON',
                                        beforeSend: function() {
                                            $.blockUI();

                                        },
                                        success: function(res) {
                                            if (res.Result == 'Ok') {
                                                    $('#PeopleTableContainer').jtable('load');
                                                $.unblockUI();
                                                toastr.success(res.Message);
                                                $(ele).closest('tr').fadeOut();

                                            } else {

                                                toastr.error(res.Message);
                                                $(ele).closest('tr').fadeOut();
                                            }

                                        }
                                    });
                                }
                            },
                            danger: {
                                label: "No",
                                className: "btn-default",
                                callback: function() {

                                }
                            },

                        }
                    });


                } else if (user_id == -1) {

                    toastr.error(Messages.Delete.CanNotDelete);
                } else {

                    toastr.error(Messages.Delete.Failure);
                }

            });

        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: true,
                sorting: false,
                
                title: '#ID',
                width: '5%'
            },

            // badge: {
            //     title: 'User Type',
            //     width: '5%',
            //     display: function(data) {
            //         var badge = "";
            //         switch (data.record.group_id) {
            //             case "1":
            //                 badge = '<span class="badge badge-danger badge-roundless" title="Super Admin">S</span>';
            //                 break;
            //             case "2":
            //                 badge = '<span class="badge badge-success badge-roundless" title="Administrator">A</span>';
            //                 break;
            //             case "3":
            //                 badge = '<span class="badge badge-warning badge-roundless" title="Sugar Baby">SB</span>';
            //                 break;
            //             case "4":
            //                 badge = '<span class="badge badge-default badge-roundless" title="Sugar Daddy">SD</span>';
            //                 break;
            //         }
            //         return badge; //+" "+data.record.first_name+" "+data.record.last_name;
            //     },
            //     sorting: false,
            // },

            name: {
                title: 'Name',
                width: '10%',
                
            },

            email: {
                title: 'email',
                width: '10%'
            },
          
           
              actions: {
                title: 'Action',
                sorting: false,
                width: '5%',
                display: function(data) {
                     var act='';
                    act+='<center><div class="btn-group" >' +
                        '<a href="' + APP_ADMIN_URL + '/user/add/' + data.record.id + '" class="btn btn-xs  btn-default" title="Edit">' +
                        '<i class="fa fa-edit"></i></a>';

             
                        act+='<a href="javascript:;" class="btn btn-xs  btn-default red del" title="Delete" data-id="' + ((data.record.group_id == 1) ? '-1' : data.record.id) + '">' +'<i class="fa fa-trash "></i></a>';
                        
                        act+='</div></center>';
                         return act;
                }

            }
        }
    });

    //Load data from server
    $('#PeopleTableContainer').jtable('load');
    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButton').click(function(e) {
        e.preventDefault();
        // $('#PeopleTableContainer').jtable('load', {
        //     name: $('#name').val(),
        //     status: $('#status').val(),
        //     country_id: $('#country_id  ').val(),
        //     state_id: $('#state_id').val(),
        //     city_id: $('#city_id').val(),
            
        //     _token: '{!! csrf_token() !!}'
        // });
        var params = { 'name':$('#name').val(), 'status':$('#status').val()};
        // console.log(params);
         
        // history.pushState(params, "page 2", "http://localhost/chatri/admin/analytics/courseviewanalytics?");

        var new_url = APP_URL+'/admin/users/?' + jQuery.param(params);

       window.location=new_url;
    });

    $('#reset_button').click(function(e) {
        //$('#PeopleTableContainer').jtable('load');
        window.location=APP_URL+'/admin/users';
    });

    

});

 </script>


<script type="text/javascript">
$(document).ready(function(){
   $('.status-btn').on('click',function(){
       var id = $(this).attr('id');
       $.ajax({
             url: APP_URL+'/admin/changevendorstatus',
             method: 'POST',
             data:{id: id,_token: '{!! csrf_token() !!}'},
             success: function(data)
             {
                if(data == "Active")
                {
                   $('#'+id).text(data);
                   $('#'+id).css('color', 'green');
                   
                }
                if(data == 'Inactive')
                {
                   $('#'+id).text(data);
                   $('#'+id).css('color', 'red');
                   
                }
             }
       });
    }); 
    $('.remove_vendor').on('click',function(){
       var row_id = $(this).parent().parent().attr('id');
       if (confirm('Are you sure you want to delete this?')) {
       $.ajax({
            url: APP_URL+'/admin/deletevendorbyid',
             method: 'POST',
             data:{id: row_id,_token: '{!! csrf_token() !!}'},
             success: function(data)
             {
                if(data == 'Success')
                {
                   
                    $('#'+row_id).remove();
                }
             }
         });
         }
    });
    
});
</script>
@endsection