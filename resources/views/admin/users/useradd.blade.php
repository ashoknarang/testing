@extends('layouts.admin.layouts')
@section('content')
<head>
<link rel="stylesheet" type="text/css" href="custom.css">
</head>
 <div class="content-wrapper">
    <section class="content-header">
        <h1><?= $title;?></h1>
        <?php if((count($errors)>0)){ ?>
        <div class="alert alert-danger">
        <ul>

        <?php 
        foreach ($errors->all('<li>:message</li>') as $message)
        {
        echo $message;
        }
        ?>
        </ul>
        </div>
        <?php  } ?>

       @if(session()->has('warning'))
       <center style="color:green"> {!! session()->get('warning') !!}</center>
       @endif
      
       <ol class="breadcrumb">
       <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
       <li class="active"><?= $title;?></li>
    </section>
     <section class="content">
        <div class="box   box-primary">
           <!--  <div class="box-header with-border">
                <h3 class="box-title">Add Vendor</h3>
            </div> -->

        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
        
          <form  action="{{url('admin/user/add'). ($id!='' ?  '/'.$id : '')}}" method="post" enctype="multipart/form-data" id="myform" autocomplete="off">
                 <input type="hidden" name="id" value="<?=$id?>"/>
                 <input type="hidden" name="self" value="{{ old(
                 (Request::post('self')) ? Request::post('self') :'' , $self) }}"/>
                 
                  <div class='box-body' >
                        {{ csrf_field() }}                     
                        <div class="row">
                            <div class="form-group col-md-2">
                              <label for="inputCity">Title</label>
                                <select class="form-control select2" name="title" style="width: 50%;">
                                    <option value="mr"  <?=  isset($_POST['title']) && $_POST['title']=="mr" ? "selected='selected'"   :   (isset($user_details->title) && strtolower($user_details->title) =='mr' ? "selected='selected'"  : ''); ?> selected="selected">Mr</option>
                                    <option  <?=  isset($_POST['title']) && $_POST['title']=="miss" ? "selected='selected'"   :   (isset($user_details->title) && strtolower($user_details->title) =='miss' ? "selected='selected'"  : ''); ?>  value="miss">Miss.</option>
                                    <option  <?=  isset($_POST['title']) && $_POST['title']=="mrs" ? "selected='selected'"   :   (isset($user_details->title) && strtolower($user_details->title) =='mrs' ? "selected='selected'"  : ''); ?>  value="mrs">Mrs.</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                              <label for="inputState">Name*</label>
                              <input type="text" value="<?= isset($_POST['name'])  ? $_POST['name']  : ( isset($user_details->name) ? $user_details->name : ''); ?>" class="form-control" id="" name="name" placeholder="Enter Name" required/>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="inputCity">Email*</label>
                              <input type="text" class="form-control" id="email" name="email" value="<?= isset($_POST['email']) ? $_POST['email'] : ( isset($user_details->email) ? $user_details->email : ''); ?>" placeholder="Email"/>
                              <span id='error'></span>
                            </div>
                             
                        </div>


                        <div class="row">
                                <?php if(($id=='' || $id=="0" || $id==0) &&  $type!="trainers" && $type!=Config::get('constants.USER_TYPE_TRAINER') ){?>
                                <div class="form-group col-md-6">

                                <label for="exampleInputEmail1">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">

                                </div>
                                <?php } ?>
                                 
                          
                        </div>
                        
                       
                        <div class="row">
                      
                          <div class="form-group col-md-6">
                               
                               
                             
                              
                                  <label for="">Profile image <small>(Size:500x500)</small> </label>
                                  <input type="file" class="form-control size-check-image" id="profile_image" name="profile_image" placeholder="profile image" />
                                  <div class="image-pic1 rmv{{isset($user_details->id) ? $user_details->id :'0'}}">
                                    <?php  if(isset($user_details->image)) { ?>
                                       <a href="javascript:void(0);" class="btn" onclick="imageRemove('<?php echo $user_details->id ?>','user')" >Reset File</a>
                                       <br>
                                      <img id="image_view" src="<?php echo URL::to( (isset($user_details->image) && $user_details->image!='') ? $user_details->image : '/images/profile/vendors/user_profile_demo.png')?>" height="100px" width="150px" />
                                  <?php } ?>
                                  </div>
                              
                          </div>

                           <div class="form-group col-md-3">
                                <label for="">Status</label>

                                 
                                <select class="form-control select2"  name="status" style="width: 100%;">
                                <?php foreach ($user_status as $key => $value) {
                                    $sel="";


                                    if(isset($user_details->status) &&  $key==$user_details->status){
                                        $sel="selected='selected'";
                                    }elseif(!isset($user_details) &&  $key==Config::get('constants.COMMON_STATUS_ACTIVE')){
                                        $sel="selected='selected'";
                                    }

                                    echo "<option value='".$key."' ".$sel.">".$value['title']."</option>";
                                } ?>

                                </select>
                            </div>
                        
                        </div>
                       
                         <!-- fix image size -->
                        <input type="hidden" id="image_size" height="500" width="500">

                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>

                  </div>
            
                </form>
           
        </div>
     </section>

 
</div>

 
<script type="text/javascript">



 $(document).ready(function(){
    
    
    $("#profile_image").change(function() {
     
        readURL(this);
        });
    });
 


 function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.image-pic1').html('<img src="'+e.target.result+'" height="100px" width="150px">');
    }
    reader.readAsDataURL(input.files[0]);
  }
 }



</script>
<script type="text/javascript">
   


$('#institute_type').change(function(){
    if( this.value == '1' ){

        //$('#stream').hide();
        //$('#category').hide();
        $('#school').show();
        $('#college-lable').hide();
        $('#college_hide').hide();
        $("#institute_id_div").hide();
        $('#school-lable').show();
        
    }
    else if ( this.value == '2' ) {
        $('#stream').show();
        $('#category').show();
        $('#school').hide();
        $('#school-lable').hide();
        $('#college_hide').show();
        $("#institute_id_div").show();
        $('#college-lable').show(); 
        
    }
    getStreamByInstitueType();
});

 <?php if((isset($user_details->country_id) && $user_details->country_id) || Request::post('country_id')){
 ?>

 $("#country_id").trigger("change");
 $("#state_id").trigger("change");
 $("#institute_type").trigger("change");
<?php } ?>

$(document).ready(function (){


    
            
    $('#myform').validate({ // initialize the plugin
        rules:{
            mobile:{
                required: true,
                number: true,
                minlength:10,
                nowhitespace:true,
                mobileRegex:true,
            },

            organization_name:{
                required: true,
                minlength: 2,
            },
            name:{
                required: true,
                minlength: 2,
                maxlength: 25,
                nameRegex: true,
                
            },
             email:{
               email:true,
               required: true,
               nowhitespace:true,
               emailRegex:true,
            },

            other_email:{
                   email:true,
                   nowhitespace:true,
                  
                   //otherEmailRegex:true,
            },
            student_name:{
               //required: true,
                minlength: 2,
                maxlength: 25,
                nowhitespace:true,
                nameRegex: true,
            },
            other_mobile:{
                number: true,
                minlength:10,
                nowhitespace:true,
                mobileRegex:true,
            },
            year_of_experience:{
                number: true,
                range: [0, 100],
            },
            country_id:{
                required: true,
            },
            state_id:{
                required: true,
            },
            city_id:{
                required: true,
            },
            college_id:{
                required: true, 
            },
           /* address_line_1:{
               required: true,
            },*/
             password:{
                required: true,
                minlength: 4,
                maxlength: 8,
                nowhitespace:true,
            },
            admin_commision:{
                integer: true,  
                 range: [0, 100],
            },
            cancellation_panalty:{
                integer: true,  
                 range: [0, 100],
            }
        }
    });

$('#institute_type').change(function(){
			if( this.value == '1' ){
				$('#stream').hide();
				$('#category').hide();
				$('#school').show();
				$('#college-lable').hide();
				$('#college_hide').hide();
				$('#school-lable').show();
				$('#college-lable').hide();

			}
			else if ( this.value == '2' ) {
				$('#stream').show();
				$('#category').show();
				$('#school').hide();
                $('#school-lable').hide();
				$('#college_hide').show();
				$('#college-lable').show();	
				
			}
		});
        $('#institute_type').on('change',function(){
       var institute_type = $(this).val();
       $.ajax({
            url: APP_URL+'/admin/getsinstitute',
            method: 'POST',
            data:{institute_type: institute_type,_token: '{!! csrf_token() !!}'},
            success: function(data)
            {
                $('#institute').html('');
                $('#institute').append(data);
            }
       }); 
    }); 
 });
 
function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}



  </script>
@endsection
