@extends('layouts.admin.layouts')
@section('content')

 <div class="content-wrapper">
    <section class="content-header">
        <h1><?= $title;?></h1>
        <?php if((count($errors)>0)){ ?>
        <div class="alert alert-danger">
        <ul>

        <?php 
        foreach ($errors->all('<li>:message</li>') as $message)
        {
        echo $message;
        }
        ?>
        </ul>
        </div>
        <?php  } ?>

       @if(session()->has('warning'))
       <center style="color:green"> {!! session()->get('warning') !!}</center>
       @endif
      
       <ol class="breadcrumb">
       <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
       <li class="active"><?= $title;?></li>
    </section>
     <section class="content">
        <div class="box   box-primary">
           <!--  <div class="box-header with-border">
                <h3 class="box-title">Add Vendor</h3>
            </div> -->

        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
        
          <form  action="{{url('admin/matches/add'). ($id!='' ?  '/'.$id : '')}}" method="post" enctype="multipart/form-data" id="myform" autocomplete="off">
                 <input type="hidden" name="id" value="<?=$id?>"/>
             
                  <div class='box-body' >
                        {{ csrf_field() }}                     
                      <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Series</label>

                                 
                                <select class="form-control"  name="series_id" style="width: 100%;" required>
                                  <option>Select Series</option>
                                <?php foreach ($series as  $ser) {
                                    $sel_ser="";


                                    if(isset($match_details) &&  $ser->id==$match_details->series_id){
                                        $sel_ser="selected='selected'";
                                    }

                                    echo "<option value='".$ser->id."' ".$sel_ser.">".$ser->series_title."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>
                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Team 1</label>

                                 
                                <select class="form-control"  name="team1_id" style="width: 100%;" required>
                                  <option>Select Team1</option>
                                <?php foreach ($teams as  $team1) {
                                    $sel="";


                                    if(isset($match_details) &&  $team1->id==$match_details->team1_id){
                                        $sel="selected='selected'";
                                    }

                                    echo "<option value='".$team1->id."' ".$sel.">".$team1->team_name."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>
                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Team 2</label>

                                 
                                <select class="form-control"  name="team2_id" style="width: 100%;" required>
                                  <option>Select Team2</option>
                                <?php foreach ($teams as  $team2) {
                                    $sel="";


                                    if(isset($match_details) &&  $team2->id==$match_details->team2_id){
                                        $sel="selected='selected'";
                                    }

                                    echo "<option value='".$team2->id."' ".$sel.">".$team2->team_name."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>
                                              
                                 

                        <div class="row">
                          <div class="col-md-3">
                             <div class="form-group">
                                <label for="Timings">Date</label>
                                <input type="text" class="form-control input-sm" value="<?= isset($_POST['match_datetime'])  ? $_POST['match_datetime']  : ( isset($match_details->match_datetime) ? date("Y-m-d",strtotime($match_details->match_datetime))  : ''); ?>"  id="fromDate" name="match_date" placeholder="Select From Date"/>
                            
                            </div>
                          </div>
                          <div class="col-md-3">
                             <div class="form-group">
                                <label for="Timings">Time</label>
                                 <input id="timepicker"   type="text" class="form-control input-small timepicker required" name="match_time" value="<?= isset($_POST['match_datetime'])  ? $_POST['match_datetime']  : ( isset($match_details->match_datetime) ? date("H:i",strtotime($match_details->match_datetime))  : ''); ?>" placeholder="Start time">
                                
                            
                            </div>
                          </div>

                         
                      </div>


                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Status</label>

                                 
                                <select class="form-control select2"  name="status" style="width: 100%;">
                                <?php foreach ($series_status as $key => $value) {
                                    $sel="";


                                    if(isset($match_details->status) &&  $key==$match_details->status){
                                        $sel="selected='selected'";
                                    }
                                    echo "<option value='".$key."' ".$sel.">".$value['title']."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>
                      
                        
                       
                       
                       
                       
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>

                  </div>
            
                </form>
           
        </div>
     </section>

 
</div>

 
<script type="text/javascript">


$(document).ready(function(){

  $('#fromDate').datepicker({
    format: 'yyyy-mm-dd',
    //endDate: '+0d',
    autoclose: true

  });

  $("#timepicker").timepicker({

        minuteStep: 5,
        showInputs: false,
        disableFocus: true,

        showMeridian:false,
        format: 'hh:mm:ss',
    });
        

        var dayString ='<?php echo date("Y-m-d") ?>';
          console.log(dayString);
          //var dayWrapper = moment(day); 
         //// var dayString = day.format("YYYY-MM-DD"); 


          $('#start_date').val(dayString);
          $('#end_date').val(dayString);

      @if(isset($series_details->start_date))
        $('#datetimes').daterangepicker({
        startDate: moment("<?=$series_details->start_date?>").format('YYYY-MM-DD'),
        endDate: moment("<?=$series_details->end_date?>").format('YYYY-MM-DD'),
        showDropdowns: false,
        timePicker: true,
        autoUpdateInput: true,
        locale: {
          format: 'YYYY-MM-DD'
        },
        });
      @else
        $('input[name="datetimes"]').daterangepicker({
        startDate: new Date(),
        showDropdowns: false,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 10,
        autoUpdateInput: true,
        locale: {
          format: 'YYYY-MM-DD'
        },
        });
      @endif

      $('#datetimes').on('apply.daterangepicker', function(ev, picker){ 
        $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
        $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));

      }).on('hide.daterangepicker', function(ev, picker){ 
                $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
                $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));

               
      }).on('show.daterangepicker', function(ev, picker){ 
                $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
                $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));

               
      }).on('hideCalendar.daterangepicker', function(ev, picker){ 
        $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
        $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));
       
      });

});


 $(document).ready(function(){
    
    
    $("#profile_image").change(function() {
     
        readURL(this);
        });
    });
 


 function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.image-pic1').html('<img src="'+e.target.result+'" height="100px" width="150px">');
    }
    reader.readAsDataURL(input.files[0]);
  }
 }



</script>
@endsection
