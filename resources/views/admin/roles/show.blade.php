@extends('layouts.admin.layouts')


@section('content')

 <div class="content-wrapper">
    <section class="content-header">
       
        <div id="out">
        @if (session()->has('success'))
            <h6><center>{{ session('success') }}</center></h6> 

        @endif
        </div>
        
    </section>
    <section class="content" >
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Role</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="index"> Back</a>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            <?php isset($role->name) ? $role->name :''; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Permissions:</strong>
            @if(!empty($rolePermissions))
                @foreach($rolePermissions as $v)
                    <label class="label label-success">{{ $v->name }},</label>
                @endforeach
            @endif
        </div>
    </div>
</div>
</section>
</div>
@endsection