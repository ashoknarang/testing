@extends('layouts.admin.layouts')

@section('content')
<link href="{{ asset('public/scripts/jtable/themes/metro/darkgray/jtable.css')}}" rel="stylesheet" type="text/css" />

 
<!-- <script src="{{ asset('public/scripts/jquery-ui-1.8.16.custom.min.js')}}" type="text/javascript"></script> -->
<script src="{{ asset('public/scripts/jtable/jquery.jtable.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/js/jquery.blockui.min.js')}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Series List</h1>
        <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li >Series</li>
   
        </ol>
    </section>
     <section class="content-header">
     <div class="row">

            <div class="col-md-12">
                 
                <form action="" method="post" accept-charset="utf-8" class=""> 
                           
                    <div class="row">
                            <div class="form-group">
                                <div class="col-xs-3">

                                  <label for="inputCity">Search</label>
                                  <input type="text" class="form-control input-sm" value="{{isset($_GET['name'])? $_GET['name']: ''}}" id="name" name="name" placeholder="Search">
                                </div>
                                <div class=" col-xs-2">
                                    <label class="" for="status">Status</label>
                                    <select id="status" class="form-control input-sm" name="status" placeholder="status">
                                    <option value="">All</option>
                                    <option value="activate" <?php echo isset($_GET['status']) && $_GET['status']=='activate'? 'selected="selected"': ''  ?>>Active</option>
                                    <option value="deactivate" <?php echo isset($_GET['status']) && $_GET['status']=='deactivate'? 'selected="selected"': ''  ?>>Deactivate</option> 
                                    </select>
                                </div>
                               
                                
                               
                                    
                               
                                <div class="col-xs-4">
                                <br/>
                                <label for="inputCity"></label>
                                <button type="submit" class="btn blue " id="LoadRecordsButton"><i class="fa fa-search"></i> Filter</button>
                                <button type="reset" class="btn default" id="reset_button"><i class="fa fa-refresh"></i> Reset</button>
                                </div>
                            </div>
                    </div> 
                      <a href="<?php echo url('admin/series/add/'); ?> " class="btn blue pull-right"><i class="fa fa-plus"></i> Add Series</a>
                   
                </form>
                    
            </div>
        </div>
        </section>
    <section class="content">
        
        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
       
        <div class="row">


            <div class="col-xs-12">
                <div class="box">
                   <!--  <div class="box-header">
                        <h3 class="box-title">Vendors List</h3>
                    </div> -->
                   
                    <div class="box-body">

                        <div id="PeopleTableContainer"></div>

 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
  
<script type="text/javascript">



$(document).ready(function() {

    var Messages = {
        Delete: {
            Confirm: "Are you sure you want to delete this series?",
            Success: "Series has been deleted successfully.",
            Failure: "Unable to delete series, please try again.",
            CanNotDelete: "Can not delete this series.",

        },
    };

    //Prepare DataGrid


 $('#PeopleTableContainer').jtable({
        title: 'Manage Series',
        paging: true,
        pageSize: 10,
        sorting: true,
        defaultSorting: 'id desc',
        pageSizeChangeArea: true,
        gotoPageArea: 'combobox',
        pageSizeChangeLabel: 'Row count',
       
        actions: {
            listAction: APP_ADMIN_URL + '/series/xhr?cmd=list',

        },
        ajaxSettings: {
            data: {

                '_token': '<?php echo csrf_token();?>',

                name:"<?= Request::get('name');?>",
                status:"<?= Request::get('status');?>",
                
                },

            },
        recordsLoaded: function(event, data) {
           

            $("thead tr th:last").css("text-align", "center");
            ///$("a, .badge").tooltip();


            $("a.del").on("click", function() {
                var ele = $(this);

                var series_id = $(ele).data("id");

                if (series_id != undefined && series_id > 0 && series_id != "") {
                    bootbox.dialog({
                        title: "Please Confirm!",
                        message: Messages.Delete.Confirm,
                        buttons: {
                            success: {
                                label: "Yes",
                                className: "btn-danger",
                                callback: function() {

                                    $.ajax({
                                        type: "POST",
                                        url: APP_ADMIN_URL + '/series/xhr?cmd=delete',
                                        data: {
                                            series_id: series_id,
                                            _token: '{!! csrf_token() !!}'
                                        },
                                        dataType: 'JSON',
                                        beforeSend: function() {
                                            $.blockUI();

                                        },
                                        success: function(res) {
                                            if (res.Result == 'Ok') {
                                                    $('#dt').jtable('load', {
                                                    name: $('#name').val(),
                                                    series_id: $('#series_id').val()

                                                    });
                                                 $.unblockUI();
                                                toastr.success(res.Message);
                                                $(ele).closest('tr').fadeOut();

                                            } else {

                                                toastr.error(res.Message);
                                                $(ele).closest('tr').fadeOut();
                                            }

                                        }
                                    });
                                }
                            },
                            danger: {
                                label: "No",
                                className: "btn-default",
                                callback: function() {

                                }
                            },

                        }
                    });


                } else if (user_id == -1) {

                    toastr.error(Messages.Delete.CanNotDelete);
                } else {

                    toastr.error(Messages.Delete.Failure);
                }

            });
           

        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: true,
                sorting: false,
                
                title: '#ID',
                width: '5%'
            },

            // badge: {
            //     title: 'User Type',
            //     width: '5%',
            //     display: function(data) {
            //         var badge = "";
            //         switch (data.record.group_id) {
            //             case "1":
            //                 badge = '<span class="badge badge-danger badge-roundless" title="Super Admin">S</span>';
            //                 break;
            //             case "2":
            //                 badge = '<span class="badge badge-success badge-roundless" title="Administrator">A</span>';
            //                 break;
            //             case "3":
            //                 badge = '<span class="badge badge-warning badge-roundless" title="Sugar Baby">SB</span>';
            //                 break;
            //             case "4":
            //                 badge = '<span class="badge badge-default badge-roundless" title="Sugar Daddy">SD</span>';
            //                 break;
            //         }
            //         return badge; //+" "+data.record.first_name+" "+data.record.last_name;
            //     },
            //     sorting: false,
            // },

            series_title: {
                title: 'Series Title',
                width: '10%',
                
            },

            start_date: {
                title: 'Start Date',
                width: '10%'
            },

            end_date: {
                title: 'End Date',
                width: '10%'
            },
          
           
              actions: {
                title: 'Action',
                sorting: false,
                width: '5%',
                display: function(data) {
                     var act='';
                    act+='<center><div class="btn-group" >' +
                        '<a href="' + APP_ADMIN_URL + '/series/add/' + data.record.id + '" class="btn btn-xs  btn-default" title="Edit">' +
                        '<i class="fa fa-edit"></i></a>';

             
                        act+='<a href="javascript:;" class="btn btn-xs  btn-default red del" title="Delete" data-id="' +  data.record.id + '">' +'<i class="fa fa-trash "></i></a>';
                        
                        act+='</div></center>';
                         return act;
                }

            }
        }
    });

    //Load data from server
    $('#PeopleTableContainer').jtable('load');
    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButton').click(function(e) {
        e.preventDefault();
        // $('#PeopleTableContainer').jtable('load', {
        //     name: $('#name').val(),
        //     status: $('#status').val(),
        //     country_id: $('#country_id  ').val(),
        //     state_id: $('#state_id').val(),
        //     city_id: $('#city_id').val(),
            
        //     _token: '{!! csrf_token() !!}'
        // });
        var params = { 'name':$('#name').val(), 'status':$('#status').val()};
        // console.log(params);
         
        // history.pushState(params, "page 2", "http://localhost/chatri/admin/analytics/courseviewanalytics?");

        var new_url = APP_URL+'/admin/series/?' + jQuery.param(params);

       window.location=new_url;
    });

    $('#reset_button').click(function(e) {
        //$('#PeopleTableContainer').jtable('load');
        window.location=APP_URL+'/admin/series';
    });

    

});

 </script>


@endsection