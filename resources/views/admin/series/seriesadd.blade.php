@extends('layouts.admin.layouts')
@section('content')
 <div class="content-wrapper">
    <section class="content-header">
        <h1><?= $title;?></h1>
        <?php if((count($errors)>0)){ ?>
        <div class="alert alert-danger">
        <ul>

        <?php 
        foreach ($errors->all('<li>:message</li>') as $message)
        {
        echo $message;
        }
        ?>
        </ul>
        </div>
        <?php  } ?>

       @if(session()->has('warning'))
       <center style="color:green"> {!! session()->get('warning') !!}</center>
       @endif
      
       <ol class="breadcrumb">
       <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
       <li class="active"><?= $title;?></li>
    </section>
     <section class="content">
        <div class="box   box-primary">
           <!--  <div class="box-header with-border">
                <h3 class="box-title">Add Vendor</h3>
            </div> -->

        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
        
          <form  action="{{url('admin/series/add'). ($id!='' ?  '/'.$id : '')}}" method="post" enctype="multipart/form-data" id="myform" autocomplete="off">
                 <input type="hidden" name="id" value="<?=$id?>"/>
             
                  <div class='box-body' >
                        {{ csrf_field() }}                     
                        <div class="row">
                           
                            <div class="form-group col-md-6">
                              <label for="inputState">Series Name*</label>
                              <input type="text" value="<?= isset($_POST['series_title'])  ? $_POST['series_title']  : ( isset($series_details->series_title) ? $series_details->series_title : ''); ?>" class="form-control" id="" name="series_title" placeholder="Enter Series Title" required/>
                            </div>

                        </div>

  
                                 

                        <div class="row">
                          <div class="col-md-6">
                             <div class="form-group">
                                <label for="Timings">Date</label>
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' id="datetimes" name="datetimes"  class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    <input type="hidden" id="start_date"  value=" @isset($data)<?=$data->start_date?> @endisset" name="start_date">
                                    <input type="hidden" id="end_date" value="@isset($data)<?=$data->end_date?> @endisset" name="end_date">
                                    
                                </div>
                            </div>
                          </div>

                         
                      </div>


                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Status</label>

                                 
                                <select class="form-control select2"  name="status" style="width: 100%;">
                                <?php foreach ($series_status as $key => $value) {
                                    $sel="";


                                    if(isset($series_details->status) &&  $key==$series_details->status){
                                        $sel="selected='selected'";
                                    }
                                    echo "<option value='".$key."' ".$sel.">".$value['title']."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>
                      
                        
                       
                       
                       
                       
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>

                  </div>
            
                </form>
           
        </div>
     </section>

 
</div>

 
<script type="text/javascript">

$(document).ready(function(){

      

        var dayString ='<?php echo date("Y-m-d") ?>';
          console.log(dayString);
          //var dayWrapper = moment(day); 
         //// var dayString = day.format("YYYY-MM-DD"); 


          $('#start_date').val(dayString);
          $('#end_date').val(dayString);

      @if(isset($series_details->start_date))
        $('#datetimes').daterangepicker({
        startDate: moment("<?=$series_details->start_date?>").format('YYYY-MM-DD'),
        endDate: moment("<?=$series_details->end_date?>").format('YYYY-MM-DD'),
        showDropdowns: false,
        timePicker: true,
        autoUpdateInput: true,
        locale: {
          format: 'YYYY-MM-DD'
        },
        });
      @else
        $('input[name="datetimes"]').daterangepicker({
        startDate: new Date(),
        showDropdowns: false,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 10,
        autoUpdateInput: true,
        locale: {
          format: 'YYYY-MM-DD'
        },
        });
      @endif

      $('#datetimes').on('apply.daterangepicker', function(ev, picker){ 
        $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
        $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));

      }).on('hide.daterangepicker', function(ev, picker){ 
                $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
                $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));

               
      }).on('show.daterangepicker', function(ev, picker){ 
                $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
                $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));

               
      }).on('hideCalendar.daterangepicker', function(ev, picker){ 
        $('#start_date').val(picker.startDate.format('YYYY-MM-DD'));
        $('#end_date').val(picker.endDate.format('YYYY-MM-DD'));
       
      });

});


 $(document).ready(function(){
    
    
    $("#profile_image").change(function() {
     
        readURL(this);
        });
    });
 


 function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.image-pic1').html('<img src="'+e.target.result+'" height="100px" width="150px">');
    }
    reader.readAsDataURL(input.files[0]);
  }
 }



</script>
<script type="text/javascript">
   




  </script>
@endsection
