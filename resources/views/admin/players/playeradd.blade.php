@extends('layouts.admin.layouts')
@section('content')
<head>
<link rel="stylesheet" type="text/css" href="custom.css">
</head>
 <div class="content-wrapper">
    <section class="content-header">
        <h1><?= $title;?></h1>
        <?php if((count($errors)>0)){ ?>
        <div class="alert alert-danger">
        <ul>

        <?php 
        foreach ($errors->all('<li>:message</li>') as $message)
        {
        echo $message;
        }
        ?>
        </ul>
        </div>
        <?php  } ?>

       @if(session()->has('warning'))
       <center style="color:green"> {!! session()->get('warning') !!}</center>
       @endif
      
       <ol class="breadcrumb">
       <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
       <li class="active"><?= $title;?></li>
    </section>
     <section class="content">
        <div class="box   box-primary">
           <!--  <div class="box-header with-border">
                <h3 class="box-title">Add Vendor</h3>
            </div> -->

        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
        
          <form  action="{{url('admin/player/add'). ($id!='' ?  '/'.$id : '')}}" method="post" enctype="multipart/form-data" id="myform" autocomplete="off">
                 <input type="hidden" name="id" value="<?=$id?>"/>
             
                  <div class='box-body' >
                        {{ csrf_field() }}

                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Team</label>

                                 
                                <select class="form-control select2"  name="team_id" style="width: 100%;" required>
                                  <option value="">Select team</option>
                                <?php foreach ($teams as $tvalue) {
                                    $sel="";


                                    if(isset($player_details) &&  $tvalue->id==$player_details->team_id){
                                        $sel="selected='selected'";
                                    }

                                    echo "<option value='".$tvalue->id."' ".$sel.">".$tvalue->team_name."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>                     
                        <div class="row">
                           
                            <div class="form-group col-md-6">
                              <label for="inputState">First Name*</label>
                              <input type="text" value="<?= isset($_POST['first_name'])  ? $_POST['first_name']  : ( isset($player_details->first_name) ? $player_details->first_name : ''); ?>" class="form-control" id="" name="first_name" placeholder="Enter First Name" required/>
                            </div>

                        </div>

                        <div class="row">
                           
                            <div class="form-group col-md-6">
                              <label for="inputState">Last Name</label>
                              <input type="text" value="<?= isset($_POST['last_name'])  ? $_POST['last_name']  : ( isset($player_details->last_name) ? $player_details->last_name : ''); ?>" class="form-control" id="" name="last_name" placeholder="Enter Last Name" required/>
                            </div>

                        </div>


                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Full Name*</label>
                              <input type="text" class="form-control" id="full_name" name="full_name" value="<?= isset($_POST['full_name']) ? $_POST['full_name'] : ( isset($player_details->full_name) ? $player_details->full_name : ''); ?>" placeholder="Full name"/>
                              <span id='error'></span>
                            </div>
                             
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Country*</label>
                              <input type="text" class="form-control" id="country" name="country" value="<?= isset($_POST['country']) ? $_POST['country'] : ( isset($player_details->country) ? $player_details->country : ''); ?>" placeholder="Country"/>
                              <span id='error'></span>
                            </div>
                             
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Jersey number*</label>
                              <input type="text" class="form-control" id="jersey_number" name="jersey_number" value="<?= isset($_POST['jersey_number']) ? $_POST['jersey_number'] : ( isset($player_details->jersey_number) ? $player_details->jersey_number : ''); ?>" placeholder="Jersey number"/>
                              <span id='error'></span>
                            </div>
                             
                        </div>
                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Type</label>

                                 
                                <select class="form-control select2"  name="type" style="width: 100%;">
                                <?php foreach ($player_type as $pkey => $pvalue) {
                                    $sel="";


                                    if(isset($player_details->type) &&  $pkey==$player_details->type){
                                        $sel="selected='selected'";
                                    }

                                    echo "<option value='".$pkey."' ".$sel.">".$pvalue['title']."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>


                         <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Is Caption</label>

                                 
                                <label class="radio-inline"><input type="radio" value="yes" <?php echo (isset($player_details) && $player_details->is_captain=="yes")  ? "checked" :""; ?> name="is_captain" >Yes</label>
                                <label class="radio-inline"><input type="radio" value="no"  <?php echo (isset($player_details) && $player_details->is_captain=="no")  ? "checked" :"checked"; ?> name="is_captain">No</label>
                            </div>
                        </div>

                      
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Description*</label>
                              <textarea class="form-control" placeholder="Enter description" id="description" name="description"><?= isset($_POST['description']) ? $_POST['description'] : ( isset($player_details->description) ? $player_details->description : ''); ?></textarea>
                              
                              <span id='error'></span>
                            </div>
                             
                        </div>

                        <div class="row">
                          
                           <div class="form-group col-md-6">
                                <label for="">Status</label>

                                 
                                <select class="form-control select2"  name="status" style="width: 100%;">
                                <?php foreach ($user_status as $key => $value) {
                                    $sel="";


                                    if(isset($player_details->status) &&  $key==$player_details->status){
                                        $sel="selected='selected'";
                                    }elseif(!isset($player_details) ){
                                        $sel="selected='selected'";
                                    }

                                    echo "<option value='".$key."' ".$sel.">".$value['title']."</option>";
                                } ?>

                                </select>
                            </div>
                        </div>
                      
                        
                       
                        <div class="row">
                      
                          <div class="form-group col-md-6">
                               
                               
                             
                              
                                  <label for="">Profile image <small>(Size:500x500)</small> </label>
                                  <input type="file" class="form-control size-check-image" id="profile_image" name="profile_image" placeholder="image" />
                                  <div class="image-pic1 rmv{{isset($player_details->id) ? $player_details->id :'0'}}">
                                    <?php  if(isset($player_details->profile_image)) { ?>
                                       <a href="javascript:void(0);" class="btn" onclick="imageRemove('<?php echo $player_details->id ?>','user')" >Reset File</a>
                                       <br>
                                      <img id="image_view" src="<?php echo URL::to( (isset($player_details->profile_image) && $player_details->profile_image!='') ? $player_details->profile_image : '/images/profile/vendors/user_profile_demo.png')?>" height="100px" width="150px" />
                                  <?php } ?>
                                  </div>
                              
                          </div>

                          
                        
                        </div>
                       
                         <!-- fix image size -->
                        <input type="hidden" id="image_size" height="500" width="500">

                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>

                  </div>
            
                </form>
           
        </div>
     </section>

 
</div>

 
<script type="text/javascript">



 $(document).ready(function(){
    
    
    $("#profile_image").change(function() {
     
        readURL(this);
        });
    });
 


 function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.image-pic1').html('<img src="'+e.target.result+'" height="100px" width="150px">');
    }
    reader.readAsDataURL(input.files[0]);
  }
 }



</script>
<script type="text/javascript">
   


$(document).ready(function (){


    
            
    $('#myform').validate({ // initialize the plugin
        rules:{
        }
    });




  </script>
@endsection
