@extends('layouts.admin.layouts')
@section('content')
<head>
<link rel="stylesheet" type="text/css" href="custom.css">
</head>
 <div class="content-wrapper">
    <section class="content-header">
        <h1><?= $title;?></h1>
        <?php if((count($errors)>0)){ ?>
        <div class="alert alert-danger">
        <ul>

        <?php 
        foreach ($errors->all('<li>:message</li>') as $message)
        {
        echo $message;
        }
        ?>
        </ul>
        </div>
        <?php  } ?>

       @if(session()->has('warning'))
       <center style="color:green"> {!! session()->get('warning') !!}</center>
       @endif
      
       <ol class="breadcrumb">
       <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
       <li class="active"><?= $title;?></li>
    </section>
     <section class="content">
        <div class="box   box-primary">
           <!--  <div class="box-header with-border">
                <h3 class="box-title">Add Vendor</h3>
            </div> -->

        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
        
          <form  action="{{url('admin/player/player_history/'). ($player_id!='' ?  '/'.$player_id : '')}}" method="post" enctype="multipart/form-data" id="myform" autocomplete="off">
                 <input type="hidden" name="player_id" value="<?=$player_id?>"/>
             
                  <div class='box-body' >
                        {{ csrf_field() }}

                         
                        <div class="row">
                           
                            <div class="form-group col-md-6">
                              <label for="inputState">Matches Played*</label>
                              <input type="text" value="<?= isset($_POST['matches_played'])  ? $_POST['matches_played']  : ( isset($player_history_details->matches_played) ? $player_history_details->matches_played : ''); ?>" class="form-control" id="" name="matches_played" placeholder="Matches Played" required/>
                            </div>

                           
                            <div class="form-group col-md-6">
                              <label for="inputState">Batting Innings</label>
                              <input type="text" value="<?= isset($_POST['batting_innings'])  ? $_POST['batting_innings']  : ( isset($player_history_details->batting_innings) ? $player_history_details->batting_innings : ''); ?>" class="form-control" id="" name="batting_innings" placeholder="Batting Innings" required/>
                            </div>

                        </div>


                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Total Runs</label>
                              <input type="text" class="form-control" id="total_runs" name="total_runs" value="<?= isset($_POST['total_runs']) ? $_POST['total_runs'] : ( isset($player_history_details->total_runs) ? $player_history_details->total_runs : ''); ?>" placeholder="Total Runs"/>
                              <span id='error'></span>
                            </div>
                             
                            <div class="form-group col-md-6">
                              <label for="inputCity">High score</label>
                              <input type="text" class="form-control" id="high_score" name="high_score" value="<?= isset($_POST['high_score']) ? $_POST['high_score'] : ( isset($player_history_details->high_score) ? $player_history_details->high_score : ''); ?>" placeholder="High score"/>
                              <span id='error'></span>
                            </div>
                             
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Fifties</label>
                              <input type="text" class="form-control" id="fifties" name="fifties" value="<?= isset($_POST['fifties']) ? $_POST['fifties'] : ( isset($player_history_details->fifties) ? $player_history_details->fifties : ''); ?>" placeholder="Fifties"/>
                              <span id='error'></span>
                            </div>

                            <div class="form-group col-md-6">
                              <label for="inputCity">Hundreds</label>
                              <input type="text" class="form-control" id="hundreds" name="hundreds" value="<?= isset($_POST['hundreds']) ? $_POST['hundreds'] : ( isset($player_history_details->hundreds) ? $player_history_details->hundreds : ''); ?>" placeholder="Hundreds"/>
                              <span id='error'></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Notouts</label>
                              <input type="text" class="form-control" id="notouts" name="notouts" value="<?= isset($_POST['notouts']) ? $_POST['notouts'] : ( isset($player_history_details->notouts) ? $player_history_details->notouts : ''); ?>" placeholder="Notouts"/>
                              <span id='error'></span>
                            </div>

                            <div class="form-group col-md-6">
                              <label for="inputCity">Bating Average</label>
                              <input type="text" class="form-control" id="bat_avg" name="bat_avg" value="<?= isset($_POST['bat_avg']) ? $_POST['bat_avg'] : ( isset($player_history_details->bat_avg) ? $player_history_details->bat_avg : ''); ?>" placeholder="bat_avg"/>
                              <span id='error'></span>
                            </div>
                             
                          
                           
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Bowling Average</label>
                              <input type="text" class="form-control" id="bowl_avg" name="bowl_avg" value="<?= isset($_POST['bowl_avg']) ? $_POST['bowl_avg'] : ( isset($player_history_details->bowl_avg) ? $player_history_details->bowl_avg : ''); ?>" placeholder="Bowling Average"/>
                              <span id='error'></span>
                            </div>

                            <div class="form-group col-md-6">
                              <label for="inputCity">Total wickets</label>
                              <input type="text" class="form-control" id="total_wickets" name="total_wickets" value="<?= isset($_POST['total_wickets']) ? $_POST['total_wickets'] : ( isset($player_history_details->total_wickets) ? $player_history_details->total_wickets : ''); ?>" placeholder="total_wickets"/>
                              <span id='error'></span>
                            </div>
                             
                          
                           
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                              <label for="inputCity">Bowling Economy</label>
                              <input type="text" class="form-control" id="bowl_economy" name="bowl_economy" value="<?= isset($_POST['bowl_avg']) ? $_POST['bowl_economy'] : ( isset($player_history_details->bowl_economy) ? $player_history_details->bowl_economy : ''); ?>" placeholder="Bowling Economy"/>
                              <span id='error'></span>
                            </div>

                            <div class="form-group col-md-6">
                              <label for="inputCity">Total Stumpings</label>
                              <input type="text" class="form-control" id="total_stumpings" name="total_stumpings" value="<?= isset($_POST['total_stumpings']) ? $_POST['total_stumpings'] : ( isset($player_history_details->total_stumpings) ? $player_history_details->total_stumpings : ''); ?>" placeholder="Total Stumpings"/>
                              <span id='error'></span>
                            </div>
                             
                          
                           
                        </div>


                        
                       
                       

                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>

                  </div>
            
                </form>
           
        </div>
     </section>

 
</div>

 
<script type="text/javascript">



 $(document).ready(function(){
    
    
    $("#profile_image").change(function() {
     
        readURL(this);
        });
    });
 


 function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.image-pic1').html('<img src="'+e.target.result+'" height="100px" width="150px">');
    }
    reader.readAsDataURL(input.files[0]);
  }
 }



</script>
<script type="text/javascript">
   


$(document).ready(function (){


    
            
    $('#myform').validate({ // initialize the plugin
        rules:{
        }
    });




  </script>
@endsection
