<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a href=''>Cricket APP</a>.</strong>
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>


<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminUI/components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<!-- script src="{{ asset('adminUI/components/datatables.net/js/jquery.dataTables.min.js')}}"></script> -->
<!-- <script src="{{ asset('adminUI/components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
 --><!-- SlimScroll -->
<!-- 
<script src="{{ asset('adminUI/components/select2/dist/js/select2.full.min.js')}}"></script> -->
<!-- <script src="{{ asset('adminUI/components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script> -->
<!-- FastClick -->
<script src="{{ asset('adminUI/components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminUI/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminUI/dist/js/demo.js')}}"></script>


<!-- page script -->
<script src="{{asset('adminUI/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> <!-- for toaster -->


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('adminUI/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<!-- bootstrap datepicker -->
<script src="{{asset('adminUI/components/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<!-- <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="{{ asset('public/js/jquery.blockui.min.js')}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>

<script>
  // image size check
var _URL = window.URL || window.webkitURL;
$(".size-check-banner").change(function(e) {
    var file, img;
    $(this).after('<a href="javascript:void(0);" class="btn reset-file">Reset File</a>');
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
          if(this.width != 1400 && this.height != 450)
          {
        //    alert("Image size should be less than 1400px X 450px ");
           $('.image-pic').empty();
            $(".size-check-image").val(null);
            swal("Failed", "Image size should be less than (1400px X 450px)", "error");
             $('#submit').prop('disabled',true);
          }
          else{
              $('#submit').prop('disabled',false);
          }
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
            $('#submit').prop('disabled',true);
        };
        img.src = _URL.createObjectURL(file);
      
    }
});
$(".size-check-image").change(function(e) {
    var file, img;

     $(this).after('<a href="javascript:void(0);" class="btn reset-file1">Reset File</a>');
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
             if(this.width != 500 && this.height != 500)
          {   
             // $('.image-pic').empty();
             // $(".size-check-image").val(null);
            swal("Failed", "Image size should be  (500px X 500px)", "error");
             $('#submit').prop('disabled',true);
             
          }
          else{

              $('#submit').prop('disabled',false);

          }
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
            $('#submit').prop('disabled',true);
        };
        img.src = _URL.createObjectURL(file);
     
    }
});






  

 $(document).ready(function(){
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear()-10;
var today1 = mm + '/' + dd + '/' + yyyy;
$("#birthday").datepicker({
     endDate:today1,
    });
});

 $(document).on('click','.reset-file', function(e){
    $('.banner_image').empty();
    $('.image-pic1').empty();
    $('.image_view').empty();
    $('.reset-file').remove();
    $(".size-check-banner").val(null);
   $('#submit').prop('disabled',false);
});
  $(document).on('click','.reset-file1', function(e){
    $('.banner_image').empty();
    $('.image-pic1').empty();
    $('.image_view_thumb').empty();
    $('.reset-file1').remove();
    $(".size-check-image").val(null);
   $('#submit').prop('disabled',false);
});
</script>

<script>

$("#out").delay(2000).fadeOut(300);

//use for remove image

        var Messages = {
        Delete: {
            Confirm: "Are you sure you want to delete this image?",
            Success: "image has been deleted successfully.",
            Failure: "Unable to delete image, please try again.",
            CanNotDelete: "Can not delete this image.",

        },
        
        Enablestatus: {
            Confirm: "Are you sure you want to activate this image?",
            Success: "image has been activated successfully.",
            Failure: "Unable to activate image, please try again.",
            CanNotDelete: "Can not activate this image.",

        },
        DisableStatus: {
            Confirm: "Are you sure you want to Deactivate this image?",
            Success: "image has been Deactivated successfully.",
            Failure: "Unable to Deactivate image, please try again.",
            CanNotDelete: "Can not Deactivate this image.",

        },
};
    

  function imageRemove(id,type,banner=0){
                if (id != undefined && id > 0 && id != "") {
                    bootbox.dialog({
                        title: "Please Confirm!",
                        message: Messages.Delete.Confirm,
                        buttons: {
                            success: {
                                label: "Yes",
                                className: "btn-danger",
                                callback: function() {

                                    $.ajax({
                                        type: "POST",
                                       url: APP_URL + '/common/xhr?cmd=imageremove',
                                        data: {
                                        _token: '{!! csrf_token() !!}',
                                        "type":type,
                                        "id":id   
                                        },
                                        dataType: 'JSON',
                                        beforeSend: function() {
                                            $.blockUI();

                                        },
                                        success: function(res) {
                                            $.unblockUI();
                                            if (res.Result == 'OK') {
                                        
                                                toastr.success(res.Message);

                                                $('.rmv'+id).empty();
                                                if(banner==1)
                                                {
                                                  $('.image_view').empty();
                                               
                                                }
                                                else{

                                                      $('.image_view_thumb').remove();

                                                }

                                            } else {

                                                toastr.error(res.Message);
                                               
                                            }

                                        }
                                    });
                                }
                            },
                            danger: {
                                label: "No",
                                className: "btn-default",
                                callback: function() {

                                }
                            },

                        }
                    });


                } else if (id == -1) {

                    toastr.error(Messages.Delete.CanNotDelete);
                } else {

                    toastr.error(Messages.Delete.Failure);
                }

             
 
    }

// image remove


</script>

 
</body>
</html>
