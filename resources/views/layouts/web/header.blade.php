<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="_token" content="{{ csrf_token() }}">
  <title>Cricket APP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="{{ asset('adminUI/components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminUI/components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('adminUI/components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
 <!--  <link rel="stylesheet" href="{{ asset('adminUI/components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminUI/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the loa -->
  <link rel="stylesheet" href="{{asset('adminUI/dist/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" href="{{asset('adminUI/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminUI/components/bootstrap-timepicker/css/timepicker.css')}}">

 <!--  <link rel="stylesheet" href="{{ asset('adminUI/components/select2/dist/css/select2.min.css')}}"> -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
  <!-- Google Font -->
  <!--  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 -->

  <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
 

  <!--         <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCRaiGJ3SalSwaogZXAXWRVW93wnR6Z1qQ&sensor=false&libraries=places&language=en-AU"></script> -->

  <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}

    var APP_ADMIN_URL = {!! json_encode(url('/admin')) !!}
    var APP_ANALYTICS_ADMIN_URL = {!! json_encode(url('/analytics')) !!}

  </script>
  <!-- ./wrapper -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  <!-- jQuery 3 -->
  <!-- <script src="{{ asset('adminUI/components/jquery/dist/jquery.min.js')}}"></script>   -->
  <!-- Tree View  -->
  <!-- <script src="{{ asset('public/js/jquery.treeView.js')}}"></script>   -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
  <script src="{{ asset('public/js/jquery.blockui.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('public/js/accounting.js')}}" type="text/javascript"></script>
  <script src="{{ asset('js/common.js')}}" type="text/javascript"></script>
  <!-- <link rel="stylesheet" type="text/css" src="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
 -->
 <link rel="stylesheet" href="{{ asset('adminUI/dist/css/web_style.css')}}">
  
  <style type="text/css">
    .error {
      color: red;
    }

    /* This style is added to offset the space coming on main website, since common css is being used */

    body {
      padding-top: 0! important;
    }
  </style>

  <?php $user_data = array();

?>
</head>

<body class="hold-transition skin-blue layout-boxed">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="{{url('/')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!-- <span class="logo-mini"><b></b>Chatri</span> -->
        <!-- logo for regular state and mobile devices -->
        <!--  <span class="logo-lg"><b>Chatri</b></span> -->
        <img onerror="this.onerror=null;this.src='<?=asset(Config::get('constants.DEFAULT_NO_IMAGE'))?>';" src="{{asset('images/logo.jpg')}}"
          width="120" alt="">
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        

        <div class="navbar-custom-menu">
          
        </div>
      </nav>
    </header>