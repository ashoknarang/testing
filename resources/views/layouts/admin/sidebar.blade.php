<?php  $user_data =array();



  ?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    
    <!-- search form -->

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="{{ (Request::is('admin/dashboard/')) ? 'active' : ''}}">

        <a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a>
      </li>

      
      <li class=" {{ (Request::is('admin/users/*') || Request::is('admin/user/*')   ) ? 'active' : ''}}">
        <a href="{{url('admin/users')}}"><i class="fa fa-user"></i>Manage Users</a>
      </li>
      <li class=" {{ (Request::is('admin/teams/*') || Request::is('admin/teams/*')   ) ? 'active' : ''}}">
        <a href="{{url('admin/teams')}}"><i class="fa fa-user"></i>Manage Teams</a>
      </li>

       <li class=" {{ (Request::is('admin/players/*') || Request::is('admin/players/*')   ) ? 'active' : ''}}">
        <a href="{{url('admin/players')}}"><i class="fa fa-user"></i>Manage Players</a>
      </li>
      
      <li class=" {{ (Request::is('admin/series/*') || Request::is('admin/series/*')   ) ? 'active' : ''}}">
        <a href="{{url('admin/series')}}"><i class="fa fa-user"></i>Manage Series</a>
      </li>
      
      <li class=" {{ (Request::is('admin/matches/*') || Request::is('admin/matches/*')   ) ? 'active' : ''}}">
        <a href="{{url('admin/matches')}}"><i class="fa fa-user"></i>Manage Matches</a>
      </li>
      


       
      </ul>







  </section>
  <!-- /.sidebar -->
</aside>