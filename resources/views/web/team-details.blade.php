@extends('layouts.web.layouts')

@section('content')
  <div class="content-wrapper page-inner">
    <!-- Content Header (Page header) -->
    <section class="content content-no-padd">
        <div class="banner">
            <img src="{{asset('images/cricket-banner-inner.png')}}" alt="Cricket APP" />
        </div>
        <div class="content">
            <h2 class="page-header"><a href="{{url('/')}}">IPL 2019 Teams</a> &raquo; <?php echo (!empty($team_details->team_name) ? ucwords($team_details->team_name) : "")?></h2>
            <div>
                    <div class="col-sm-12 col-md-3">
                        <?php if(!empty($team_details->image)){ ?>
                          <img src="{{asset($team_details->image)}}" alt="team logo" class="media-object" style="max-width: 250px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">
                        <?php } ?>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <p>
                                <?php if(!empty($team_details->description)){
                                    echo $team_details->description;
                                 }?>    
                        </p>
                    </div>
                    <div class="clearfix"></div>
                    <h2 class="page-header"><?php echo (!empty($team_details->team_name) ? ucwords($team_details->team_name) : "")?> Player List</h2>
                    <div class="team-players">
                            
                        <?php if(!empty($team_players)) {
                            foreach($team_players as $a=>$b) { //print_r();
                            ?>

                            <div class="modal fade" id="player-modal-<?php echo $b->id?>" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title"><?php echo $b->first_name . ' ' . $b->last_name;?> History</h4>
                                    </div>
                                    <div class="modal-body">
                                    
                                    <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Name</th>
                  <th>Value</th>
                </tr>
                <tr>
                  <td>Matches Played</td>
                  <td><?php echo $b->player_history->matches_played?></td>
                </tr>
                <tr>
                  <td>Batting Innings</td>
                  <td><?php echo $b->player_history->batting_innings?></td>
                </tr>
                <tr>
                  <td>Total Runs</td>
                  <td><?php echo $b->player_history->total_runs?></td>
                </tr>
                <tr>
                  <td>High Score</td>
                  <td><?php echo $b->player_history->high_score;?></td>
                </tr>
                <tr>
                  <td>Fifties</td>
                  <td><?php echo $b->player_history->fifties;?></td>
                </tr>
                <tr>
                  <td>Hundreds</td>
                  <td><?php echo $b->player_history->hundreds;?></td>
                </tr>
                <tr>
                  <td>Notouts</td>
                  <td><?php echo $b->player_history->notouts;?></td>
                </tr>
                <tr>
                  <td>Bating Average</td>
                  <td><?php echo $b->player_history->bat_avg;?></td>
                </tr>
                <tr>
                  <td>Bowling Average</td>
                  <td><?php echo $b->player_history->bowl_avg;?></td>
                </tr>
                <tr>
                  <td>Total Wickets</td>
                  <td><?php echo $b->player_history->total_wickets;?></td>
                </tr>
                <tr>
                  <td>Bowling Economy</td>
                  <td><?php echo $b->player_history->bowl_economy;?></td>
                </tr>
                <tr>
                  <td>Total Stumpings</td>
                  <td><?php echo $b->player_history->total_stumpings;?></td>
                </tr>
              </tbody></table>
            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        <div class="col-md-4">
                        <!-- Widget: user widget style 1 -->
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-yellow">
                            <h3 class="widget-user-username"><?php echo $b->first_name . ' ' . $b->last_name;?></h3>
                            <h5 class="widget-user-desc"><?php echo ucwords(str_replace("_"," ",$b->type));?>
                            <a href="javascript:;" data-toggle="modal" data-target="#player-modal-<?php echo $b->id?>" style='margin-top: -32px;' class="btn btn-sm btn-default pull-right">View History</a>
                            </h5>
                            </div>
                            <?php if(!empty($b->profile_image)) {?>
                            <div class="widget-user-image">
                            <img class="img-circle" src="{{asset($b->profile_image)}}" alt="User Avatar">
                            </div>
                            <?php }?>
                            <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Country</h5>
                                    <span class="description-text"><?php echo ucwords($b->country);?></span>
                                </div>
                                <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 border-right">
                                <div class="description-block">
                                <h5 class="description-header">Jersey No.</h5>
                                    <span class="description-text"><?php echo ucwords($b->jersey_number);?></span>
                                </div>
                                <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4">
                                <div class="description-block">
                                <h5 class="description-header">Captain</h5>
                                    <span class="description-text"><?php echo ucwords($b->is_captain);?></span>
                                </div>
                                <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                            </div>
                        </div>
                        <!-- /.widget-user -->
                        </div>
                        <?php }
                        }?>
                    </div>
                    <div class="clearfix"></div>

                    <div class='teamPlayers'>
                    <h2 class="page-header"><?php echo (!empty($team_details->team_name) ? ucwords($team_details->team_name) : "")?> Series List</h2>
							<?php if(!empty($series_team)) {
								foreach($series_team as $series) {
                                    
                                    //print_r($series['series_data']->series);die;
                                $startDate = date('d-M-Y', strtotime($series['series_data']->series['start_date'])); $endDate = date('d-M-Y', strtotime($series['series_data']->series['end_date'])); 
                                ?>								
								<div class='col-md-12 minfo box-body table-responsive no-padding'>
                                    <h4 class='box box-primary'><span class='pull-right text-yellow'>From: <?php echo $startDate . '<br/> To: ' . $endDate;?></span><?php echo $series['series_data']->series['series_title'] ?>
                                    </h4>
									<table class="table table-hover">
									<thead>
									<tr>
											<td>Team</td>
											<td>Total Matches</td>
											<td>Win</td>
											<td>Loss</td>
											<td>Tied / NR</td>
											<td>Points</td>
											<td>Net RR</td>
										</tr>
									</thead>
									<tbody>
									<?php foreach($series['match_data'] as $teams_records) {
                                        $teams_records = (object)$teams_records;
                                        ?>
										<tr>
											<td><?php echo $teams_records->team_name?></td>
											<td><?php echo $teams_records->played_match;?></td>
											<td><?php echo $teams_records->win_match?></td>
                                            <td><?php echo $teams_records->loss_match?></td>
                                            <?php if($series['series_data']->series['status'] == "upcoming"){?>
                                                <td>0</td>
                                            <?php } else {?>
											<td><?php echo ($teams_records->played_match - $teams_records->win_match - $teams_records->loss_match)?></td>
											<?php } if(trim($teams_records->win_match) > 0) { ?> 
											<td>{{ $teams_records->total_points}}</td>
                                            <?php } else {?>
											<td>0</td>
                                            <?php }?>
											<td><?php echo  $teams_records->net_runrate?></td>
										</tr>
                                            <?php }?>
									</tbody>
									</table>
                                </div>
                                            <?php }
                                            }?>
                        
                    <div class='clearfix'></div>
                    </div>
        </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection