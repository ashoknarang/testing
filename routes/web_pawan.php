<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::pattern('userHost', '(mychhatri\.dev)');
Route::group(['domain' => '{userHost}'], function (){
    \Route::get('/', function () {
    	return view('admin.login');
	});	
});



Route::pattern('adminHost', '(admin\.mychhatri\.dev)');
Route::group(['domain' => '{adminHost}'], function (){
	Route::get('/', function () {
	    return view('admin.login');
	}); 
	Route::get('/dashboard','Admin\DashboardController@index');
	Route::get('/student/list','Admin\UsersController@studentList');
	Route::match(['get', 'post'], '/student/add','Admin\UsersController@studentAdd');
	Route::match(['get', 'post'], '/user/status','Admin\UsersController@userStatus');
	Route::match(['get', 'post'], '/userdata/{id}','Admin\UsersController@usersData');
	Route::match(['get', 'post'], '/profileupdate','Admin\UsersController@profileUpdate');
});



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
