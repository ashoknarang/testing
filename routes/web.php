<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Route::get('/help', function () {
//     return view('web.help');
// });

error_reporting(E_ALL);
ini_set('display_errors', 1);

Route::get('/expire', function () {
    return view('session_out');
})->name('expire');

Route::get('/', 'Web\WelcomeController@index')->name('home');
Route::get('/team-details/{id}', 'Web\WelcomeController@teamDetails');

//sprint_r($_SESSION);
Route::group(['middleware' => ['redirectIfAuthenticated']], function () {

    Route::group(['prefix' => 'admin'], function () {

        Route::get('/', 'Admin\WelcomeController@index')->name('admin');
        Route::get('/login', 'Admin\WelcomeController@index');

        Route::match(['get', 'post'], '/reset_password', 'Admin\WelcomeController@resetPassword');
        Route::group(['middleware' => ['auth', 'cleanhtml']], function () {

            //======================Common Rout e For  vendor and admin===================//
            Route::get('/users/trainers', 'Admin\UsersController@usersList');
            Route::get('/users/analyticmanager', 'Admin\UsersController@usersList');
            Route::get('/userloginlog', 'Admin\UsersController@userslogilogList');
            Route::get('/dashboard', 'Admin\DashboardController@index');
            Route::get('/users', 'Admin\UsersController@usersList');
            Route::match(['get', 'post'], 'users/xhr', 'Admin\UsersController@xhr');
            Route::match(['get', 'post'], '/user/add/{id?}/', 'Admin\UsersController@addedituser');
            /** Admin Request  end**/

            Route::get('/teams', 'Admin\TeamsController@teamsList');
            Route::match(['get', 'post'], 'teams/xhr', 'Admin\TeamsController@xhr');
            Route::match(['get', 'post'], '/team/add/{id?}/', 'Admin\TeamsController@addeditteam');

            Route::get('/players', 'Admin\PlayersController@playersList');
            Route::match(['get', 'post'], 'players/xhr', 'Admin\PlayersController@xhr');
            Route::match(['get', 'post'], '/player/add/{id?}/', 'Admin\PlayersController@addeditplayer');
            Route::match(['get', 'post'], '/player/player_history/{player_id?}/', 'Admin\PlayersController@playerHistory');

            Route::get('/series', 'Admin\SeriesController@seriesList');
            Route::match(['get', 'post'], 'series/xhr', 'Admin\SeriesController@xhr');
            Route::match(['get', 'post'], '/series/add/{id?}/', 'Admin\SeriesController@addeditseries');

            Route::get('/matches', 'Admin\MatchesController@matchesList');
            Route::match(['get', 'post'], 'matches/xhr', 'Admin\MatchesController@xhr');
            Route::match(['get', 'post'], '/matches/add/{id?}/', 'Admin\MatchesController@addeditmatch');


            //========================================================================//

            
        });

    });

/**  web route * */
    Auth::routes();
   





});



 