/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.filebrowserBrowseUrl = APP_URL+'/adminUI/plugins/kcfinder/browse.php?opener=ckeditor&type=files';

    config.filebrowserImageBrowseUrl = APP_URL+'/adminUI/plugins/kcfinder/browse.php?opener=ckeditor&type=images';

    config.filebrowserFlashBrowseUrl = APP_URL+'/adminUI/plugins/kcfinder/browse.php?opener=ckeditor&type=flash';

     config.filebrowserUploadUrl = APP_URL+'/kcfinder/adminUI/plugins/upload.php?opener=ckeditor&type=files';

    config.filebrowserImageUploadUrl = APP_URL+'/kcfinder/adminUI/plugins/upload.php?opener=ckeditor&type=images';

    config.filebrowserFlashUploadUrl = APP_URL+'/kcfinder/adminUI/plugins/upload.php?opener=ckeditor&type=flash';
	
};
