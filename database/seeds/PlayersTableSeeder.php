<?php

use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // random player names according to team
        $team_player_names = array(
            1 => array('Bhuvneshwar Kumar', 'Virat Kohli', 'Shikhar Dhawan', 'Jasprit Bumrah', 'Rohit Sharma', 'MS Dhoni', 'Lokesh Rahul', 'Umesh Yadav', 'Kedar Jadhav', 'Suresh Raina', 'Manish Pandey'),
            2 => array('Sean Abbott', 'Ashton Agar', 'Fawad Ahmed', 'Terry Alderman', 'Jo Angel', 'George Bailey', 'Graeme Beard', 'Murray Bennett', 'Michael Bevan', 'Andy Bichel', 'Glenn Bishop'),
            3 => array('Chris Adams', 'Jonathan Agnew', 'Kabir Ali', 'Moeen Ali', 'Mark Alleyne', 'Paul Allott', 'Tim Ambrose', 'Dennis Amiss', 'James Anderson', 'Zafar Ansari', 'Geoff Arnold'),
            4 => array('Kyle Abbott', 'Shafiek Abrahams', 'Paul Adams', 'Hashim Amla', 'Adam Bacher', 'Temba Bavuma', 'Farhaan Behardien', 'Dale Benkenstein', 'Gulam Bodi', 'Nicky Boje', 'Tertius Bosch'),
            5 => array('Chris Adams', 'Graeme Aldridge', 'Geoff Allott', 'Corey Anderson', 'Robert Anderson', 'Nathan Astle', 'Todd Astle', 'Mark Bailey', 'Michael Bates', 'Matthew Bell', 'Hamish Bennett'),
            6 => array('Andre Mark', 'Chris Adams', 'Robert Allott', 'Hashim Amla', 'Geoff Anderson', 'Mark Alleyne', 'Astle Todd', 'Mark Bailey', 'Michael Bell', 'Matthew Hamish', 'Bates Bennett'),
           
        );

        $team_players  =array(); // all players for seed

        // default player roles
        $player_role_list = ['batsman','bowler','wicket_keeper','wicket_keeper_batsman','all_rounder'];

        // default bating styles
        $bating_style_list = ['LHB', 'RHB'];

        // default bowling styles
        $bowling_style_list = ['leg_spin','leg_break','off_spin','off_break','fast','medium_fast'];

        // default captain type
        $captain_type_list = ['yes', 'no'];

        // default description
        $default_description = "Left till here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall cold he. Feelings way likewise addition wandered contempt bed indulged. Paid was hill sir high. For him precaution any advantages dissimilar comparison few terminated projecting. Prevailed discovery immediate objection of ye at. Repair summer one winter living feebly pretty his. In so sense am known these since. Shortly respect ask cousins brought add tedious nay. Expect relied do we genius is. On as around spirit of hearts genius. Is raptures daughter branched laughter peculiar in settling. Attachment apartments in delightful by motionless it no. And now she burst sir learn total. Hearing hearted shewing own ask. Solicitude uncommonly use her motionless not collecting age. The properly servants required mistaken outlived bed and. Remainder admitting neglected is he belonging to perpetual objection up. Has widen too you decay begin which asked equal any. Same an quit most an. Admitting an mr disposing sportsmen. Tried on cause no spoil arise plate. Longer ladies valley get esteem use led six. Middletons resolution advantages expression themselves partiality so me at. West none hope if sing oh sent tell is. His having within saw become ask passed misery giving. Recommend questions get too fulfilled. He fact in we case miss sake. Entrance be throwing he do blessing up. Hearts warmth in genius do garden advice mr it garret. Collected preserved are middleton dependent residence but him how. Handsome weddings yet mrs you has carriage packages. Preferred joy agreement put continual elsewhere delivered now. Mrs exercise felicity had men speaking met. Rich deal mrs part led pure will but. Not him old music think his found enjoy merry. Listening acuteness dependent at or an. Apartments thoroughly unsatiable terminated sex how themselves. She are ten hours wrong walls stand early. Domestic perceive on an ladyship extended received do. Why jennings our whatever his learning gay perceive. Is against no he without subject. Bed connection unreserved preference partiality not unaffected. Years merit trees so think in hoped we as. Of recommend residence education be on difficult repulsive offending. Judge views had mirth table seems great him for her. Alone all happy asked begin fully stand own get. Excuse ye seeing result of we. See scale dried songs old may not. Promotion did disposing you household any instantly. Hills we do under times at first short an. Resolution possession discovered surrounded advantages has but few add. Yet walls times spoil put. Be it reserved contempt rendered smallest. Studied to passage it mention calling believe an. Get ten horrible remember pleasure two vicinity. Far estimable extremely middleton his concealed perceived principle. Any nay pleasure entrance prepared her. Mr oh winding it enjoyed by between. The servants securing material goodness her. Saw principles themselves ten are possession. So endeavor to continue cheerful doubtful we to. Turned advice the set vanity why mutual. Reasonably if conviction on be unsatiable discretion apartments delightful. Are melancholy appearance stimulated occasional entreaties end. Shy ham had esteem happen active county. Winding morning am shyness evident to. Garrets because elderly new manners however one village she. Brother set had private his letters observe outward resolve. Shutters ye marriage to throwing we as. Effect in if agreed he wished wanted admire expect. Or shortly visitor is comfort placing to cheered do. Few hills tears are weeks saw. Partiality insensible celebrated is in. Am offended as wandered thoughts greatest an friendly. Evening covered in he exposed fertile to. Horses seeing at played plenty nature to expect we. Young say led stood hills own thing get.";

        // create random player list
        foreach($team_player_names as $team_id => $player_list) {
            $captain_selected = false;
            foreach($player_list as $index => $name) {
                $t = array(); // temp array

                $first_name = $last_name = '';

                if(trim($name) != '') {
                    // break first and last name
                    $exp = explode(" ", $name);
                    $t['first_name'] = $exp[0];
                    $t['last_name'] = isset($exp[1]) ? $exp[1] : str_random(10);

                    // get display name
                    $rnd = rand(0, 1);
                    $t['full_name'] = ($rnd == 1) ? $t['last_name'] : $t['first_name'];
                }

                $is_captain = 'no';

                if($captain_selected == false) {
                    $is_captain = $captain_type_list[array_rand($captain_type_list, 1)];

                    if($is_captain == 'yes') {
                        $captain_selected = true;
                    }
                }


                $t['team_id'] = $team_id;
                $t['jersey_number'] = rand(1, 100);
                $t['type'] = $player_role_list[array_rand($player_role_list, 1)];
                $t['status'] = 'activate';
                $t['is_captain'] = $is_captain;
                $t['profile_image'] = ($team_id . '_' . ($index + 1)) . '.png';
                $t['created_at'] = date('Y-m-d H:i:s');
                $t['updated_at'] = date('Y-m-d H:i:s');
                $t['description'] = substr($default_description, rand(0,50), rand(300, 350));

                $team_players[] = $t;
            }
        }

        if(DB::table('players')->get()->count() == 0) {
            DB::table('players')->insert($team_players);
        }
    }
}
