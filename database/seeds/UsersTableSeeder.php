<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adminusers')->insert([
            'name' => 'Admin',
            'email' => 'admin@yopmail.com',
			'role' => 'admin',
			'status' => 'active',
            'password' => bcrypt('1234567890'),
        ]);
    }
}
