<?php

use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default_series_status = ['upcoming','ongoing','completed','cancelled'];

        $teams = [
            [
                'series_title' => 'VIVO IPL 2018',
                'status' => 'completed',
                'start_date' => '2018-04-12',
                'end_date' => '2018-06-19',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'series_title' => 'VIVO IPL 2019',
                'status' => 'running',
                'start_date' => '2019-03-22',
                'end_date' => '2019-05-12',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'series_title' => 'VIVO IPL 2020',
                'status' => 'upcoming',
                'start_date' => '2020-04-09',
                'end_date' => '2020-06-02',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        if(DB::table('series')->get()->count() == 0) {
            
            if(DB::table('series')->insert($teams)) {

                // default nrr sign
                $nrr_sign = array('','-');

                $series = [
                    [
                        'series_id' => '1',
                        'team_id' => '1',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '1',
                        'team_id' => '2',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '1',
                        'team_id' => '3',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '1',
                        'team_id' => '6',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '2',
                        'team_id' => '1',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '2',
                        'team_id' => '2',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '2',
                        'team_id' => '3',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '2',
                        'team_id' => '4',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '2',
                        'team_id' => '5',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '2',
                        'team_id' => '6',
                        'total_points' => rand(0,10),
                        'net_runrate' => $nrr_sign[array_rand($nrr_sign, 1)] . rand(0,2).'.'.rand(10,99),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '3',
                        'team_id' => '1',
                        'total_points' => 0,
                        'net_runrate' => 0.00,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '3',
                        'team_id' => '2',
                        'total_points' => 0,
                        'net_runrate' => 0.00,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '3',
                        'team_id' => '3',
                        'total_points' => 0,
                        'net_runrate' => 0.00,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ],
                    [
                        'series_id' => '3',
                        'team_id' => '4',
                        'total_points' => 0,
                        'net_runrate' => 0.00,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s') 
                    ]
                ];

                if(DB::table('series_teams')->get()->count() == 0) {

                    DB::table('series_teams')->insert($series);
                        // get all series with team ids
                    
                        $series_list = DB::select("SELECT s.id,s.status,s.end_date,s.start_date, GROUP_CONCAT(t.team_id) as team_ids FROM series s left join series_teams t on s.id = t.series_id group by s.id");
                        
                        if(count($series_list) > 0) {

                            $match_list = array();

                            $match_status = ['upcoming', 'running', 'completed'];
                            $match_result = ['team1','team2','match_tied'];

                            foreach($series_list as $a => $b) {
                                
                                $team_ids = explode(",", $b->team_ids);

                                $total_teams = count($team_ids);

                                $tn = $total_teams*rand(2,4);
                                for($i = 0; $i < $tn; $i++) {

                                    $matchteams = array_rand($team_ids, 2);

                                    $t = array();

                                    $mstatus = '';

                                    if($b->status == 'completed') {
                                        $match_status = ['completed','cancelled'];
                                        $match_result = ['team1','team2','match_tied'];
                                    } else if($b->status == 'running') {
                                        $match_status = ['completed','cancelled'];
                                        $match_result = ['team1','team2','match_tied'];
                                    } else if($b->status == 'upcoming') {
                                        $match_status = ['upcoming','upcoming'];
                                        $match_result = ['N/A','N/A'];
                                    }

                                    $int= mt_rand(strtotime($b->start_date), strtotime($b->end_date));
                                    $matchtime = date("Y-m-d",$int).' '.rand(0, 23).':'.rand(0,59).':'.rand(0,59);

                                    $t['series_id'] = $b->id;
                                    $t['team1_id'] = $team_ids[$matchteams[0]];
                                    $t['team2_id'] = $team_ids[$matchteams[1]];
                                    $t['match_datetime'] = $matchtime;
                                    $t['status'] = $match_status[array_rand($match_status, 1)];
                                    $t['result'] = $match_result[array_rand($match_result, 1)];
                                    if($t['result'] == 'team1') {
                                        $t['winner_team_id'] = $t['team1_id'];
                                    } else if($t['result'] == 'team1') {
                                        $t['winner_team_id'] = $t['team2_id'];
                                    } else {
                                        $t['winner_team_id'] = 0;
                                    }
                                    $t['created_at'] = date('Y-m-d H:i:s');
                                    $t['updated_at'] = date('Y-m-d H:i:s');

                                    $match_list[] = $t;
                                }
                            }

                            if(DB::table('matches')->get()->count() == 0) {
                                DB::table('matches')->insert($match_list);
                            }
                        }
                    
                }
            }
        }
    }
}
