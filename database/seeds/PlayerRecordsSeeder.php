<?php

use Illuminate\Database\Seeder;

class PlayerRecordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $player_records = array();

        $players = DB::table('players')->get();

        if( count($players) > 0) {

            // default match types
            $match_type_list = ['t20'];
            
            // default high score
            $df_high_score = ['','*'];

            foreach($players as $index => $obj) {

                foreach($match_type_list as $i => $match_type) {
                    $t = array();
                    $t['player_id'] = $obj->id;
                    $t['matches_played'] = rand(5, 200);
                    $t['batting_innings'] = rand(0, ($t['matches_played'] - 5));
                    $t['total_runs'] = rand(0, 8000);
                    $t['high_score'] = rand(0, 200).($df_high_score[array_rand($df_high_score, 1)]);
                    $t['fifties'] = rand(0, 50);
                    $t['hundreds'] = rand(0, 30);
                    $t['notouts'] = rand(0, ($t['batting_innings'] - 20));
                    $t['bat_avg'] = ($t['batting_innings'] > 0) ? (round(($t['total_runs'] / $t['batting_innings']), 2)) : 0.00;
                    $t['total_wickets'] = rand(0, ($t['matches_played'] + 25));
                    $t['bowl_avg'] = rand(20, 32).'.'.rand(10, 99);
                    $t['bowl_economy'] = rand(4, 7).'.'.rand(10, 99);
                    $t['total_stumpings'] = ($obj->type == 'wicket_keeper' || $obj->type == 'wicket_keeper_batsman') ? rand(0, ($t['matches_played'] + 25)) : 0;
                    $t['created_at'] = date('Y-m-d H:i:s');
                    $t['updated_at'] = date('Y-m-d H:i:s');

                    $player_records[] = $t;
                }
            }

            if(DB::table('player_history')->get()->count() == 0) {
                DB::table('player_history')->insert($player_records);
            }
        }
    }
}
