<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('series_id')->default(0);
            $table->integer('team1_id')->default(0);
            $table->integer('team2_id')->default(0);
            $table->dateTime('match_datetime')->default('1970-01-01 00:00:00');
            $table->enum('status', ['upcoming', 'running', 'completed'])->default('upcoming');
            $table->integer('winner_team_id')->default(0);
            $table->enum('result', ['team1', 'team2', 'match_tied'])->default('team1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
