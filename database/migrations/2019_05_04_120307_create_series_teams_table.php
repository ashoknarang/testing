<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('series_id')->default(0);
            $table->integer('team_id')->default(0);
            $table->tinyInteger('total_points')->default(0);
            $table->decimal('net_runrate')->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series_teams');
    }
}
