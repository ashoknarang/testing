<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->default(0);
            $table->integer('matches_played')->default(0);
            $table->integer('batting_innings')->default(0);
            $table->integer('total_runs')->default(0);
            $table->string('high_score', 30)->default('0');
            $table->integer('fifties')->default(0);
            $table->integer('hundreds')->default(0);
            $table->integer('notouts')->default(0);
            $table->decimal('bat_avg')->default(0.00);
            $table->decimal('bowl_avg')->default(0.00);
            $table->integer('total_wickets')->default(0);
            $table->decimal('bowl_economy')->default(0.0);
            $table->integer('total_stumpings')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_history');
    }
}
