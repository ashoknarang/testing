<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adminusers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',250)->default('');
            $table->enum('role', ['default', 'admin'])->default('default');
            $table->string('email',250)->default('');
            $table->string('password',250)->default('');
            $table->enum('status', ['active', 'deactivate'])->default('active');
            $table->string('image',250)->default('');
            $table->rememberToken()->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adminusers');
    }
}
