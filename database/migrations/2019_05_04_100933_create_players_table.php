<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id')->default(0);
            $table->string('first_name',200)->default('');
            $table->string('last_name',200)->default('');
            $table->string('full_name',250)->default('');
            $table->string('country',150)->default('');
            $table->integer('jersey_number')->default(0);
            $table->enum('type', ['batsman', 'bowler', 'wicket_keeper', 'wicket_keeper_batsman', 'all_rounder'])->default('batsman');
            $table->enum('status', ['activate', 'deactivate'])->default('activate');
            $table->enum('is_captain', ['yes', 'no'])->default('no');
            $table->string('profile_image',250)->default('');
            $table->text('description')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
