<?php
namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Players  extends Model
{
    

    protected $table = 'players';
    //protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['team_id', 'first_name', 'last_name','full_name','country','jersey_number','type','is_captain','','status', 'description', 'profile_image','updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


     public function team()
    {
      return $this->hasOne('App\Teams','id','team_id');
    }

    public function player_history()
    {
      return $this->belongsTo('App\PlayerHistory','id','player_id');
    }







}
