<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Jobs\SendEmail;
use Config,DB;
class ReminderSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public function handle()
    {
        $mutable = Carbon::now();
 
        $one_day = Carbon::now()->addDays(1)->toDateString();
        $two_day = Carbon::now()->addDays(2)->toDateString();
        $seven_day = Carbon::now()->addDays(7)->toDateString();
        $thirty_day = Carbon::now()->addDays(30)->toDateString();
 

        $subscriptions=\App\PlanSubscription::where(function($query) use ($one_day,$two_day,$seven_day,$thirty_day){

            $query->where(DB::raw("DATE_FORMAT(ends_at,'%Y-%m-%d')"), $one_day);  
            $query->orWhere('ends_at', '=', $two_day);  
            $query->orWhere('ends_at', '=', $seven_day);  
            $query->orWhere('ends_at', '=', $thirty_day);  
            
        })->get();


        
        foreach($subscriptions as $subscription){
                
                

              
                $user=\App\User::find($subscription->subscriber_id);


                $emaildata=array();
                $emaildata['email_template_type']=Config::get('constants.EMAIL_TEMPLATE_FOR_REMINDER_SUBSCRIPTION_END');

                    $emaildata['to']=$user->email;
                    $emaildata['tags']=array(
               
                    "[NAME]"=>$user->name,
                    "[ACCOUNTRENEWALDATE]"=>$subscription->ends_at,
                    
                    '[APP_NAME]'=>config('app.name'));

                // print_r($emaildata);

                dispatch(new SendEmail($emaildata));


              

        }
    }
}
