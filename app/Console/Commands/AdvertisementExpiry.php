<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Jobs\SendEmail;
use Config,DB;
use App\Advertisement, App\User;
class AdvertisementExpiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'advertisement:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public function handle()
    {
        $mutable = Carbon::now();
 
        $one_day = Carbon::now()->addDays(1)->toDateString();
        $three_day = Carbon::now()->addDays(3)->toDateString();
        $seven_day = Carbon::now()->addDays(7)->toDateString();
 

        $advertisements=Advertisement::where(function($query) use ($seven_day, $one_day, $three_day){
            $query->where('end_date', '=', $seven_day);  
            $query->orWhere('end_date', '=', $three_day);  
            $query->orWhere('end_date', '=', $one_day);
        })->get();
        
        foreach($advertisements as $advertisement){
              
          $user=User::find($advertisement->vendor_id);


          $emaildata=array();
          $emaildata['email_template_type']=Config::get('constants.EMAIL_TEMPLATE_ADVERTISEMENT_EXPIRY');

          $emaildata['to']=$user->email;
          $emaildata['tags']=array(

            "[NAME]"=>$user->name,
            "[ACCOUNTRENEWALDATE]"=>$advertisement->end_date,
            "[TITLE]"=>$advertisement->title,

            '[APP_NAME]'=>config('app.name'));

          dispatch(new SendEmail($emaildata));
        }
    }
}
