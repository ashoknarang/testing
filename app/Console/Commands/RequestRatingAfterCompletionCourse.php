<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Booking;
use Config;
use Carbon\Carbon;
use App\Jobs\SendEmail;

class RequestRatingAfterCompletionCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:requestratingaftercompletioncourse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $bookings=Booking::with(['user:id,email,name','vendor'=>function($query){
                $query->with('userdetails:organization_name,user_id,id');
        }])->where(['status'=>Config::get('constants.BOOKING_STATUS_PAID'),'is_request_for_rating'=>0])->where('end_date', '<', Carbon::now()->toDateString())->get();



        foreach($bookings as $booking){
                $updatebooking=new Booking;

                        
                $emaildata=array();
                $emaildata['email_template_type']=Config::get('constants.REQUEST_FOR_RATING_AFTER_COURSE_COMPLETION_DATE');
                $emaildata['to']=$booking->user->email;
                
                $emaildata['tags']=array("[NAME]"=>$booking->user->name,'[ORGANIZATIONNAME]'=>$booking->vendor->userdetails->organization_name,'[APP_NAME]'=>config('app.name'));


                dispatch(new SendEmail($emaildata));


                $updatebooking->where('id',$booking->id)->update(['is_request_for_rating'=>1]);

        }


        $this->info('Word of the Day sent to All Users');
    }
}
