<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Booking;
use Config;
use App\Helper\Helpers;
use Carbon\Carbon;
use App\Jobs\SendEmail;

class ReminderEnrolledProgramStarting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $mutable = Carbon::now();
 
        $one_day = Carbon::now()->addDays(1)->toDateString();
        $two_day = Carbon::now()->addDays(2)->toDateString();
        $seven_day = Carbon::now()->addDays(7)->toDateString()   ;
 

        $bookings=Booking::with(['user:id,email,name','vendor'=>function($query){
                $query->with('userdetails:organization_name,user_id,id');
        },'course'=>function($query){
            $query->select('id','type');

        }])->where(['status'=>Config::get('constants.BOOKING_STATUS_PAID')])->where(function($query) use ($one_day,$two_day,$seven_day){

            $query->where('start_date', '=', $one_day);  
            $query->orWhere('start_date', '=', $two_day);  
            $query->orWhere('start_date', '=', $seven_day);  
        })->select('id','course_id','order_no','course_id','batch_id','user_id','vendor_id','start_date','end_date','start_time','end_time','duration','repeat_days')->get();

        

        foreach($bookings as $booking){
                $updatebooking=new Booking;

                

                $days_str="";
                if($booking->repeat_days!=''){

                    $days=Config::get('services.days');

                  $batch_weekdays=explode(",", $booking->repeat_days);
                  
                  if(count($batch_weekdays)>0){
                    foreach ($batch_weekdays as $day) {
                      $shw_days_arr[]=$days[$day];                    
                    }

                  }
                  $days_str=is_array($shw_days_arr) ? implode(",", $shw_days_arr) : ''; 
                  
                }


                $emaildata=array();
                $emaildata['email_template_type']=Config::get('constants.REQUEST_FOR_RATING_AFTER_COURSE_COMPLETION_DATE');

                    $emaildata['to']=$booking->user->email;
                    $emaildata['tags']=array(
               
                    "[NAME]"=>$booking->user->name,
                    "[COURSE_NAME]"=>$booking->course->title,
                    "[COURSESTARTDATE]"=>$booking->start_date,
                    "[ENROLLEDID]"=>$booking->order_no,
                    "[BOOKINGTYPE]"=>Helpers::getBookedCourseType($booking->course->type),
                    "[BATCHSTARTDATE]"=>$booking->start_date,
                    "[BATCHENDDATE]"=>$booking->end_date,
                    "[BATCHSTARTTIME]"=>$booking->start_time,
                    "[BATCHENDTIME]"=>$booking->end_time,
                    "[BATCHDAYS]"=>$days_str,
                    '[ORGANIZATIONNAME]'=>$booking->vendor->userdetails->organization_name,
                    '[APP_NAME]'=>config('app.name'));

                // print_r($emaildata);

                dispatch(new SendEmail($emaildata));


                $updatebooking->where('id',$booking->id)->update(['is_request_for_rating'=>1]);

        }

    }
}
