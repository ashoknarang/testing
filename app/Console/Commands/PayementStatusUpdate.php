<?php

namespace App\Console\Commands;

use App\Booking;
use Carbon\Carbon;
use Config;
use Illuminate\Console\Command;

class PayementStatusUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:updatestatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update booking status for the payment in process course ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $booking = Booking::where(['status' => Config::get('constants.BOOKING_STATUS_PAYMENT_PANDING')])->where('created_at', '<', Carbon::now()->subMinutes(1)->toDateTimeString());

        $booking->update(['status' => Config::get('constants.BOOKING_STATUS_PAYMENT_CANCEL')]);
        $this->info($courses);
        //print_r($bookings);
    }
}
