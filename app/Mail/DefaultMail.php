<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Auth;
use Config;
class DefaultMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
         
        $this->data    = $data; 
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
       

       
        $email=\App\EmailTemplate::find($this->data['email_template_type']);
        
        $content=$email->content;


        $subject=isset($email->subject) ? $email->subject : "";


        if(isset($this->data['tags']) && is_array($this->data['tags'])){

            foreach($this->data['tags'] as $tkey=>$tval){



              $content=str_replace($tkey,$tval,$content);


              
            }

        }
        

    
        
        $this->data['content']=html_entity_decode($content);

        

        
 
        return $this->from('admin@mychatri.com',"Mychatri")->subject($subject)->markdown('emails.default',$this->data);
 
 
       
       

    }
}
