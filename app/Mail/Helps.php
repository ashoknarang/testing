<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Auth;
use Config;
class Helps extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$role=0)
    {
        $this->email   = $data['email'];;
        $this->name    = $data['name'];;
        $this->id      = $data['msg_id'];
        $this->subject = $data['subject'];
        $this->message = $data['message'];
        $this->role    = $role; 
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        if($this->role==Config::get('constants.USER_TYPE_ADMIN'))
        {
        return $this->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME','Mychhatri'))->markdown('emails.email_to_admin',['first_name' => $this->name,"id"=>$this->id,"subject"=> $this->subject,"message"=>$this->message,"email"=>$this->email]);
        }
        else
        {
       return $this->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME','Mychhatri'))->markdown('emails.help',['first_name' => $this->name,"id"=>$this->id]);
        }

    }
}
