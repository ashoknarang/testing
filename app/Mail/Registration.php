<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

//use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
//implements ShouldQueue
class Registration extends Mailable 
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user;
    protected $otp;
    protected $markdown;
    protected $entity_type;

    public function __construct(User $user, $otp = null, $entity_type, $markdown= null)
    {
        $this->user = $user;
        $this->otp = $otp;
        $this->markdown = $markdown;
        $this->entity_type = $entity_type;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $markdown_template = '';
        switch ($this->markdown) {
            case 'email_verification':
                $markdown_template = 'emails.users.registration';
                break;
            // case 'registration':
            //     return 'emails.users.registration';
            //     break;
            // case 'registration':
            //     return 'emails.users.registration';
            //     break;
            default:
                $markdown_template = 'emails.users.registration';
                break;
        }

        // echo $this->entity_type;
        // die;
        if($this->entity_type == 'user')
        {       
           
                if($this->user->role==2 || $this->user->role=='2' )
                {
                    return $this->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME','Mychhatri'))->markdown($markdown_template,[
                                'title' => $this->user->title,
                                'name' => $this->user->name,
                                'email' => $this->user->email,
                                'username' => $this->user->username,
                                'mobile' => $this->user->mobile,
                                'registration_number' => $this->user->registration_number,
                                'otp' => $this->otp,
                                'email_token' => $this->user->email_token,
                                'user_type' => $this->user->user_type,
                                'key' => $this->entity_type,
                            ]);
                }
                elseif($this->user->role ==3)
                {
                    return $this->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME','Mychhatri'))->markdown($markdown_template,[
                                'title' => $this->user->title,
                                'name' => $this->user->name,
                                'email' => $this->user->email,
                                'username' => $this->user->username,
                                'mobile' => $this->user->mobile,
                                'registration_number' => $this->user->registration_number,
                                'otp' => $this->otp,
                                'email_token' => $this->user->email_token,
                                'user_type' => $this->user->user_type,
                                'key' => $this->entity_type,
                            ]);  
                }
                elseif($this->user->role ==4 )
                {
                    return $this->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME','Mychhatri'))->markdown($markdown_template,[
                                'title' => $this->user->title,
                                'name' => $this->user->name,
                                'email' => $this->user->email,
                                'username' => $this->user->username,
                                'mobile' => $this->user->mobile,
                                'registration_number' => $this->user->registration_number,
                                'otp' => $this->otp,
                                'email_token' => $this->user->email_token,
                                'user_type' => $this->user->user_type,
                                'key' => $this->entity_type,
                            ]);  
                } 
        }
    }
}
