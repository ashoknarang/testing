<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
         
      $this->first_name =    $data['first_name'];
      $this->last_name  =    $data['last_name'];
      $this->email     =     $data['email'];
      $this->mobile    =     $data['mobile'];
      $this->message   =     $data['message'];

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email ,env('MAIL_FROM_NAME','Mychhatri'))->markdown('emails.contactUs',[
                                'first_name' => $this->first_name,'last_name'=>$this->last_name,'email'=>$this->email,'mobile'=>$this->mobile,'message'=>$this->message]);
    }
}
