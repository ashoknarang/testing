<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class verifyEmailOtp extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
         
      
      $this->otp   =     $data['otp'];
      $this->email     =     $data['email'];  
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email ,env('MAIL_FROM_NAME','Mychhatri'))->markdown('emails.cerifyEmailOtp',[
                                'otp'=>$this->otp]);
    }
}
