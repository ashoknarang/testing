<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;
use DB;

class User extends Authenticatable 
{
    use  Notifiable;
  
    

    protected $table = 'adminusers';
    //protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

         'name', 'email', 'password', 'mobile', 'gender', 'profile_image','status','is_deleted', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    protected $hidden = ['password', 'remember_token'];

   








}
