<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mail;
use App\Mail\Registration;

class SendVerificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user;
    protected $otp;
    protected $markdown;
    protected $entity_type;

    public function __construct($user, $otp = null, $entity_type, $markdown)
    {
        $this->user = $user;
        $this->otp = $otp;
        $this->markdown = $markdown;
        $this->entity_type = $entity_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new Registration($this->user, $this->otp, $this->entity_type, $this->markdown);

        Mail::to($this->user->email)->send($email);
    }
}
