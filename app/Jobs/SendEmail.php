<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mail;
use Swift_Mailer;
use Swift_SmtpTransport;
use App\Mail\DefaultMail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data=array();
   

    public function __construct($data)
    {   


        $this->data = $data;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   

        $email = new DefaultMail($this->data);
 
       
        $backup = Mail::getSwiftMailer();
      
      
        // Setup your gmail mailer

        $template=\App\EmailTemplate::find($this->data['email_template_type']);
       

        if(isset($template->smtp_id) && is_numeric($template->smtp_id) && $template->smtp_id>0 ){

            // print_r($this->data);
            // die;

            $smtp_details=\App\Smtp::find($template->smtp_id);
            if($smtp_details){

                $transport = new \Swift_SmtpTransport('smtp.gmail.com', 587, 'tls');
                $transport->setUsername($smtp_details->email);
                $transport->setPassword($smtp_details->password);
                // Any other mailer configuration stuff needed...

                $gmail = new Swift_Mailer($transport);

                // Set the mailer as gmail
                Mail::setSwiftMailer($gmail);

                // Send your message
                Mail::to($this->data['to'])->send($email);

                // Restore your original mailer
                Mail::setSwiftMailer($backup);

            }else{

                if(isset($this->data['cc']))
                {
                     Mail::to($this->data['to'])->cc($this->data['cc'])->send($email);
                }
                else
                {
                     Mail::to($this->data['to'])->send($email);
                }
            }
        
          
        }else{

            if(isset($this->data['cc']))
            {
                 Mail::to($this->data['to'])->cc($this->data['cc'])->send($email);
            }
            else
            {
                 Mail::to($this->data['to'])->send($email);
            }

 
        }


       
    }
}
