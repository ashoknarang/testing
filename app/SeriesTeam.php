<?php
namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class SeriesTeam  extends Model
{
    

    protected $table = 'series_teams';
    //protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['series_id', 'team_id', 'total_points','net_runrate','updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function series()
    {
      return $this->belongsTo('App\Series','series_id','id');
    }

    

    public function teams()
    {
      return $this->hasOne('App\Teams','id','team_id');
    }

    public function matches()
    {
      return $this->hasMany('App\matches','series_id','series_id');
    }

    public function winMatches()
    {
      return $this->hasOne('App\matches','series_id','series_id')->where('status','completed');
    }
    
    public function lossMatches()
    {
      return $this->hasOne('App\matches','series_id','series_id')->where('status','completed');
    }

  







}
