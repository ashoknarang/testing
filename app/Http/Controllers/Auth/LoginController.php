<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Illuminate\Http\Request;
use Validator,Config;
use Session;
use URL;
use App\CertificateAssigned;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
       
    }


    public function login(Request $request)
    {

        $email = $request->input('email');
        $password = base64_decode($request->input('password'));
         
        $validator = Validator::make($request->all(), [
            'email_or_mobile' => 'required',
            'password' => 'required|max:255'            
        ])->validate();
/**************for remember me for user*******************/
       $remember_me = $request->get('remember'); 
        if(!empty($remember_me)) {
                setcookie ("email_or_mobile",$request->input('email_or_mobile'),time()+ (6 * 30 * 24 * 60 * 60));
                setcookie ("password",base64_decode($request->input('password')),time()+ (6 * 30 * 24 * 60 * 60));
            } else {
                if(isset($_COOKIE["email_or_mobile"])) {
                    setcookie ("email_or_mobile","");
                }
                if(isset($_COOKIE["password"])) {
                    setcookie ("password","");
                }
            }
/**************for remember me *******************/

/**************for remember me for admin*******************/
       $remember_admin = $request->get('admin_remember'); 
        if(!empty($remember_admin)) 
        {
            setcookie ("admin_email",$request->input('email_or_mobile'),time()+ (6 * 30 * 24 * 60 * 60));
            setcookie ("admin_password",base64_decode($request->input('password')),time()+ (6 * 30 * 24 * 60 * 60));
        } else {
                if(isset($_COOKIE["admin_email"])){
                    setcookie ("admin_email","");
                }
                if(isset($_COOKIE["admin_password"])) {
                    setcookie ("admin_password","");
                }
        }
/**************for remember me *******************/
        $login_data=array('password'=>base64_decode($request->input('password')));

        $field = filter_var($request->input('email_or_mobile'), FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';

        $field_value =$request->input('email_or_mobile');

      
        

        // if($role== Config::get('constants.USER_TYPE_ANALYSIS_MANAGER'))
        // {

          

        //     $login_data['role']=$request->input('role');
             
        // }
       
       

        
        $login_data[$field]=$field_value;

      
        $request->merge([$field => $request->input('email_or_mobile')]);


        $st=\Auth::attempt($login_data,$remember_me);
        
         if ($st){
 


         
 
                  return redirect('/admin/dashboard');
               
          

    
 

        } else{
           
                return Redirect::back()->withErrors([__("email_or_password_does_not_match")]); 
        }
    }
    
    
    public function logout(Request $request)
    {


        if(Auth::check())
        {
           
            $userRole = Auth::user()->role;
    
            Auth::logout();
           session_unset();
            
                return redirect('/admin');
            
        
            
        }else{
            Session::flush();
           return redirect('/');
        }



    }

public function redirectTo()
{
        
   if(Auth::check())
        {
           
        $userRole = Auth::user()->role;
    
        Auth::logout();
        if($userRole ==1 || $userRole ==4)
        {
        return redirect('/admin');
        }
        if($userRole ==2 || $userRole ==3 || $userRole ==4)
        {
        return redirect('/');
        }

 
        }
}
    
public function doCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch), true);
        curl_close($ch);
        return $data;
        }

public function fblogin(Request $request){
        $input =$request->all();
        $app_id = Config::get('constants.FACEBOOK_APP_ID');
        $secret = Config::get('constants.ACCOUNT_KIT_APP_SECRET');
        $version = Config::get('constants.ACCOUNT_KIT_API_VERSION'); // 'v1.1' for example

        // Method to send Get request to url
        

        // Exchange authorization code for access token
        $token_exchange_url = 'https://graph.accountkit.com/'.$version.'/access_token?'.
        'grant_type=authorization_code'.
        '&code='.$_POST['code'].
        "&access_token=AA|$app_id|$secret";
        $data = doCurl($token_exchange_url);
        $user_id = $data['id'];
        $user_access_token = $data['access_token'];
        $refresh_interval = $data['token_refresh_interval_sec'];

        // Get Account Kit information
        $me_endpoint_url = 'https://graph.accountkit.com/'.$version.'/me?'.
        'access_token='.$user_access_token;
        $data = doCurl($me_endpoint_url);
        $phone = isset($data['phone']) ? $data['phone']['number'] : '';
        $email = isset($data['email']) ? $data['email']['address'] : '';
}



}
