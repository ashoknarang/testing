<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Illuminate\Http\Request;
use Validator;
use ResetsPasswords;
use SendsPasswordResetEmails;
// use Illuminate\Foundation\Auth\ResetsPasswords;

// use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
// use Password;

class VendorLoginController extends Controller
{

    

    // protected $guard = 'vendors';
    // protected $broker = 'vendors';

    public function __construct()
    {
      $this->middleware('guest');
    }

    // protected function guard()
    // {
    //     return Auth::guard('vendors');
    // }

    //defining which password broker to use, in our case its the vendors
    // protected function broker() {
    //     return Password::broker('vendors');
    // }

    public function showLoginForm()
    {
      return view('vendors.login');
    }
    public function login(Request $request)
    {

      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        // if successful, then redirect to their intended location
        return redirect()->route('dashboard');
      } else{
        return Redirect::back()->withErrors(['Email or Password Not match']);
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email'));
    }


    //Password reset for vendors
    public function getEmail()
    {

        return $this->showLinkRequestForm();
    }


    public function showLinkRequestForm()
    {
        if (property_exists($this, 'linkRequestView')) {
            return view($this->linkRequestView);
        }

        if (view()->exists('vendors.auth.passwords.email')) {
            return view('vendors.auth.passwords.email');
        }

        return view('vendors.auth.passwords.email');
    }

    public function showResetForm(Request $request, $token = null)
    {

        if (is_null($token)) {
            return $this->getEmail();
        }
        $email = $request->input('email');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email'));
        }

        if (view()->exists('vendors.auth.passwords.reset')) {
            return view('vendors.auth.passwords.reset')->with(compact('token', 'email'));
        }

        return view('vendors.auth.passwords.reset')->with(compact('token', 'email'));
    }
    
}