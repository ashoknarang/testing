<?php

namespace App\Http\Controllers\Admin;

use App\Advertisement;
use App\Booking;
use App\City;
use App\Course;
use App\Help;
use App\Helper\Helpers;
use App\Http\Controllers\Controller;
use App\State;
use App\Testimonial;
use App\User;
use Auth;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $current_data = date('Y-m-d H:i:s');
        $one_year_past_date = date('Y-m-d H:i:s', strtotime('-1 year'));
        $one_month_past_date = date('Y-m-d H:i:s', strtotime('-1 month'));

       
       return view('admin.welcome');
    }

    public function getState(Request $request)
    {

        $country_id = $request->input('country_id');
        $state_id = $request->input('state_id') ? $request->input('state_id') : 0;
        $with_defaul_option = $request->input('with_defaul_option');
        if (isset($with_defaul_option)) {
            $html = '<option value="">Please choose a state</option>';
        } else {
            $html = '<option value="0">Please choose a state</option>';
        }
        $stateDetails = State::where('country_id', '=', $country_id)->where('status', 1)->get();
        foreach ($stateDetails as $stateDetail) {

            $sel = "";
            if ($state_id == $stateDetail->id) {
                $sel = "selected='selected'";

            }

            $html .= '<option value="' . $stateDetail->id . '" ' . $sel . ' >' . $stateDetail->name . '</option>';
        }
        echo $html;
    }

    public function getCity(Request $request)
    {
        $state_id = $request->input('state_id');
        $city_id = $request->input('city_id');
        $country_id = $request->input('country_id');
        $with_defaul_option = $request->input('with_defaul_option');
        
        if (isset($with_defaul_option)) {
            $html = '<option value="">Please choose a city</option>';
        } else {
            $html = '<option value="0">Please choose a city</option>';
        }
        $cityDetails = City::where('state_id', '=', $state_id)->where('status', 1)->get();
        foreach ($cityDetails as $cityDetail) {

            $sel = "";
            if ($city_id == $cityDetail->id) {
                $sel = "selected='selected'";

            }

            $html .= '<option value="' . $cityDetail->id . '" ' . $sel . '>' . $cityDetail->name . '</option>';
        }
        echo $html;
    }

    public function getCollege(Request $request)
    {

        $city_id = $request->input('city_id');
        $college_id = $request->input('college_id');
        $with_defaul_option = $request->input('with_defaul_option');
        if (isset($with_defaul_option)) {
            $html = '<option value="">Please choose a college</option>';
        } else {
            $html = '<option value="0">Please choose a college</option>';
        }
        $colleges = \App\Colleges::where('city_id', '=', $city_id)->orderBy('name', 'ASC')->get();
        foreach ($colleges as $college) {

            $sel = "";
            if ($college_id == $college->id) {
                $sel = "selected='selected'";
            }

            $html .= '<option value="' . $college->id . '" ' . $sel . '>' . $college->name . '</option>';
        }
        echo $html;
    }
    public function getCollegeName(Request $request)
    {

        $city_id = $request->input('city_id');
        $college_id = $request->input('college_id');
        $html = array();

        $colleges = \App\Colleges::where('city_id', '=', $city_id)->get();
        foreach ($colleges as $college) {
            array_push($html, $college->name);
        }
        return response($html);
    }

}
