<?php
namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use Mail;
use App\Teams;
use App\Helper\Helpers;
use Illuminate\Validation\Rule;

use Config;
use Gate;
 
class TeamsController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth'); 

             
    }
      

   
   
     // return Excel::download(new UsersExport, 'users.xlsx');

    public function addeditteam($id="",Request $request){
        

          
         

         // $this->data['states'] = State::All();
        // $this->data['cities'] = City::All();
        $this->data['id'] = $id;
        
         

        $this->data['errors'] =array();
        $input = $request->all();
        // print_r($input);
      
        $title = "Add ";
        $this->data['user_status'] =Config::get("services.user_status");
        if( is_numeric($id) && $id>0){
            $this->data['team_details'] =$team_details = Teams::where('id','=',$id)->first();              
             $title = "Edit ";    
       }


        $title.="Teams";


 

        $this->data['title']=$title; 
        
      if($request->isMethod('post')){

      ///  $user_details->syncPermissions(['trainer-list', 'request-admin']);
      


        $validator = Validator::make($request->all(),[
         'team_name' => 'required',
        
        ]);
       
        


        if(!$validator->fails()){
           
           
          
            $data['team_name'] = isset($input['team_name'] ) ? trim($input['team_name']):'';    
            $data['club_name'] = isset($input['club_name']) ? $input['club_name'] :''; 
            $data['description'] = isset($input['description']) ? $input['description']:'';
            $data['status'] = isset($input['status']) ? $input['status'] :'';
           
        

            
            $team = Teams::updateOrCreate(['id'=>$id],$data);


          

            if($team){

            
              $url = '';
              //$user_details->removeRole('vendor');
              if($request->has('image')) 
              {
               
                $extension = $request->file('image')->getClientOriginalExtension();
                $dir = 'images/team/';
                $filename = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
                $req=$request->file('image')->move($dir, $filename);


                $url = 'images/team/'.$filename;
                Teams::where("id",$team->id)->update(array('image' =>$url));
              }
              
           


                if($request->hasFile('image') && $team && $team->image!='' && file_exists(base_path()."/".$team->image)  && strpos($team->image,"/team/")!=false ){
                

                 if(!unlink($team->image)){
                    return redirect('teams')->with('success', 'team updated failed.');
                 }

                } 
          // }
               
          }

          //  dispatch(new SendVerificationEmail($user, null, 'user', 'email_verification'));

            return redirect('/admin/teams' )->with('status', 'Team updated!');
             
            //return \Redirect::back()->withWarning('Register completed');
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      
      return view('admin.teams.teamadd',$this->data);
    } 


 
   

    public function teamsList(Request $request)
    {        


        $this->data=array();
     
        $title="";


        return view('admin.teams.teamslist',$this->data);
    }
   
    public function xhr(Request $request){

      $input = $request->all();

      $cmd=isset($input['cmd']) ? $input['cmd']  : "";

      switch ($cmd) {
        case 'list':
          
          $this->getTeamList($input);
          break;
      
          case 'delete':
          
          $this->deleteTeam($input);
          break;
          case 'team_status':
          
          $this->updateTeamStatus($input);
          break;
          
          
          
        default:
          # code...
          break;
      }
 

    }

    private function getTeamList($input_data){



      $condition=[];
     
      $limit=isset($input_data['jtPageSize']) ? $input_data['jtPageSize'] : '10';
      $offset=isset($input_data['jtStartIndex']) ? $input_data['jtStartIndex'] : '0';
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
     


      //$condition['status1']=1;
      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }
      
      $teams = Teams::where($condition)

        ->where(function($query){

            $query->where(function($query) {

              if(Input::post('name') &&  Input::post('name') !=''){
                $query->where('team_name','like','%'.Input::post('name').'%')
                ->orWhere('club_name', 'like', '%'.Input::post('name') . '%');
               

              }
            });

        })
       

        ->skip($offset)->take($limit)->orderBy($order_by_key,$order_by_val)->get();

       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $teams_count=Teams::where($condition)
        ->where(function($query)
              { 


                $query->where(function($query) {
                  if(Input::post('name')!=''){
                      $query->where('team_name','like',Input::post('name').'%')
                      ->orWhere('club_name', 'like', Input::post('name') . '%');  
                  }
                });
        })
        ->count();

       echo Helpers::responseJson(array("Result"=>"OK","Records"=>$teams,"TotalRecordCount"=>$teams_count));


    } 

   
   

    private function deleteTeam($input_data){

      $team = Teams::find($input_data['team_id']);
     
      if($team->delete()){
      
       echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"Team Delete successfully!!!"));
        
      }
    }


}
?>