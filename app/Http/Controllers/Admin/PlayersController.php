<?php
namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use Mail;
use App\Players,App\PlayerHistory;
use App\Helper\Helpers;
use Illuminate\Validation\Rule;

use Config;
use Gate;
 
class PlayersController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth'); 

             
    }
      

   
   
     // return Excel::download(new UsersExport, 'users.xlsx');

    public function addeditplayer($id="",Request $request){
        

          
         

         // $this->data['states'] = State::All();
        // $this->data['cities'] = City::All();
        $this->data['id'] = $id;
        
         

        $this->data['errors'] =array();
        $input = $request->all();
        // print_r($input);
      
        $title = "Add ";
        $this->data['user_status'] =Config::get("services.user_status");
        $this->data['player_type'] =Config::get("services.player_type");

        $this->data['teams'] =\App\Teams::where('status','activate')->get();


        if( is_numeric($id) && $id>0){
            $this->data['player_details'] =$player_details = Players::where('id','=',$id)->first();              
             $title = "Edit ";    
       }


        $title.="Players";


 

        $this->data['title']=$title; 
        
      if($request->isMethod('post')){

      ///  $user_details->syncPermissions(['trainer-list', 'request-admin']);
      


        $validator = Validator::make($request->all(),[
         'first_name' => 'required',
        
        ]);
       
        


        if(!$validator->fails()){
           
           
          
            $data['team_id'] = isset($input['team_id'] ) ? trim($input['team_id']):0;    
            $data['first_name'] = isset($input['first_name'] ) ? trim($input['first_name']):'';    
            $data['last_name'] = isset($input['last_name'] ) ? trim($input['last_name']):'';    
            $data['full_name'] = isset($input['full_name'] ) ? trim($input['full_name']):'';    
            $data['country'] = isset($input['country'] ) ? trim($input['country']):'';    
            $data['jersey_number'] = isset($input['jersey_number'] ) ? trim($input['jersey_number']):'';    
            $data['type'] = isset($input['type'] ) ? trim($input['type']):'';    
            $data['is_captain'] = isset($input['is_captain']) ? $input['is_captain'] :''; 
            $data['description'] = isset($input['description']) ? $input['description']:'';
            $data['status'] = isset($input['status']) ? $input['status'] :'';
           
        
            $player = Players::updateOrCreate(['id'=>$id],$data);


          

            if($player){

            
              $url = '';
              //$user_details->removeRole('vendor');
              if($request->has('profile_image')) 
              {
               
                $extension = $request->file('profile_image')->getClientOriginalExtension();
                $dir = 'images/players/';
                $filename = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
                $req=$request->file('profile_image')->move($dir, $filename);


                $url = 'images/players/'.$filename;
                Players::where("id",$player->id)->update(array('profile_image' =>$url));
              }
              
           


                if($request->hasFile('profile_image') && $player && $player->profile_image!='' && file_exists(base_path()."/".$player->profile_image)  && strpos($player->profile_image,"/players/")!=false ){
                

                 if(!unlink($player->profile_image)){
                    return redirect('players')->with('success', 'Player updated failed.');
                 }

                } 
          // }
               
          }

          //  dispatch(new SendVerificationEmail($user, null, 'user', 'email_verification'));

            return redirect('/admin/players' )->with('status', 'Player updated!');
             
            //return \Redirect::back()->withWarning('Register completed');
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      
      return view('admin.players.playeradd',$this->data);
    } 


    public function playerHistory($player_id="",Request $request){
       
         // $this->data['states'] = State::All();
        // $this->data['cities'] = City::All();
        ///$this->data['id'] = $id;
        $this->data['player_id'] = $player_id;
        
         

        $this->data['errors'] =array();
        $input = $request->all();
        // print_r($input);
      
        
     

        if( is_numeric($player_id) && $player_id>0){
            $this->data['player_history_details'] =$player_history_details = PlayerHistory::where('player_id','=',$player_id)->with('player:full_name,id')->first();              
               
       }

        $title = "Add ";

        $title=$player_history_details->player->full_name."::Player History";


 

        $this->data['title']=$title; 
        
      if($request->isMethod('post')){

      ///  $user_details->syncPermissions(['trainer-list', 'request-admin']);
      


        $validator = Validator::make($request->all(),[
         'player_id' => 'required',
        
        ]);
       
        


        if(!$validator->fails()){
           
           
          
            $data['player_id'] = isset($input['player_id'] ) ? trim($input['player_id']):0;    
            $data['matches_played'] = isset($input['matches_played'] ) ? trim($input['matches_played']):0;    
            $data['batting_innings'] = isset($input['batting_innings'] ) ? trim($input['batting_innings']):0;    
            $data['total_runs'] = isset($input['total_runs'] ) ? trim($input['total_runs']):0;    
            $data['high_score'] = isset($input['high_score'] ) ? trim($input['high_score']):0;    
            $data['fifties'] = isset($input['fifties'] ) ? trim($input['fifties']):0;    
            $data['hundreds'] = isset($input['hundreds'] ) ? trim($input['hundreds']):0;    
            $data['notouts'] = isset($input['notouts']) ? $input['notouts'] :'0'; 
            $data['bat_avg'] = isset($input['bat_avg']) ? $input['bat_avg']:'0';
            $data['bowl_avg'] = isset($input['bowl_avg']) ? $input['bowl_avg'] :'0';
            $data['total_wickets'] = isset($input['total_wickets']) ? $input['total_wickets'] :'0';
            $data['total_stumpings'] = isset($input['total_stumpings']) ? $input['total_stumpings'] :'0';
            $data['bowl_economy'] = isset($input['bowl_economy']) ? $input['bowl_economy'] :'0';
           
        
            $player = PlayerHistory::updateOrCreate(['player_id'=>$player_id],$data);
            return redirect('/admin/player/player_history/'.$player_id)->with('status', 'Player History  updated!');
             
            //return \Redirect::back()->withWarning('Register completed');
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      
      return view('admin.players.playerHistory',$this->data);
    } 


   

    public function playersList(Request $request)
    {        


        $this->data=array();
     
        $title="";


        return view('admin.players.playerslist',$this->data);
    }
   
    public function xhr(Request $request){

      $input = $request->all();

      $cmd=isset($input['cmd']) ? $input['cmd']  : "";

      switch ($cmd) {
        case 'list':
          
          $this->getPlayersList($input);
          break;
      
          case 'delete':
          
          $this->deletePlayer($input);
          break;
       
          
        default:
          # code...
          break;
      }
 

    }

    private function getPlayersList($input_data){



      $condition=[];
     
      $limit=isset($input_data['jtPageSize']) ? $input_data['jtPageSize'] : '10';
      $offset=isset($input_data['jtStartIndex']) ? $input_data['jtStartIndex'] : '0';
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
     


      //$condition['status1']=1;
      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }
      
      $players = Players::where($condition)
        ->with('team:id,team_name')
        ->where(function($query){

            $query->where(function($query) {

              if(Input::post('name') &&  Input::post('name') !=''){
                $query->where('first_name','like','%'.Input::post('name').'%')
                ->orWhere('last_name', 'like', '%'.Input::post('name') . '%')
                ->orWhere('full_name', 'like', '%'.Input::post('name') . '%');
               

              }
            });

        })
       

        ->skip($offset)->take($limit)->orderBy($order_by_key,$order_by_val)->get();

       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $players_count=Players::where($condition)
        ->where(function($query)
              { 


                $query->where(function($query) {
                  if(Input::post('name')!=''){
                      $query->where('full_name','like',Input::post('name').'%')
                      ->orWhere('first_name', 'like', Input::post('name') . '%');  
                  }
                });
        })
        ->count();

       echo Helpers::responseJson(array("Result"=>"OK","Records"=>$players,"TotalRecordCount"=>$players_count));


    } 

   
   

    private function deleteTeam($input_data){

      $team = Teams::find($input_data['team_id']);
     
      if($team->delete()){
      
       echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"Team Delete successfully!!!"));
        
      }
    }


}
?>