<?php
namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Helper\Helpers;
use App\Country;
use App\Colleges;
use Illuminate\Validation\Rule;
use App\User,App\Languages,App\EmailTemplate;
use Config;
use Gate;
use App\Jobs\SendEmail;

class EmailController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');        
    }
    
    public function sendMail(Request $request){
      
      if($request['user_type']=='students'){
        $query = User::where('role','3');
      }
      elseif($request['user_type']=='vendors'){
        $query = User::where('role','4');
      }
      if($request['name']!=''){
        $query = $query->where('name','LIKE','%'.$request['name'].'%');
      }
      if($request['status']!=''){
        $query = $query->where('status',$request['status']);
      }
      if($request['country']!=''){
        $query = $query->where('country_id',$request['country']);
      }
      if($request['state']!='' && $request['state']!='0'){
        $query = $query->where('state_id',$request['state']);
      }
      if($request['city']!='' && $request['city']!='0'){
        $query = $query->where('city_id',$request['city']);
      }
      if($request['college']!='' && $request['college']!='0'){
        $query = $query->where('college_id',$request['college']);
      }
      
      $users = $query->select('email','name')->get();
      
      foreach($users as $user){

        $data=array();
        $data['email_template_type']=$request['email_temp'];

        $data['to']=$user->email;
        $data['tags']=array("[NAME]"=>$user->name);
        dispatch(new SendEmail($data));
      }

    }

    public function selectedSendMail(Request $request){
      
      $users = $request['rows'];
      
      foreach($users as $user){

        $data=array();
        $data['email_template_type']=$request['email_temp'];

        $data['to']=$user['email'];
        $data['tags']=array("[NAME]"=>$user['name']);
        dispatch(new SendEmail($data));
      }

    }

    public function emailTemplates(Request $request)
    {
        
        $this->data['state_id'] =$state_id= 0;
        $this->data['city_id'] =$city_id= 0;
        $this->data=array();
        // echo $input = $request->segment(3);
        // die;
        

        
        // $vendor_details = User::

        // join('users',function($join){
        //     $join->on('users.id','=','vendors.vendor_id');
        // })->orderBy('vendors.vendor_id')->where('users.is_deleted','=',0)->paginate(15);
        // $this->data['vendors'] = $vendor_details;


        return view('admin.emails.emailtemplatelist',$this->data);
    }


    public function addeditemailtemplate($id="",Request $request){
        

        
       

            $user = User::find(Auth::id());
            $this->data['user']=$user;
            $this->data['vendors'] =array();
            if($user->role!=Config::get('constants.USER_TYPE_VENDOR')){
              
               
            }
       
        
      
        // $this->data['states'] = State::All();
        // $this->data['cities'] = City::All();
        $this->data['id'] = $id;
        
        $this->data['smtp']=$smtp=\App\Smtp::where('status',Config::get('constants.COMMON_STATUS_ACTIVE'))->get();
         
       
        $this->data['errors'] =array();
        $input = $request->all();

        
        

       
 
        $title = "Add Template";

     

        if( is_numeric($id) && $id>0){


            $emailtemplate = EmailTemplate::where('id','=',$id)->first();
            $this->data['emailtemplate']=$emailtemplate;  
            $title = "Edit Template";             
        }
      

     
        $this->data['title']=$title; 
         

      $this->data['languages']=$languages=Languages::where('status','1')->get();


      if($request->isMethod('post')){


        

        $validator = Validator::make($request->all(),[
        
        
        ]);
       
        


        if(!$validator->fails()){
            $data['email_template_for'] = isset($input['email_template_for']) ? $input['email_template_for'] : '';
            $data['tags'] = isset($input['tags']) ? $input['tags'] :'';
            $data['status'] =   isset($input['status'])? $input['status']:'1';
            $data['smtp_id'] =   isset($input['smtp_id'])? $input['smtp_id']:'0';

            $data['created_at'] = \Carbon\Carbon::now();
            $data['updated_at'] = \Carbon\Carbon::now();

        
          
            $template = EmailTemplate::updateOrCreate(['id'=>$id],$data);

            foreach($input['subject']  as $key=>$val){
              $translation_data=array();
              $course_translation = new \App\EmailTemplateTranslation;

              $translation_data['subject'] = $input['subject'][$key];
              $translation_data['content'] = isset($input['content'][$key]) ? $input['content'][$key] :'' ;
              $translation_data['email_template_id'] = $template->id;
              $translation_data['locale'] = $key;

              $interview_traslation=\App\EmailTemplateTranslation::updateOrCreate(['email_template_id'=>$template->id,'locale'=>$key],$translation_data);
            }
            return redirect('/admin/emailtemplate/')->with('status', 'Email template updated!');
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      return view('admin.emails.emailtemplateadd',$this->data);
    } 

    public function xhr(Request $request){

      $input = $request->all();

      $cmd=isset($input['cmd']) ? $input['cmd']  : "";

      switch ($cmd) {
        case 'list':
          
          $this->getEmailTemplateList($input);
          break;
          case 'delete':
          
          $this->deleteCourse($input);
          break;
          case 'course_status':
          
          $this->updateCourseStatus($input);
          break;

        default:
          # code...
          break;
      }
 

    }

    private function getEmailTemplateList($input_data){


      $condition=[];
     
      $limit=isset($input_data['jtPageSize']) ? $input_data['jtPageSize'] : '';
      $offset=isset($input_data['jtStartIndex']) ? $input_data['jtStartIndex'] : '';
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
  
      $user = User::find(Auth::id());

      if($user->role==Config::get('constants.USER_TYPE_VENDOR')){
        $condition['user_id']=$user->id;

      }

      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }
      

      $users = EmailTemplate::with('translations')->where($condition) 
 
       
        
      ->where(function($query)
      {

        if(trim(Input::post('name'))!=''){

            $query->whereTranslationLike('subject',Input::post('name').'%');
          }
          
          
      })->skip($offset)->take($limit)->orderBy($order_by_key,$order_by_val)->get();

       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $user_count=EmailTemplate::where($condition)
                  ->where(function($query)
                        {
                           if(trim(Input::post('name'))!=''){

            $query->whereTranslationLike('subject',Input::post('name').'%');
          }
                        })
                  ->count();

       echo Helpers::responseJson(array("Result"=>"OK","Records"=>$users,"TotalRecordCount"=>$user_count));


    }

    private function deleteCourse($input_data){

      $course = Course::find($input_data['course_id']);
    
      if($course->delete()){

       echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"User Delete successfully!!!"));
        
      }
    }
 

 


 

   private function updateCourseStatus($input_data) // common function 
   {
     $id =$input_data['course_id'];
    
     $status = Course::where('id', $id)->value('status');

     if($status==1)
     {
       Course::where('id', $id)->update(['status'=>0]);
       
     } 
     else
     {

        Course::where('id', $id)->update(['status'=>1]);
     
       
     }

     echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"Course Status updated successfully!!!"));


   }

   
   public function emailUsersList(Request $request)
    {
        $this->data['state_id'] =$state_id= 0;
        $this->data['city_id'] =$city_id= 0;
        $this->data['college_id'] =$college_id= 0;

        $this->data=array();
     
        $title="";

        $this->data['user_type']=$type=$user_type=$request->segment(3);
        


     

        if($type=="vendors" || $type==Config::get('constants.USER_TYPE_VENDOR') ){

          $title="vendor";
          
          $role=Config::get('constants.USER_TYPE_VENDOR');
         

        }elseif($type=="students" || $type==Config::get('constants.USER_TYPE_STUDENT') ){

          $title="student";
          $role=Config::get('constants.USER_TYPE_STUDENT');
          
         

        }
        $this->data['role'] =$role;


        $this->data['countries'] = Country::where('status',1)->get();
        $this->data['colleges'] = Colleges::all();
        $this->data['emailtemplates'] = EmailTemplate::all();
        if($user_type=="students"){
            $role=Config::get('constants.USER_TYPE_STUDENT');
        }elseif($user_type=="vendors"){
            $role=Config::get('constants.USER_TYPE_VENDOR');

        }
        $this->data['role'] = $role;


        if(Gate::denies('userlist')) {
          abort(403,'Unauthorized action.');
        }
        

        return view('admin.emails.emailuserslist',$this->data);
    }



}
?>