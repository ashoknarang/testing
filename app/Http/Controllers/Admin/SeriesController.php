<?php
namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use Mail;
use App\Series;
use App\Helper\Helpers;
use Illuminate\Validation\Rule;

use Config;
use Gate;
 
class SeriesController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth'); 

             
    }
      

   
   
     // return Excel::download(new UsersExport, 'users.xlsx');

    public function addeditseries($id="",Request $request){
        

          
         

         // $this->data['states'] = State::All();
        // $this->data['cities'] = City::All();
        $this->data['id'] = $id;
        
         

        $this->data['errors'] =array();
        $input = $request->all();
        // print_r($input);
      
        $title = "Add ";
        $this->data['series_status'] =Config::get("services.series_status");
        if( is_numeric($id) && $id>0){
            $this->data['series_details'] =$series_details = Series::where('id','=',$id)->first();              
             $title = "Edit ";    
       }


        $title.="Series";


 

        $this->data['title']=$title; 
        
      if($request->isMethod('post')){

      ///  $user_details->syncPermissions(['trainer-list', 'request-admin']);
      


        $validator = Validator::make($request->all(),[
         'series_title' => 'required',
        
        ]);
       
        


        if(!$validator->fails()){
           
           
          
            $data['series_title'] = isset($input['series_title'] ) ? trim($input['series_title']):'';    
            $data['start_date'] = isset($input['start_date']) ? $input['start_date'] :''; 
            $data['end_date'] = isset($input['end_date']) ? $input['end_date']:'';
            $data['status'] = isset($input['status']) ? $input['status'] :'';
           
        

            
            $series = Series::updateOrCreate(['id'=>$id],$data);


          



          //  dispatch(new SendVerificationEmail($user, null, 'user', 'email_verification'));

            return redirect('/admin/series' )->with('status', 'Series updated!');
             
            //return \Redirect::back()->withWarning('Register completed');
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      
      return view('admin.series.seriesadd',$this->data);
    } 


 
   

    public function seriesList(Request $request)
    {        


        $this->data=array();
     
        $title="";


        return view('admin.series.serieslist',$this->data);
    }
   
    public function xhr(Request $request){

      $input = $request->all();

      $cmd=isset($input['cmd']) ? $input['cmd']  : "";

      switch ($cmd) {
        case 'list':
          
          $this->getSeriesList($input);
          break;
      
          case 'delete':
          
          $this->deleteSeries($input);
          break;
        
          
          
        default:
          # code...
          break;
      }
 

    }

    private function getSeriesList($input_data){



      $condition=[];
     
      $limit=isset($input_data['jtPageSize']) ? $input_data['jtPageSize'] : '10';
      $offset=isset($input_data['jtStartIndex']) ? $input_data['jtStartIndex'] : '0';
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
     


      //$condition['status1']=1;
      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }
      
      $series = Series::where($condition)

        ->where(function($query){

            $query->where(function($query) {

              if(Input::post('name') &&  Input::post('name') !=''){
                $query->where('series_title','like','%'.Input::post('name').'%');
               

              }
            });

        })
       

        ->skip($offset)->take($limit)->orderBy($order_by_key,$order_by_val)->get();

       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $series_count=Series::where($condition)
        ->where(function($query)
              { 


                $query->where(function($query) {
                  if(Input::post('name')!=''){
                      $query->where('series_title','like',Input::post('name').'%');  
                  }
                });
        })
        ->count();

       echo Helpers::responseJson(array("Result"=>"OK","Records"=>$series,"TotalRecordCount"=>$series_count));


    } 

   
   

    private function deleteSeries($input_data){

      $team = Series::find($input_data['series_id']);
     
      if($team->delete()){
      
       echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"Series Delete successfully!!!"));
        
      }
    }


}
?>