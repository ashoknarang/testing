<?php
namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use  Illuminate\Support\MessageBag;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\User,App\Vendor_type,App\User_type,App\Country,App\Vendor,App\State,App\City,App\UserDetails,App\UserRateAndReview;
use DB;
use App\Mail\Registration;
use Mail;
use App\Jobs\SendVerificationEmail;
use App\Jobs\SendEmail;
use App\Helper\Helpers;
use Illuminate\Validation\Rule;


use Config;

class UsersController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth'); 

             
    }
      

   
   
     // return Excel::download(new UsersExport, 'users.xlsx');

    public function addedituser($id="",Request $request){
        

          
         if(($id=="" && $request->segment(2)=="editprofile") || ($request->post('self')!='' && $request->post('self')=="1")){

            $id=Auth::User()->id;
            $this->data['self']=$self="1";
           
         }else{
            $this->data['self']=$self=0;
         }  
         

         // $this->data['states'] = State::All();
        // $this->data['cities'] = City::All();
        $this->data['id'] = $id;
        
         

        $this->data['errors'] =array();
        $input = $request->all();
        // print_r($input);
      
        $title = "Add ";
        $this->data['user_status'] =Config::get("services.user_status");
        if( is_numeric($id) && $id>0){
            $this->data['user_details'] =$user_details = User::where('id','=',$id)->first();              
             $title = "Edit ";    
       }


        $title.="Admin";



        if($request->segment(2)=="editprofile" || $request->post('self')=="1"){

        

          $redirect_type="editprofile";
            
        }

        $this->data['title']=$title; 
        
      if($request->isMethod('post')){

      ///  $user_details->syncPermissions(['trainer-list', 'request-admin']);
      


        $validator = Validator::make($request->all(),[
        'title' => 'required',
        'name' => 'required', //full name of student
       
        ///'email' => 'required|email|unique:users,email',  
        'email' => [
          'required',
          Rule::unique('adminusers')->ignore($id),
          //->whereNull('deleted_at')
        ],
        

       // 'password' => 'required',
        //'c_password' => 'required|same:password',
        
        ]);
       
        


        if(!$validator->fails()){
           
           
          
            $data['name'] = trim($input['name']);
            $data['email'] = trim($input['email']);
            
            if(isset($input['password'])){
              $data['password'] = bcrypt(trim($input['password']));

            }



            $data['status'] = isset($input['status']) ? $input['status'] :0;
           


            
            if(!(is_numeric($id) && $id>0)){
             
              $data['status'] = 1;
            }

            $user = User::updateOrCreate(['id'=>$id],$data);


          

            if($user){

            
              $url = '';
              //$user_details->removeRole('vendor');
              if($request->has('profile_image')) 
              {
               
                $extension = $request->file('profile_image')->getClientOriginalExtension();
                $dir = 'images/profile/';
                $filename = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
                $req=$request->file('profile_image')->move($dir, $filename);


                $url = 'images/profile/'.$filename;
                User::where("id",$user->id)->update(array('image' =>$url));
              }
              
              // $user_details_data['user_id'] = $user->id;
               if(!(is_numeric($id) && $id>0)){


             
                $emaildata=array();
                $emaildata['email_template_type']=Config::get('constants.EMAIL_TO_VENDOR_ON_ADD_FROM_ADMIN_PANEL');

                $emaildata['to']=$user->email;
                $emaildata['tags']=array(

                "[NAME]"=>$course_details->title,
                "[MOBILE]"=>$user->mobile,
                "[PASSWORD]"=>isset($input['password']) ? $input['password'] :'',
                '[ORGANIZATIONNAME]'=>isset($input['organization_name']) ? trim($input['organization_name']) : '',
                '[APP_NAME]'=>config('app.name')
                );

                // print_r($emaildata);

              //  dispatch(new SendEmail($emaildata));

              }



                if($request->has('profile_image') && $user && $user->image!='' && file_exists(base_path()."/".$user->image)  && strpos($user->image,"/profile/")!=false ){
                

                 if(!unlink($user->image)){
                    return redirect('user-profile')->with('success', 'profile updated failed.');
                 }

                } 
          // }
               
          }

          //  dispatch(new SendVerificationEmail($user, null, 'user', 'email_verification'));

            return redirect('/admin/users' )->with('status', 'Profile updated!');
             
            //return \Redirect::back()->withWarning('Register completed');
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      
      return view('admin.users.useradd',$this->data);
    } 


 
   

    public function usersList(Request $request)
    {        

        $this->data['state_id'] =$state_id= 0;
        $this->data['city_id'] =$city_id= 0;
        $this->data['college_id'] =$college_id= 0;

        $this->data=array();
     
        $title="";



       
        // $vendor_details = User::

        // join('users',function($join){
        //     $join->on('users.id','=','vendors.vendor_id');
        // })->orderBy('vendors.vendor_id')->where('users.is_deleted','=',0)->paginate(15);
        // $this->data['vendors'] = $vendor_details;


        return view('admin.users.userslist',$this->data);
    }
     public function userslogilogList(Request $request)
    {





        
       

        $this->data=array();
     
        $title="";

        
      
 
        // $vendor_details = User::

        // join('users',function($join){
        //     $join->on('users.id','=','vendors.vendor_id');
        // })->orderBy('vendors.vendor_id')->where('users.is_deleted','=',0)->paginate(15);
        // $this->data['vendors'] = $vendor_details;


        return view('admin.users.usersloginloglist',$this->data);
    }


    public function xhr(Request $request){

      $input = $request->all();

      $cmd=isset($input['cmd']) ? $input['cmd']  : "";

      switch ($cmd) {
        case 'list':
          
          $this->getUserList($input);
          break;
        case 'list_custome':
          
          $this->getUserListCustome($input);
          break;
          case 'delete':
          
          $this->deleteUser($input);
          break;
          case 'user_status':
          
          $this->updateUserStatus($input);
          break;
                
        default:
          # code...
          break;
      }
 

    }

    private function getUserList($input_data){



      $condition=[];
     
      $limit=isset($input_data['jtPageSize']) ? $input_data['jtPageSize'] : '10';
      $offset=isset($input_data['jtStartIndex']) ? $input_data['jtStartIndex'] : '0';
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
     


      //$condition['status1']=1;
      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }
      
      $users = User::where($condition)

        ->where(function($query){

            $query->where(function($query) {

              if(Input::post('name') &&  Input::post('name') !=''){
                $query->where('email','like','%'.Input::post('name').'%')
                ->orWhere('name', 'like', '%'.Input::post('name') . '%')
                ->orWhere('mobile', 'like', '%'.Input::post('name') . '%');
               

              }
            });

        })
       

        ->skip($offset)->take($limit)->orderBy($order_by_key,$order_by_val)->get();

       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $user_count=User::where($condition)
        ->where(function($query)
              { 


                $query->where(function($query) {
                  if(Input::post('name')!=''){
                      $query->where('email','like',Input::post('name').'%')
                      ->orWhere('name', 'like', Input::post('name') . '%')
                      ->orWhere('mobile', 'like', Input::post('name') . '%')

                      ;  
                  }
                });
        })
        ->count();

       echo Helpers::responseJson(array("Result"=>"OK","Records"=>$users,"TotalRecordCount"=>$user_count));


    } 

   
   

    private function deleteUser($input_data){

      $user = User::find($input_data['user_id']);
      User::where('id', $input_data['user_id'])->update(['mobile'=>'','email'=>'']);
      if($user->delete()){
      
       echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"User Delete successfully!!!"));
        
      }
    }
 

 


 

   private function updateUserStatus($input_data) // common function 
   {
     $id =$input_data['user_id'];
    
    

     if(isset($input_data['type']) && ($input_data['type']=='enable' || $input_data['type']=='disable' ) ){
        
        $user = User::where('id', $id)->first();
        $status=$user->status;
        
        if($status==1){

          $userTokens = $user->tokens;

          foreach($userTokens as $token) {
            $token->delete();   
          }
          $update_data['status']=0;
         
        }else{
          $update_data['status']=1;
       
         
       }

      
     }elseif(isset($input_data['type']) && ($input_data['type']=='approve' || $input_data['type']=='unapprove')  ){


        $approval_status = User::where('id', $id)->value('is_approved');


          $status = User::where('id', $id)->value('status');

          if($status==0 && $approval_status==0){
            $update_data['status']=1;
          }


        

         if($approval_status==1){
            $update_data['is_approved']=0;
         
          }else{
            $update_data['is_approved']=1;
         
           
         }
     }

      User::where('id', $id)->update($update_data);
      
      if(isset($input_data['type']) && $input_data['type']=='approve'){
   
        $randomNum=substr(str_shuffle("0123456789adhijkDEFGHIJOPQRS"), 0, 5);
        $password =  bcrypt(trim($randomNum));
        User::where('id', $id)->update(['password'=>$password]);

        $user=User::with('userdetails:organization_name,user_id')->where('id', $id)->select('email','name','id')->first();

        $emaildata=array();
        $emaildata['email_template_type']=Config::get('constants.EMAIL_TEMPLATE_FOR_VENDOR_WHEN_ADMIN_APPROVEB_BE_OUR_AFFILIATE_REQUEST');

        $emaildata['to']=$user->email;



        $emaildata['tags']=array("[ORANIZATION_NAME]"=>$user->userdetails->organization_name,'[APP_NAME]'=>config('app.name'),'[NAME]'=>$user->name,'[PASSWORD]'=>$randomNum);


        dispatch(new SendEmail($emaildata));
        }

     echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"User Status updated successfully!!!"));


   }

   
   


}
?>