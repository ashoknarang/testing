<?php
namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use Mail,App\User;
use App\Jobs\SendVerificationEmail;
use App\Helper\Helpers;
use App\Jobs\SendEmail;
use App\Smtp;
 
use Config;
 
class SmtpController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');        
    }
    
   
    public function index(Request $request)
    {
        
       
        $this->data=array();
        // echo $input = $request->segment(3);
        // die;
        
 
     

      

        return view('admin.smtp.list',$this->data);
    }


    public function addeditsmtp($id="0",Request $request){
        
 
       
        

        $user = User::find(Auth::id());
        $this->data['user']=$user;
        $this->data['vendors'] =array();


          
       

        
        $this->data['id'] = $id;
       
        $this->data['type'] =Config::get("services.user_status");
        
         

        $this->data['errors'] =array();
        $input = $request->all();
        

        $this->data['type']=$type=isset($input['type']) ? $input['type'] :''; 


       
        
        $title = "Add ";

     

        if(is_numeric($id) && $id>0){

            $this->data['smtp_details'] =$smtp_details = Smtp::where('id','=',$id)->first();


 
            

            
          
            $title = "Edit ";             
        }

     
        $this->data['title']=$title; 
         
   
    

      if($request->isMethod('post')){




        $validator = Validator::make($request->all(),[
           'email' => 'required',
           'password' => 'required',
        
        ]);
       


        if(!$validator->fails()){
           
            try{

            \DB::beginTransaction();



         
         
            
            $data['name'] = isset($input['name']) ? $input['name'] : '';
            $data['email'] = isset($input['email']) ? $input['email'] : '';
            $data['password'] = isset($input['password']) ? $input['password'] : '';
            

            $data['status'] = (isset($input['status'])?$input['status']:'');
           
            $data['created_at'] = \Carbon\Carbon::now();
            $data['updated_at'] = \Carbon\Carbon::now();


            
        

            

                

          
            $course = \App\Smtp::updateOrCreate(['id'=>$id],$data);

           
 
 
       
           
       
            \DB::commit();

            return redirect('/admin/smtp/')->with('status', 'Smtp updated!');
              
            //return \Redirect::back()->withWarning('Register completed');
          }catch(\Exception $e) {

            \DB::rollback();
           echo  $e->getMessage();

           
            // $this->data['errors'] =  [ $e->getMessege()];

               
          
          }

         
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      return view('admin.smtp.add',$this->data);
    } 
 
 
   

   


    public function xhr(Request $request){

      $input = $request->all();

      $cmd=isset($input['cmd']) ? $input['cmd']  : "";

      switch ($cmd) {
        case 'list':
          
          $this->getSmtpList($input);
          break;
          case 'delete':
          
          $this->deleteSmtp($input);
          break;
          case 'course_status':
          
          $this->updateSmtpStatus($input);
          break;
            case 'course_detials':
          
          $this->getSmtpDetails($input);
          break;

        default:
          # code...
          break;
      }
 

    }

    private function getSmtpList($input_data){


      $condition=[];
     
      $limit=isset($input_data['jtPageSize']) ? $input_data['jtPageSize'] : '';
      $offset=isset($input_data['jtStartIndex']) ? $input_data['jtStartIndex'] : '';
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
  
      $user = User::find(Auth::id());

      

      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }

   


     
     

       $courses = Smtp::where($condition)->skip($offset)->take($limit)->orderBy($order_by_key,$order_by_val)->get();



       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $smtp_count=Smtp::where($condition)->count();


      


       echo Helpers::responseJson(array("Result"=>"OK","Records"=>$courses,"TotalRecordCount"=>$smtp_count));


    }
    private function getCourseDetails($input_data){


      $condition=[];
     
     
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
  
      $user = User::find(Auth::id());

      if($user->role==Config::get('constants.USER_TYPE_VENDOR')){
        $condition['user_id']=$user->id;

      }

      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }

      if(Input::post('is_approved') !=''){

         $condition['is_approved']=Input::post('is_approved');
      }
     
     
     

       $courses = Course::with(['user'=>function($query){

          $query->with(['userdetails'=>function($query){
              $query->addSelect('organization_name','user_id');
          }])->select('name','id');
       }])

//select('name','email','id','mobile','created_at','status')
        ->where($condition)

         ->where('user_id','>=','0')

       ->where(function($query)
      {

        if(trim(Input::post('name'))!=''){

            $query->whereTranslationLike('title',Input::post('name').'%');
          }
          
          
      })->orderBy($order_by_key,$order_by_val)->get();

       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $course_count=Course::where($condition)
                  ->where(function($query)
                        {
                           if(trim(Input::post('name'))!=''){

            $query->whereTranslationLike('title',Input::post('name').'%');
          }
                        })
                  ->count();


       echo Helpers::responseJson($courses);

     

    }


    private function deleteCourse($input_data){

      $course = Course::find($input_data['course_id']);
    
      if($course->delete()){

       echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"User Delete successfully!!!"));
        
      }
    }
 

 


 

   private function updateCourseStatus($input_data) // common function 
   {
     $id =$input_data['course_id'];
      


      if(isset($input_data['type']) && ($input_data['type']=='enable' || $input_data['type']=='disable' ) ){
        
         $status = Course::where('id', $id)->value('status');

        if($status==1){
         $update_data['status']=0;
         
        }else{
          $update_data['status']=1;
       
         
       }

      
     }elseif(isset($input_data['type']) && ($input_data['type']=='approve' || $input_data['type']=='unapprove')  ){

         $course = Course::with("user:id,name,email")->where('id', $id)->first();

        if($course->is_approved==1){
          $update_data['is_approved']=0;
         
        }else{
          


          if(trim($course->user->email)!='' && $course->user->email!=Null && filter_var($course->user->email, FILTER_VALIDATE_EMAIL)) {


        
            $emaildata=array();
            $emaildata['email_template_type']=Config::get('constants.EMAIL_TEMPLATE_FOR_VENDOR_COURSE_APPROVED_BY_ADMIN');
            $emaildata['to']=$course->user->email;




            $emaildata['tags']=array("[CourseName]"=>$course->title,"[VendorName]"=>$course->user->name,'[APP_NAME]'=>config('app.name'));
             dispatch(new SendEmail($emaildata));
          }
          $update_data['is_approved']=1;
       
         
       }
      
     }


     Course::where('id', $id)->update($update_data); 


   

     echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"Course Status updated successfully!!!"));


   }

    

   private function updateUserStatus($input_data) // common function 
   {
     $id =$input_data['user_id'];
    
    

     if(isset($input_data['type']) && ($input_data['type']=='enable' || $input_data['type']=='disable' ) ){
        
         $status = User::where('id', $id)->value('status');

        if($status==1){
         $update_data['status']=0;
         
        }else{
          $update_data['status']=1;
       
         
       }

      
     }elseif(isset($input_data['type']) && ($input_data['type']=='approve' || $input_data['type']=='unapprove')  ){

         $status = User::where('id', $id)->value('is_approved');

         if($status==1){
         $update_data['is_approved']=0;
         
        }else{
          $update_data['is_approved']=1;
       
         
       }
     }

      User::where('id', $id)->update($update_data);
    

     echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"User Status updated successfully!!!"));


   }
   


}
?>