<?php
namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use Mail;
use App\Matches;
use App\Helper\Helpers;
use Illuminate\Validation\Rule;

use Config;
use Gate;
 
class MatchesController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth'); 

             
    }
      

   
   
     // return Excel::download(new UsersExport, 'users.xlsx');

    public function addeditmatch($id="",Request $request){
        

          
         

         // $this->data['states'] = State::All();
        // $this->data['cities'] = City::All();
        $this->data['id'] = $id;
        
         

        $this->data['errors'] =array();
        $input = $request->all();
        // print_r($input);
      
        $title = "Add ";
        $this->data['series_status'] =Config::get("services.series_status");


          $this->data['series'] =\App\Series::get();        
          $this->data['teams'] =\App\Teams::where('status','activate')->get();        

        if( is_numeric($id) && $id>0){
            $this->data['match_details'] =$match_details = Matches::where('id','=',$id)->first();              
             $title = "Edit ";    
        }


        $title.="Matches";


 

        $this->data['title']=$title; 
        
      if($request->isMethod('post')){

      ///  $user_details->syncPermissions(['trainer-list', 'request-admin']);
      


        $validator = Validator::make($request->all(),[
         'series_id' => 'required',
         'team1_id' => 'required',
         'team2_id' => 'required',
         'match_date' => 'required',
         'match_time' => 'required',
        
        ]);
       
        


        if(!$validator->fails()){
           
           
          
            $data['series_id'] = isset($input['series_id'] ) ? trim($input['series_id']):'';    
            $data['team1_id'] = isset($input['team1_id']) ? $input['team1_id'] :''; 
            $data['team2_id'] = isset($input['team2_id']) ? $input['team2_id']:'';
            $data['match_datetime'] = isset($input['match_date']) ? $input['match_date']." ".$input['match_time']:'';
            $data['winner_team_id'] = isset($input['winner_team_id']) ? $input['winner_team_id']:'';
            $data['result'] = isset($input['result']) ? $input['result']:'';
            $data['status'] = isset($input['status']) ? $input['status'] :'';         
            $match = Matches::updateOrCreate(['id'=>$id],$data);


          



          //  dispatch(new SendVerificationEmail($user, null, 'user', 'email_verification'));

            return redirect('/admin/matches' )->with('status', 'Match updated!');
             
            //return \Redirect::back()->withWarning('Register completed');
        }
        else{

          $this->data['errors'] = $validator->messages();
        
        }

        
      }

      
      return view('admin.matches.matchadd',$this->data);
    } 


 
   

    public function matchesList(Request $request)
    {        


        $this->data=array();
     
        $title="";


        return view('admin.matches.matcheslist',$this->data);
    }
   
    public function xhr(Request $request){

      $input = $request->all();

      $cmd=isset($input['cmd']) ? $input['cmd']  : "";

      switch ($cmd) {
        case 'list':
          
          $this->getMatchesList($input);
          break;
      
          case 'delete':
          
          $this->deleteSeries($input);
          break;
        
          
          
        default:
          # code...
          break;
      }
 

    }

    private function getMatchesList($input_data){



      $condition=[];
     
      $limit=isset($input_data['jtPageSize']) ? $input_data['jtPageSize'] : '10';
      $offset=isset($input_data['jtStartIndex']) ? $input_data['jtStartIndex'] : '0';
      $order_by=isset($input_data['jtSorting']) ? $input_data['jtSorting'] : 'id desc';
      $order_by_arr=explode(" ",$order_by);

      $order_by_key=$order_by_arr['0'];
      $order_by_val=$order_by_arr['1'];
       
     


      //$condition['status1']=1;
      if(Input::post('status') !=''){

         $condition['status']=Input::post('status');
      }
      
      $series = Matches::where($condition)
        ->with(['team1:id,team_name','team2:id,team_name','series:id,series_title'])
        ->where(function($query){

            $query->where(function($query) {

              if(Input::post('name') &&  Input::post('name') !=''){
                $query->where('series_title','like','%'.Input::post('name').'%');
               

              }
            });

        })
       

        ->skip($offset)->take($limit)->orderBy($order_by_key,$order_by_val)->get();

       // orWhere('email', 'like', Input::post('name') . '%')->get();

      $series_count=Matches::where($condition)
        ->where(function($query)
              { 


                $query->where(function($query) {
                  if(Input::post('name')!=''){
                      $query->where('series_title','like',Input::post('name').'%');  
                  }
                });
        })
        ->count();

       echo Helpers::responseJson(array("Result"=>"OK","Records"=>$series,"TotalRecordCount"=>$series_count));


    } 

   
   

    private function deleteSeries($input_data){

      $team = Series::find($input_data['series_id']);
     
      if($team->delete()){
      
       echo Helpers::responseJson(array("Result"=>"Ok","Message"=>"Series Delete successfully!!!"));
        
      }
    }


}
?>