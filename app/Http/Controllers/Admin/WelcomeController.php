<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB,Config;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Categorie,App\Stream;
use App\User;
class WelcomeController extends Controller
{

    public function index()
    {
       if (Auth::check()){
          if(auth()->user()->role ==1 || auth()->user()->role ==1){
            return redirect('/admin/dashboard');

          }

        }

     return view('admin.login');

    }
    public function vendorindex()
    {
       if (Auth::check()){
          if(auth()->user()->role ==1 || auth()->user()->role ==4){
            return redirect('/admin/dashboard');

          }

        }

     return view('admin.vendorlogin');

    }

// public function login(){

//   echo "dasdas";
//   die;
//     if (Auth::check()) 
//     {
//       if(auth()->user()->role ==1 || auth()->user()->user_type ==1)
//       {
//         return redirect('/admin/dashboard');
//       }

//     }

//      return view('admin.login');

// }
  


public function resetPassword(Request $request)
{
$input = $request->all();
$token = DB::table('password_resets')->where('email','=',$input['email'])->first();
 if(!isset($token->token))
 {
    return Redirect::back()->withErrors(['Token is invalid']);
 }

if(Hash::check($input['token'], $token->token)) 
{
  $newPassword   = $input['password'];
  $password_ency = password_hash($newPassword,PASSWORD_BCRYPT,[10]);
  User::where('email',$token->email)->update(['password' =>$password_ency]);
  DB::table('password_resets')->where('email', $input['email'])->delete();
  return redirect('/admin/login');
}  
   

}

public function existEmail(Request $request)
{

$input = $request->all();
if(User::where('email', '=',$input['email'])->count() > 0)
{

$data = array('msg' =>"Email Id Already Registered",'status'=>0);

return response()->json(['data'=>$data]);
  
}

else
{

$data = array('msg' =>"Email Not Exist",'status'=> 1);
return response()->json(['data'=>$data]);

}


}

public function existMobile(Request $request)
{

$input = $request->all();
if(User::where('mobile', '=',substr($input['mobile'], -10))->count() > 0 || User::where('mobile', '=',substr($input['mobile'], -10))->count() > 0)
{

$data = array('msg' =>"Mobile Already Registered",'status'=>0);

return response()->json(['data'=>$data]);
  
}
else
{

$data = array('msg' =>"Mobile Not Exist",'status'=> 1);
return response()->json(['data'=>$data]);

}


}



public function streamCategory(Request $request)
   {
   
       $stream_id = $request->input('stream_id');
       $categories = Categorie::where('is_root', '=',1)->where('status', '=',1)->where('stream_id','=',$stream_id)->get();
       if(count($categories)>0)
       {
        $html = '<div class="col-lg-6 col-sm-6 form-field" >
                <label>'.__("specialization").':</label>
                <div>
                  <select name="category_id" id="category_id">
                  <option value="0">Please choose a Specialization</option>';
        foreach ($categories as $category) {
            $html .= '
             
                   <option value="' . $category->id . '">' . $category->title . '</option>
              
           ';
        }
         $html .='    </select>
                </div>
              </div>';
        }
        else
        {
         $html ='';

        }
        echo $html;


   }
   
    public function instituteType(Request $request) {
        $type = $request->input('institute_type');
        $institute_id = $request->input('institute_id') ? $request->input('institute_id') : 0;
        $i_type = $type==1 ? "school":"college";
        $d_type = $type==3 ? "pg":"ug";
        $s_type = ($i_type =="school" ) ? "school" :$d_type;

        $Cdata = DB::table('institute_type')->where('type',$i_type)->where('standard_type',$s_type)->get();
        if(Auth::check()){
          $user=Auth::User(); 
        }       
        


        if($type==1){
            $html ='<option>'.__("select_class").' </option>';
        }else{
           $html ='<option>'.__("select_year").' </option>';
        }
        

        foreach ($Cdata as $Sdata) {
            $disable="";
            $sel="";
            if($institute_id==$Sdata->id){
              $sel="selected";
            }
            if(isset($user) && $user->institute_type==Config::get('constants.ACADEMIC_STATUS_SCHOOL') && $user->institute_id!=$Sdata->id  &&  $Sdata->id!=$user->institute_id+1){
                $disable="disabled";
            }

            $html .= '<option '.$sel.' '.$disable.' value="' . $Sdata->id . '">' . $Sdata->standard_value . '</option>';
        }
         
        echo $html;
    }


    public function getStreamByType(Request $request) {
        $type = $request->input('institute_type');
        if($type==3){
          $type=2;
        }
         $stream_id = $request->input('stream_id') ? $request->input('stream_id') : '';
        
        $streams=\Helpers::getStream($type);



        if($type==1){
            $html ='<option>'.__("select_class").' </option>';
        }else{
           $html ='<option>'.__("choose_stream").' </option>';
        }
        

        foreach ($streams as $stream) {
            
            $sel="";
            
            if($stream_id==$stream->id){
              $sel="selected";
            }

            $html .= '<option '.$sel.'  value="' . $stream->id . '">' . $stream->title . '</option>';
        }
         
        echo $html;
    }



}
