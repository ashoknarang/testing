<?php

namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  DB;
use App;
use App\Helper\Helpers;

class WelcomeController extends Controller
{
    
    public $data = [];
     public function index(Request $request) {
        $this->data['teams'] = \App\Teams::where('status','activate')->get();    
        return view('web.welcome',$this->data);
    }

    public function teamDetails($team_id){
        $this->data['team_details'] = \App\Teams::where('id',$team_id)->where('status','activate')->first();
        $this->data['team_players'] = \App\Players::where('team_id',$team_id)->where('status','activate')->with(['player_history'=>function($query){
            

        }])->get();

        // 'matches'=>function($query) use ($team_id){
        //     $query->with(['team1:id,team_name','team2:id,team_name'])->where('team1_id',$team_id)->orWhere('team2_id',$team_id)->withCount(['totalmatches'=>function($query) use ($team_id){
        //         $query->where('team1_id',$team_id)->orWhere('team2_id',$team_id);
        //     }]);
        //  }
         $this->data['series_team'] = \App\SeriesTeam::where('team_id',$team_id)->with("series:id,series_title,start_date,end_date,status")->get()->toArray();
       
            $match_points = [];
        foreach($this->data['series_team']  as $series_team){
            $series_team = (object)$series_team;

            $matches=\App\SeriesTeam::where('series_id',$series_team->series_id)
            ->select("series_id","total_points","net_runrate","t.team_name","team_id")
            ->addSelect(DB::raw("(select count(distinct id) as user_count from matches where  (team1_id=series_teams.team_id or team2_id=series_teams.team_id) and series_id=$series_team->series_id) as played_match"))
            ->addSelect(DB::raw(" (select count(distinct id) from matches where  winner_team_id = series_teams.team_id and status = 'completed' and (team1_id=series_teams.team_id or team2_id=series_teams.team_id) and series_id=$series_team->series_id) as win_match"))
            ->addSelect(DB::raw(" (select count(distinct id) from matches where  winner_team_id != series_teams.team_id and status = 'completed' and (team1_id=series_teams.team_id or team2_id=series_teams.team_id) and series_id=$series_team->series_id) as loss_match"))
            ->join("teams as t", "series_teams.team_id", "=","t.id")->groupBy("series_id","team_id")->get()->toArray();
            
            $match_points[$series_team->series_id] = ['series_data' => $series_team, "match_data" => $matches];

        }

        $this->data['series_team'] = array_values($match_points);

        //print_r($this->data['series_team']);die;
        return view('web.team-details',$this->data); 
    }
}