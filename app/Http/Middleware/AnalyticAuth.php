<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AnalyticAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(Auth::user()->role == 6 || Auth::user()->role == 7)
        {     

            return $next($request);

        }
     else{

         return redirect()->back();

       }
    }
}
