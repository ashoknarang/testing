<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use App,Config;
use Redirect;
use Request;
class SubscribeAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = "api")
    {
     
       if(Auth::user()->role == Config::get('constants.USER_TYPE_STUDENT') || Auth::user()->role == Config::get('constants.USER_TYPE_GUARDIAN') )
        {     
          $user = Auth::user();
          

       

          if($user->subscription('main')){

            if(!$user->subscription('main')->isActive()){
                
               
              
             return redirect('user-profile');
            }else{

              return $next($request);
            }
          }else{
              return redirect('user-profile');
            
          }

           

        }
       else
       {
        return $next($request);
       }

     }    
        
    }
