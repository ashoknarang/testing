<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use App,Config;
use Redirect;
class WebApproveAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        

        if(Auth::check()){


       
        
           if(Auth::user()->role == Config::get('constants.USER_TYPE_VENDOR'))
            {     


         



              if(Auth::user()->is_approved==0){

                Auth::logout();
                 
                return Redirect::back()->withErrors([__('your_account_is_not_activated_yet_please_contact_mychatri_team')]);

              } if(Auth::user()->is_verified != Config::get('constants.COMMON_STATUS_ACTIVE')){

                 Auth::logout();
                 return Redirect()->route('otp')->with('message', 'Please Verify Your Mobile Number');;

              }else{

               return $next($request);
                
              }
                

            } else{

             return $next($request);


           }
       }
        
    }
}
