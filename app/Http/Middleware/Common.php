<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
class Common
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = Session::get('lang');
		if($lang){
		App::setLocale($lang);
		}else
        {
            $lang = 'en';
            Session::put('lang', $lang);
            App::setLocale($lang);
        } 
        return $next($request);
    }
}
