<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use App,Config;
use Redirect;
use Request;
class WebAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
       if(Auth::user()->role == Config::get('constants.USER_TYPE_STUDENT') || Auth::user()->role == Config::get('constants.USER_TYPE_GUARDIAN') )
        {     

           return $next($request);

        }
       else
       {
         return redirect()->back();
       }

     }    
        
    }
