<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use App,Config;
use Redirect;
use Request;
class SubscribeAuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
       if($request->user()->role == Config::get('constants.USER_TYPE_STUDENT') || $request->user()->role == Config::get('constants.USER_TYPE_GUARDIAN') )
        {     
          $user = $request->user();
          
                  
          if($user->subscription('main')){

            if(!$user->subscription('main')->isActive()){
               return responder()->error(423)->data(['error_description' =>__('please_renew_your_subscription_to_avail_mychatri_services')])->respond(423)->setStatusCode(200); 
            }else{

              return $next($request);
            }
          }else{
             return responder()->error(423)->data(['error_description' =>__('please_renew_your_subscription_to_avail_mychatri_services')])->respond(423)->setStatusCode(200); 
            
          }

           

        }
       else
       {
        return $next($request);
       }

     }    
        
    }
