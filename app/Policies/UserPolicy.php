<?php

namespace App\Policies;

use App\User,Config;
use Illuminate\Auth\Access\HandlesAuthorization;
use Request;
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function updateUser($user,$updateuser){
    
        if($user->role== Config::get('constants.USER_TYPE_ADMIN') &&  $user->id>0 ) {
            return true;

        }else if($user->role== Config::get('constants.USER_TYPE_VENDOR') &&  isset($updateuser->parent_id) &&  $user->id==$updateuser->parent_id) {
           return true;
        }else if($user->role== Config::get('constants.USER_TYPE_ANALYSIS_MANAGER') &&  isset($updateuser->role) && $updateuser->role==Config::get('constants.USER_TYPE_CALL_CENTER')) {
           return true;
        }
        

    }

    public function userList($user){
         
        
        

        

             return true;
       
        

    } 

    public function requestAdminDetails($user,$adminrequest){
         
        
        

        if($user->role== Config::get('constants.USER_TYPE_ADMIN') &&  $user->id>0  && isset($adminrequest)  ) {
            return true;

        }elseif($user->role== Config::get('constants.USER_TYPE_VENDOR') && isset($adminrequest) && $adminrequest->user_id== $user->id ){

             return true;
        }
        

    } 

    public function addUser($user,$data)
    {



        if($user->role== Config::get('constants.USER_TYPE_ADMIN') &&  $user->id>0    ) {
            return true;

        }elseif($user->role== Config::get('constants.USER_TYPE_VENDOR') && isset($data) && $data->role== Config::get('constants.USER_TYPE_TRAINER') ){

             return true;
        }elseif($user->role== Config::get('constants.USER_TYPE_ANALYSIS_MANAGER') && isset($data) && $data->role== Config::get('constants.USER_TYPE_CALL_CENTER') ){

             return true;
        }
    }

}
