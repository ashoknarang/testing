<?php

namespace App\Policies;

use App\User,Config;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update($user,$course){
        
        if(isset($course) && is_numeric($course->id) && $course->id>0 && $user->role==Config::get('constants.USER_TYPE_ADMIN')) {
            return true;

        }
        

    } 

     public function viewBooking($user,$booking){
        
        if(isset($booking) && is_numeric($booking->id) && $booking->id>0 && $user->role==Config::get('constants.USER_TYPE_ADMIN')) {
            return true;

        }elseif( isset($booking) && $user->role==Config::get('constants.USER_TYPE_VENDOR') && $booking->vendor_id==$user->id){
            return true;
        }
        elseif( isset($booking) && $user->role==Config::get('constants.USER_TYPE_ANALYSIS_MANAGER')){
            return true;
        }
        
        

    } 

    
    public function viewEarningByBatch($user,$batchCourseDetails){
        
        

        if(isset($batchCourseDetails) && is_numeric($batchCourseDetails->id) && $batchCourseDetails->id>0 && $user->role==Config::get('constants.USER_TYPE_ADMIN')) {
            return true;

        }elseif( isset($batchCourseDetails) && $user->role==Config::get('constants.USER_TYPE_VENDOR') &&  isset($batchCourseDetails->course->user_id)  && $batchCourseDetails->course->user_id==$user->id){
            return true;
        }
        

    } 
    

     
    
}
