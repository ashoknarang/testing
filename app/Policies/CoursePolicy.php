<?php

namespace App\Policies;

use App\User,Config;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update($user,$course){
        
        if(isset($course) && is_numeric($course->id) && $course->id>0 && $user->role==Config::get('constants.USER_TYPE_ADMIN')) {
            return true;

        }
        

    } 

     public function viewCourse($user,$course){
        
        if(isset($course) && is_numeric($course->id) && $course->id>0 && $user->role==Config::get('constants.USER_TYPE_ADMIN')) {
            return true;

        }elseif($user->role==Config::get('constants.USER_TYPE_VENDOR') && $course->user_id==$user->id){
            return true;
        }elseif($user->role==Config::get('constants.USER_TYPE_ANALYSIS_MANAGER')){
            return true;
        }
        

    } 

    public function manageCategoryPer($user,$course){
        

        if($user->role==Config::get('constants.USER_TYPE_ADMIN')){
            return true;
        }elseif($user->role==Config::get('constants.USER_TYPE_VENDOR') && isset($course) && is_numeric($course->id) && $course->id>0 && $course->user_id==$user->id ) {
            return true;

        }
        

    } 
    
}
