<?php
namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class PlayerHistory  extends Model
{
    

    protected $table = 'player_history';
    //protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'player_id', 'matches_played','batting_innings','total_runs','high_score','fifties','hundreds','notouts','bat_avg', 'bowl_avg', 'total_wickets','bowl_economy', 'total_stumpings','created_at','updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    
    public function player()
    {
      return $this->hasOne('App\Players','id','player_id');
    }








}
