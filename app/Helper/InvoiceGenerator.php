<?php

namespace App\Helper;

use App;
use App\Booking;
use App\Invoice;
use App\User;
use App\City;
use App\Helper\Helpers;
use NumberToWords\NumberToWords;
use config;

class InvoiceGenerator
{
    public static function generateInvoice($bookingId)
    {
        $db_file_location = "storage/Invoices/";
        $mainInvoiceNumber = "";
        $responseForPdf = [];
        $invoices = Invoice::where('booking_id', $bookingId)->get();

        if ($bookingId) {
            $pdf = App::make('dompdf.wrapper');
        }
        $bookingTable = Booking::where(["id" => $bookingId]);
        $responseForPdf["booking"] = $bookingTable->first();


        $bookings = $bookingTable
            ->with(['user' => function ($query) {
                $query->with('city:id,name')->with('state:id,name')->with('userdetails:organization_name,user_id,address_line_1,address_line_2')
                    ->select(['name', 'email', 'mobile', 'id', 'profile_image', 'role', 'user_type', 'city_id', 'state_id']);
            }, 'vendor' => function ($query) {
                $query->with('city:id,name')->with('state:id,name')->with('userdetails:organization_name,user_id,address_line_1,address_line_2,pan_id,gst_id')
                    ->select(['name', 'email', 'mobile', 'id', 'profile_image', 'role', 'user_type','city_id', 'state_id']);
            }, 'course' => function ($query) {},
            ])->first();

        

        
        $lcpl_id = 2;
        $lcpl = User::where(["id" => $lcpl_id])->with('userdetails:organization_name,user_id,address_line_1,address_line_2,pan_id,gst_id')->first();

        // course Invoice
        $responseForPdf["invoice_image"] = $lcpl->profile_image;
        $responseForPdf["from"] = $bookings["vendor"];
        $responseForPdf["from"]["profile_image"] = $lcpl;
        $responseForPdf["to"] = $bookings["user"];
        $responseForPdf["course"] = $bookings["course"];
        $responseForPdf["title"] = "Tax Invoice/ Course Fee";
        $tax = [];
        $tax[0]["type"] = "GST(CGST @9% & SGST @ 9%)";
        $tax[0]["rate"] = 18;
        $responseForPdf["tax"] = $tax;
        
        $db_id1 = $bookings["id"] . '-' . $responseForPdf["from"]["id"] . '-' . $responseForPdf["to"]["id"] . '-1';
        if (!Self::checkEntry($invoices, $db_id1, 0)) {
            $table_header = [];
            $table_header[1] = "Sr/No";
            $table_header[2] = "Details";
            $table_header[3] = "Duration";
            $table_header[4] = "Start Date";
            $table_header[5] = "Course Fee";
            $table_header[6] = "Tax Type";
            $table_header[7] = "Tax Rate";
            $table_header[8] = "Tax Amount";
            $table_header[9] = "Total Course Fee";
            $responseForPdf["table_header"] = $table_header;

            $table_data = [];
            $table_data[1] = "1";
            $table_data[2] = $responseForPdf["course"]["title"];
            $table_data[3] = $responseForPdf["booking"]["duration"];
            $table_data[4] = $responseForPdf["booking"]["start_date"];
            $table_data[5] = Helpers::numberFormat($responseForPdf["booking"]["default_course_fees"]);
            $table_data[61] = $responseForPdf["tax"][0]["type"];
            $table_data[71] = $responseForPdf["tax"][0]["rate"] . " %";

            $table_data[81] = Helpers::numberFormat( $table_data[5] * $responseForPdf["tax"][0]["rate"] / 100);
            $table_data[9] = Helpers::numberFormat($table_data[5] + $table_data[81]);
            $table_data[10] = Helpers::getAmountInWords(Helpers::numberFormat($table_data[9]));
            $responseForPdf["table_data"] = $table_data;
            $dbEntry = [];
            $dbEntry["id"] = (string) $db_id1;
            $dbEntry["booking_id"] = $bookings["id"];
            $dbEntry["from_id"] = $responseForPdf["from"]["id"];
            $dbEntry["to_id"] = $responseForPdf["to"]["id"];
            $responseForPdf["invoice_number"] = $dbEntry["id"];
            $responseForPdf["invoice_date"] = date('d-m-Y');
            $pdf->loadView('invoice', $responseForPdf);
            $pdf->save(storage_path('Invoices/' . $dbEntry["id"] . '.pdf'));
            $dbEntry["path"] = $db_file_location . $dbEntry["id"] . '.pdf';
            $mainInvoiceNumber = $dbEntry["id"];
            Invoice::addInvoice($dbEntry);
        }
        $responseBookingTmp = $responseForPdf["booking"];
        $responseForPdf = [];


        // Technology Invoice
        $responseForPdf["invoice_image"] = $lcpl->profile_image;
        $responseForPdf["booking"] = $responseBookingTmp;
        $responseForPdf["from"]["id"]= \Config::get('constants.LCPL_ID');
        $responseForPdf["from"]["userdetails"]["organization_name"] = \Config::get('constants.LCPL');
        $responseForPdf["from"]["userdetails"]["address_line_1"] = \Config::get('constants.LCPL_ADDRESS_LINE_1');
        $responseForPdf["from"]["userdetails"]["address_line_2"] = \Config::get('constants.LCPL_ADDRESS_LINE_2');
        $responseForPdf["from"]["city"]["name"] = \Config::get('constants.LCPL_CITY');
        $responseForPdf["from"]["state"]["name"] = \Config::get('constants.LCPL_STATE');

        $responseForPdf["from"]["userdetails"]["pan_id"] = \Config::get('constants.LCPL_PAN');
        $responseForPdf["from"]["userdetails"]["gst_id"] = \Config::get('constants.LCPL_GST');
        $responseForPdf["title"] = "Tax Invoice/ Technology Fee";
        $responseForPdf["to"] = $bookings["user"];
        $db_id2 = $bookings["id"] . '-' . $responseForPdf["from"]["id"] . '-' . $responseForPdf["to"]["id"] . '-2';
        if (!Self::checkEntry($invoices, $db_id2, 1)) {
            $pdf = App::make('dompdf.wrapper');
            $responseForPdf["course"] = $bookings["course"];
            $tax = [];
            $tax[0]["type"] = "GST(CGST @9% & SGST @ 9%)";
            $tax[0]["rate"] = 18;
            $responseForPdf["tax"] = $tax;
            $table_header = [];
            $table_header[1] = "Sr/No";
            $table_header[2] = "Details";
            $table_header[3] = "Duration";
            $table_header[4] = "Start Date";
            $table_header[5] = "Tech Fee";
            $table_header[6] = "Tax Type";
            $table_header[7] = "Tax Rate";
            $table_header[8] = "Tax Amount";
            $table_header[9] = "Total Tech Fee";
            $responseForPdf["table_header"] = $table_header;

            $table_data = [];
            $table_data[1] = "1";
            $table_data[2] = $responseForPdf["course"]["title"];
            $table_data[3] = $responseForPdf["booking"]["duration"];
            $table_data[4] = $responseForPdf["booking"]["start_date"];
            $table_data[5] = Helpers::numberFormat($responseForPdf["booking"]["registration_price"]);
            $table_data[61] = $responseForPdf["tax"][0]["type"];
            $table_data[71] = $responseForPdf["tax"][0]["rate"] . " %";
            $table_data[81] = Helpers::numberFormat( $table_data[5] * $responseForPdf["tax"][0]["rate"] / 100);
            $table_data[9] =Helpers::numberFormat($table_data[5] + $table_data[81]);
            $table_data[10] = Helpers::getAmountInWords(Helpers::numberFormat($table_data[9]));
            $responseForPdf["table_data"] = $table_data;
            $dbEntry = [];
            $dbEntry["id"] = (string) $db_id2;
            $dbEntry["booking_id"] = $bookings["id"];
            $dbEntry["from_id"] = $responseForPdf["from"]["id"];
            $dbEntry["to_id"] = $responseForPdf["to"]["id"];
            $responseForPdf["invoice_number"] = $dbEntry["id"];
            $responseForPdf["invoice_date"] = date('d-m-Y');
            $pdf->loadView('invoice', $responseForPdf);
            $pdf->save(storage_path('Invoices/' . $dbEntry["id"] . '.pdf'));
            $dbEntry["path"] = $db_file_location . $dbEntry["id"] . '.pdf';
            Invoice::addInvoice($dbEntry);
        }


        // commission Invoice
        $responseBookingTmp = $responseForPdf["booking"];
        $responseForPdf = [];
        $responseForPdf["invoice_image"] = $lcpl->profile_image;
        $responseForPdf["booking"] = $responseBookingTmp;
        $responseForPdf["from"]["id"]= \Config::get('constants.LCPL_ID');
        $responseForPdf["from"]["userdetails"]["organization_name"] = \Config::get('constants.LCPL');
        $responseForPdf["from"]["userdetails"]["address_line_1"] = \Config::get('constants.LCPL_ADDRESS_LINE_1');
        $responseForPdf["from"]["userdetails"]["address_line_2"] = \Config::get('constants.LCPL_ADDRESS_LINE_2');
        $responseForPdf["from"]["city"]["name"] = \Config::get('constants.LCPL_CITY');
        $responseForPdf["from"]["state"]["name"] = \Config::get('constants.LCPL_STATE');
        $responseForPdf["from"]["userdetails"]["pan_id"] = \Config::get('constants.LCPL_PAN');
        $responseForPdf["from"]["userdetails"]["gst_id"] = \Config::get('constants.LCPL_GST');
        $responseForPdf["title"] = "Tax/Commission Invoice";
        $responseForPdf["to"] = $bookings["vendor"];
        $db_id3 = $bookings["id"] . '-' . $responseForPdf["from"]["id"] . '-' . $responseForPdf["to"]["id"] . '-3';
        $flag = false;
        
        if (!Self::checkEntry($invoices, $db_id3, 2)) {
            $pdf = App::make('dompdf.wrapper');
            $responseForPdf["course"] = $bookings["course"];
            $tax = [];
            $tax[0]["type"] = "GST(CGST @9% & SGST @ 9%)";
            $tax[0]["rate"] = 18;

            $tax[1]["type"] = "TCS";
            $tax[1]["rate"] = 1;
            $responseForPdf["tax"] = $tax;

            $table_header = [];
            $table_header[1] = "Sr/No";
            $table_header[2] = "Details";
            $table_header[3] = "Course fee";
            $table_header[4] = "Comm. Rate";
            $table_header[5] = "Comm. Amount";
            $table_header[6] = "Tax Type";
            $table_header[7] = "Tax Rate";
            $table_header[8] = "Tax Amount";
            $table_header[9] = "Total Commission";
            $responseForPdf["table_header"] = $table_header;

            $table_data = [];
            $table_data[1] = "1";
            $table_data[2] = "Commision on course Fee with invoice number " . $mainInvoiceNumber;
            $table_data[3] = $responseForPdf["booking"]["default_course_fees"];
            $table_data[4] = $responseForPdf["booking"]["vendor_commission_per"] . ' %';
            $table_data[5] = Helpers::numberFormat($responseForPdf["booking"]["admin_commission"]);
            $table_data[61] = $responseForPdf["tax"][0]["type"];
            $table_data[62] = $responseForPdf["tax"][1]["type"] . "(Taxable fee)";
            $table_data[71] = $responseForPdf["tax"][0]["rate"] . " %";
            $table_data[72] = $responseForPdf["tax"][1]["rate"] . " %";
            $tax1 =Helpers::numberFormat( $table_data[5] * $responseForPdf["tax"][0]["rate"] / 100);
            $tax2 = Helpers::numberFormat($table_data[3] * $tax[1]["rate"] / 100);
            $table_data[81] = Helpers::numberFormat($tax1);
            $table_data[82] = Helpers::numberFormat($tax2);
            $table_data[9] = Helpers::numberFormat($table_data[5] + $tax1 + $tax2);
            $table_data[10] = Helpers::getAmountInWords(Helpers::numberFormat($table_data[9]));
            $responseForPdf["table_data"] = $table_data;
            $dbEntry = [];
            $dbEntry["id"] = (string) $db_id3;
            $dbEntry["booking_id"] = $bookings["id"];
            $dbEntry["from_id"] = $responseForPdf["from"]["id"];
            $dbEntry["to_id"] = $responseForPdf["to"]["id"];
            $responseForPdf["invoice_number"] = $dbEntry["id"];
            $responseForPdf["invoice_date"] = date('d-m-Y');
            $pdf->loadView('invoice', $responseForPdf);
            $pdf->save(storage_path('Invoices/' . $dbEntry["id"] . '.pdf'));
            $dbEntry["path"] = $db_file_location . $dbEntry["id"] . '.pdf';
            Invoice::addInvoice($dbEntry);
        }

        return $responseForPdf;
    }
    public static function checkEntry($invoices, $db_id, $index)
    {
        $flag = false;
        foreach ($invoices as $value) {
            if (isset($value["id"])) {
                if ($value["id"] == $db_id) {
                    $flag = true;
                }
            }
        }
        return $flag;
    }

}
