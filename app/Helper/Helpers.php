<?php

namespace App\Helper;

use App;
use App\Advertisement;
use App\AdvertisementManage;
use App\Banner;
use App\Booking;
use App\Categorie;
use App\City;
use App\Cms;
use App\Country;
use App\Course;
use App\Manage_category;
use App\News;
use App\Otp;
use App\State;
use App\Stream;
use App\Testimonial;
use App\SeminarFeedback,App\IC,App\KC;
use App\User;
use Auth;
use Config;
use DB;
use Image;
use Redirect;
use Session;
use NumberToWords\NumberToWords;
use \Carbon\Carbon;
use FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class Helpers
{

    public static function userStatus($id)
    {

        $count = User::where('id', $id)->where('status', 1)->count();

        if ($count == 1) {

            echo "<button type='button'  id='b$id' class='btn btn-block btn-success btn-xs' style='width:100%'  onclick = 'buttonChange($id)' >Active</button>";
        } else {

            echo "<button type='button' id='b$id'  class='btn btn-block btn-danger btn-xs' style='width:100%' onclick = 'buttonChange($id)' >Inactive</button>";
        }
    }

    public static function userprofileImage($id)
    {

        $profile_image = User::where('id', $id)->get();
        if ($profile_image[0]->profile_image == '' && $profile_image[0]->gender == '') {
            $url = "adminUI/dist/img/user.png";
            return $url;
        } else if ($profile_image[0]->profile_image == '' && $profile_image[0]->gender == 1) {
            $url = "adminUI/dist/img/userm.png";
            return $url;
        } else if ($profile_image[0]->profile_image == '' && $profile_image[0]->gender == 2) {
            $url = "adminUI/dist/img/userf.png";
            return $url;
        } else if ($profile_image[0]->profile_image == '' && $profile_image[0]->gender == 3) {
            $url = "adminUI/dist/img/user.png";
            return $url;
        } else {
            $url = $profile_image[0]->profile_image;
            return $url;
        }
    }

    /**  Home Screen */
    public static function testimonial($type)
    {

        $data = Testimonial::with(['user' => function ($query) {
            $query->select('name', 'id', 'profile_image');
        }])->where('type', $type)->where('status', Config::get('constants.COMMON_STATUS_ACTIVE'))->orderBy('id', 'DESC')->limit(5)->get();

        return $data;
    }

    public static function seminar_feedback()
    {
        $data = SeminarFeedback::where('status', Config::get('constants.COMMON_STATUS_ACTIVE'))->get();        
        return $data;
    }

   

    public static function responseJson($arr)
    {
        //header("Content-type: application/json");
        return json_encode($arr);
    }

    public static function News()
    {

        $data = News::orderBy('id', 'DESC')->where('status', Config::get('constants.COMMON_STATUS_ACTIVE'))->limit(5)->get();
        return $data;
    }

    /**  home  screen end* */
    public static function streamHomeStatus($id)
    {

        $count = Stream::where('id', $id)->where('is_home', 1)->count();

        if ($count == 1) {

            echo "<button type='button'  id='b$id' class='btn btn-block btn-success btn-xs' style='width:50%' onclick = 'buttonChange($id)' >Active</button>";
        } else {

            echo "<button type='button' id='b$id'  class='btn btn-block btn-danger btn-xs' style='width:50%' onclick = 'buttonChange($id)' >Inactive</button>";
        }
    }

    public static function newsStatus($id)
    {

        $count = News::where('id', $id)->where('status', 1)->count();

        if ($count == 1) {

            echo "<button type='button'  id='b$id' class='btn btn-block btn-success btn-xs' style='width:50%' onclick = 'buttonChange($id)' >Active</button>";
        } else {

            echo "<button type='button' id='b$id'  class='btn btn-block btn-danger btn-xs' style='width:50%' onclick = 'buttonChange($id)' >Inactive</button>";
        }
    }

    public static function bannerStatus($id)
    {
        $count = Banner::where('id', $id)->where('status', 1)->count();

        if ($count == 1) {

            echo "<button type='button'  id='b$id' class='btn btn-block btn-success btn-xs' style='width:50%' onclick = 'buttonChange($id)' title='Enabled' >E</button>";
        } else {

            echo "<button type='button' id='b$id'  class='btn btn-block btn-danger btn-xs' style='width:50%' onclick = 'buttonChange($id)' title='Disabled'>D</button>";
        }
    }

    public static function registrationNo()
    {

        $numbers = range(0, 9);
        shuffle($numbers);
        for ($i = 0; $i < 10; $i++) {
            global $digits;
            $digits .= $numbers[$i];
        }

        $reg = 'MC' . $digits;
        return $reg;
    }

    // public static function stream() {

    //     $data = Stream::where('status', 1)->where('institute_type_id',2)->get();
    //     return $data;
    // }

    public static function country()
    {

        $country = Country::where('status', 1)->get();
        return $country;
    }

    public static function categories()
    {
        $categories = Categorie::where('is_root', '=', 1)->where('status', '=', 1)->get();
        return $categories;
    }

    public static function instituteType($type = 2)
    {
        $i_type = $type == 1 ? "school" : "college";
        $d_type = $type==3 ? "pg":"ug";
        $s_type = $i_type == "school" ? "school" : $d_type;
        $data = DB::table('institute_type')->where('type', $i_type)->where('standard_type', $s_type)->get();
        return $data;
    }
    public static function getActiveInstituteType($type = 2)
    {
        $i_type = $type == 1 ? "school" : "college";
        $d_type = $type==3 ? "pg":"ug";
        $s_type = $i_type == "school" ? "school" : $d_type;
        $data = DB::table('institute_type')->where('status', Config::get('constants.COMMON_STATUS_ACTIVE'))->where('type', $i_type)->where('standard_type', $s_type)->get();
        return $data;
    }

    public static function getRandomNumber($digits = 4)
    {
        return rand(pow(10, $digits - 1), pow(10, $digits) - 1);
    }

    public static function encrypt_decrypt($string, $action = 'e')
    {
        $secret_key = 'my_chhatriweb_secret_key';
        $secret_iv = 'my_chhatriweb_secret_iv';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'e') {
            $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
        } else if ($action == 'd') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public static function getOS()
    {
        $info = array();
        $OS = array("Windows" => "/Windows/i",
            "Linux" => "/Linux/i",
            "Unix" => "/Unix/i",
            "Mac" => "/Mac/i",
        );

        foreach ($OS as $key => $value) {
            if (preg_match($value, $_SERVER['HTTP_USER_AGENT'])) {
                $info = array_merge($info, array("Operating System" => $key));
                break;
            }
        }
        return $info['Operating System'];
    }

    public static function getAllChildsViaRootCategoryId($root_catgory_id = null)
    {
        if (!$root_catgory_id) {
            return false;
        } else {
            return DB::select(DB::raw("SELECT * FROM categories WHERE parent_id =" . $root_catgory_id . "
            UNION SELECT * FROM categories WHERE parent_id IN (SELECT id FROM categories WHERE parent_id =" . $root_catgory_id . ")"));
        }
    }

    public static function getUserViaMobileNumber($mobile = null)
    {

        if (!$mobile) {
            return false;
        }

        return User::where('mobile', trim($mobile))->orWhere('mobile', trim($mobile))->orWhere('mobile', $mobile)->first();
    }
    public static function sendOtpByMobile($input)
    {
        $temp_message_status = '';
        //TODO verify the mobile exist or not in users table
        try {

            //$twilio_sms_receiver = trim($input['mobile']);
            //TODO verify the mobile exist or not in users table
            $user = User::where('mobile', trim($input['mobile']))->first();

            if ($user) {

                $otp = Helpers::getRandomNumber();

                // if(env('APP_ENV') == 'development'){
                $websmsapp_username = env('WEBSMSAPP_USERNAME');
                $websmsapp_password = env('WEBSMSAPP_PASSOWRD');
                $websmsapp_message = urlencode('Hello ' . (isset($user->name) ? $user->name : 'Guest') . '. OTP for mychatri registration is ' . $otp);
                $smsURL = "http://websmsapp.in/api/mt/SendSMS?user=" . $websmsapp_username . "&password=" . $websmsapp_password . "&senderid=TELEOS&channel=Trans&DCS=0&flashsms=0&number=" . $user->mobile . "&text=" . $websmsapp_message . "&route=9";

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $smsURL,
                    CURLOPT_RETURNTRANSFER => false,
                    CURLOPT_ENCODING => "",
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HEADER => false,
                ));
                $st = curl_exec($curl);
                curl_close($curl);
                //  }

                \DB::beginTransaction();
                //$temp_message_status = $st;
                $otp_input['value'] = $otp;
                $otp_input['user_id'] = $user->id;
                $otp_input['created_by'] = $input['mobile'];
                $otp_input['status'] = 1;
                $otp_input['is_expired'] = 0;
                // $otp_input['message_sid'] = $message->sid;
                // $otp_input['message_status'] = $message->status;
                // $otp_input['message_error_code'] = $message->errorCode;
                // $otp_input['message_error_message'] = $message->errorMessage;
                $otp_input['created_at'] = Carbon::now();
                $otp_input['updated_at'] = Carbon::now();
                //TODO verify the mobile number in users table
                \DB::table('otp')->insert($otp_input);
                \DB::commit();

            }

            return array("otp" => $otp, "status" => "1", "mobile" => $otp_input['created_by'], 'user_id' => $user->id);

        } catch (\Exception $e) {
            //  \DB::rollback();
            // return responder()->error(500,$e->getMessage())->data(['error_description' => 'server error due to twilio error of : ' .$e->getMessage() ])->respond(500);
        }

    }
    public static function SendOtp($input_data = array())
    {

        try {

            $otp = Self::getRandomNumber();

            $otp_input['user_id'] = $input_data['user_id'];
            $otp_input['value'] = $otp;
            $otp_input['status'] = 1;
            $otp_input['is_expired'] = 0;
            $otp_input['created_for'] = isset($input_data['created_for']) ? $input_data['created_for'] : 'mobile';
            $otp_input['created_at'] = Carbon::now();
            $otp_input['updated_at'] = Carbon::now();
            // save to otp table

            $user = User::where('id', $input_data['user_id'])->first();

            // print_r($user);
            // die;

            $mobile = isset($input_data['mobile']) ? $input_data['mobile'] : $user->mobile;

            ///$country_code = strtoupper(Country::where(['id' => trim($user->country_id)])->first()->code);
            try {

                //$otp_input['created_by'] = PhoneNumber::make(trim($mobile), $country_code)->formatE164();
                $otp_input['created_by'] = $mobile;

            } catch (Exception $e) {
                echo $e->getMessage();
            }

            ob_start();
            //  if(env('APP_ENV') == 'development'){
            $websmsapp_username = env('WEBSMSAPP_USERNAME');
            $websmsapp_password = env('WEBSMSAPP_PASSOWRD');
            $websmsapp_message = urlencode('Hello ' . $user->name . '. OTP for mychatri registration is ' . $otp);

            $smsURL = "http://websmsapp.in/api/mt/SendSMS?user=" . $websmsapp_username . "&password=" . $websmsapp_password . "&senderid=TELEOS&channel=Trans&DCS=0&flashsms=0&number=" . $otp_input['created_by'] . "&text=" . $websmsapp_message . "&route=9";

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $smsURL,
                CURLOPT_RETURNTRANSFER => false,
                CURLOPT_ENCODING => "",
                CURLOPT_TIMEOUT => 1,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HEADER => false,
            ));
            $st = curl_exec($curl);
            curl_close($curl);

            //  }
            ob_end_clean();

            //var_dump($message);

            // $otp_input['message_sid'] = $message->sid;
            // $otp_input['message_status'] = $message->status;
            // $otp_input['message_error_code'] = $message->errorCode;
            // $otp_input['message_error_message'] = $message->errorMessage;
            // }

            if (Otp::create($otp_input)) {
                return array("otp" => $otp, "user_id" => $user->id, "status" => "1", "mobile" => $otp_input['created_by']);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function countryName($id)
    {
        $name = Country::where('id', $id)->value('name');
        return $name;
    }

    public static function stateName($id)
    {

        $name = State::where('id', $id)->value('name');
        return $name;
    }

    public static function cityName($id)
    {
        $name = City::where('id', $id)->value('name');
        return $name;
    }

    public static function videoType($url)
    {
        if (strpos($url, 'youtube') > 0) {
            return 'youtube';
        } elseif (strpos($url, 'vimeo') > 0) {
            return 'vimeo';
        } else {
            return 'unknown';
        }
    }

    public static function getImportVideoUrl($import_url)
    {

        if (strpos($import_url, 'youtube') > 0) {

            if (strpos($import_url, "?v=") != false) {
                $exp = explode('v=', $import_url);
            } else {
                $exp = explode('youtu.be/', $import_url['document']['online_url']);
            }

            if (isset($exp[1])) {
                $return_arr['thumb'] = "https://img.youtube.com/vi/" . $exp[1] . "/hqdefault.jpg";

                $return_arr['original_file'] = "https://www.youtube.com/embed/" . $exp[1];

                //print_r($return_arr)

                return $return_arr;
            }
        } elseif (strpos($import_url, 'vimeo') > 0) {

        } else {

        }

        return array();
    }

    public static function getCategoryName($id)
    {

        $category_lists = Categorie::where('id', $id)->where('status', '=', 1)->get()->first();

        return $category_lists;
    }

    public static function logout()
    {

        $userRole = auth()->user()->role;
        Auth::logout();
        if ($userRole == 1) {
            return redirect('/admin');
        }
        if ($userRole == 2 || $userRole == 3) {
            return redirect('/');
        }
        if ($userRole == 4) {
            return redirect('/vendors');
        }
    }

    public static function langChange()
    {
        $lang = Session::get('lang');
        if ($lang) {
            App::setLocale($lang);
        }
    }

    public static function menu()
    {
        $data = DB::table('menu')->get();
        return $data;
    }

    public static function menuName($id)
    {
        $data = DB::table('menu')->where('id', $id)->value('title');

        return $data;
    }

    public static function createThumbnail($filename, $width, $height, $full_path, $url, $crop)
    {
        $thumb_path = $full_path . 'thumbnail';

        if (!is_dir($thumb_path)) {
            mkdir($thumb_path, 0777);
        }
        $imagePath = realpath($url);
        $img = Image::make($imagePath);

        $img->resize($width, $height, function ($constraint) use ($crop) {

            if (!$crop) {

                $constraint->aspectRatio();
            }
        })->save($thumb_path . '/' . $filename);
        return $thumb_path . '/' . $filename;
    }

    public static function filterString($title = '', $size = 20)
    {
        return strlen($title) > $size ? mb_substr($title, 0, $size) . ".." : $title;
    }

    public static function createBreadcrumb($category_id,$stream_id, &$breadcrum_array = array())
    {

        $category_rel = \App\CategoriesRelationship::where(['category_id'=>$category_id,'stream_id'=>$stream_id])->first();


         
        $breadcrum_array[$category_rel->category->slug] = $category_rel->category->title;

        if ($category_rel->parent_id >0 ) {
        //     $get_stream = Stream::where('id', $category->stream_id)->get()->first();
        //     $breadcrum_array[$get_stream->slug] = $get_stream->title;
        // } else {

            Helpers::createBreadcrumb($category_rel->parent_id, $stream_id,$breadcrum_array);
       }

        return array_reverse($breadcrum_array);
    }

    public static function howItWorks()
    {

        $data = Cms::where('slug', 'how-it-works')->where('status', Config::get('constants.COMMON_STATUS_ACTIVE'))->get()->first();
        return $data;
    }

    public static function getBookingStatusText($booking)
    {

        $status_text = "";
        if ($booking->status == Config::get("constants.BOOKING_STATUS_PAID")) {

            $status_text = __("enrolled");

            if ($booking->start_date > \Carbon\Carbon::now() && $booking->end_date < \Carbon\Carbon::now()) {

                $status_text = __("running");
            }
            if ($booking->end_date < \Carbon\Carbon::now()) {

                $status_text = __("completed");
            }
        } elseif ($booking->status == Config::get("constants.BOOKING_STATUS_CANCELLED")) {
            $status_text = __("cancelled");
        } else if ($booking->status == Config::get("constants.BOOKING_STATUS_PAYMENT_CANCEL")) {
            $status_text = __("payment_failed");

        } else if ($booking->status == Config::get("constants.BOOKING_STATUS_PAYMENT_PANDING")) {
            $status_text = __("payment_in_process");

        } else if ($booking->status == Config::get("constants.BOOKING_STATUS_REFUNDED")) {
            $status_text = __("cancelled");

        } else if ($booking->status == Config::get("constants.BOOKING_STATUS_INSTALLMENT_PENDING")) {
            $status_text = __("Installments Pending");
        } else if ($booking->status == Config::get("constants.BOOKING_STATUS_INSTALLMENT_PAID")) {
            $status_text = __("Installments Paid");
        }

        return $status_text;
    }

    public static function getBookingVendorType($booking)
    {

        $vendor_type = "NA";

        if (isset($booking->vendor->user_type)) {

            if ($booking->vendor->user_type == 1) {
                $vendor_type = __("Corporate");
            }
            if ($booking->vendor->user_type == 2) {
                $vendor_type = __("Training organization");
            }
            if ($booking->vendor->user_type == 3) {
                $vendor_type = __("Freelancer");
            }
        }
        return $vendor_type;
    }

    public static function getBookedCourseType($course_type = 0)
    {

        $course_type_text = "NA";

        if (isset($course_type) && $course_type > 0) {

            if ($course_type == Config::get('constants.COURSE_TYPE_TRAINING')) {
                $course_type_text = __("Training");
            }
            if ($course_type == Config::get('constants.COURSE_TYPE_INTERNSHIP')) {
                $course_type_text = __("Internship");
            }
        }
        return $course_type_text;
    }

    public static function creatOrderNo($id)
    {

        $booking = Booking::with(["vendor" => function ($query) {
            $query->with('userdetails:organization_name,user_id')->select('id');
        }, "course" => function ($query) {
            $query->select('id');
        }])->where("id", $id)->first();

        if (!empty($booking)) {

            $company_name = $booking->vendor->userdetails->organization_name;

            $course_name = $booking->course->translate('en')->title;

            $comp_short_name = '';
            $course_short_name = '';

            if (trim($company_name) != '') {
                $comp_arr = explode(" ", $company_name);

                if (isset($comp_arr) && is_array($comp_arr) && count($comp_arr) > 1) {
                    foreach ($comp_arr as $name_str) {
                        $name_str = preg_replace("/[^a-zA-Z0-9]/", "", $name_str);
                        $comp_short_name .= substr($name_str, 0, 1);
                    }
                } else {
                    $comp_arr_str = isset($comp_arr[0]) ? preg_replace("/[^a-zA-Z0-9]/", "", $comp_arr[0]) : '';
                    $course_short_name .= substr($comp_arr_str, 0, 1);
                }
            }
            if (trim($course_name) != '') {
                $course_arr = explode(" ", $course_name);

                if (count($course_arr) > 1) {
                    foreach ($course_arr as $crs_name_str) {
                        $crs_name_str = preg_replace("/[^a-zA-Z0-9]/", "", $crs_name_str);

                        $course_short_name .= substr($crs_name_str, 0, 1);
                    }
                } else {
                    $comp_arr_str1 = isset($course_arr[0]) ? preg_replace("/[^a-zA-Z0-9]/", "", $course_arr[0]) : '';
                    $course_short_name .= substr($comp_arr_str1, 0, 1);
                }
            }

            $zeros = substr("00000", strlen($booking->id));

            return $booking_order_no = strtoupper($comp_short_name) . "/" . strtoupper($course_short_name) . "/B" . $booking->batch_id . "/" . $zeros . $booking->id;
        }
    }

    public static function advertisementPage($slug, $category_id = 0)
    {
        if ($slug != '0') {

            $cms = Cms::where('slug', $slug)->where('status', Config::get('constants.COMMON_STATUS_ACTIVE'))->first();
            if (isset($cms->id)) {
                $data = AdvertisementManage::with(['advertisement' => function ($query) use ($cms) {
                    $query->where(['status' => Config::get('constants.COMMON_STATUS_ACTIVE'), 'is_premium' => Config::get('constants.COMMON_STATUS_DEACTIVE')]);
                    $query->orderBy('sort_order', 'asc');
                }])->whereIn('advertisement_id', function ($query) {
                    $query->select('id')
                        ->from(with(new Advertisement)->getTable())
                        ->whereDate('start_date', '<=', \Carbon\Carbon::now()->toDateString())->whereDate('end_date', '>=', \Carbon\Carbon::now()->toDateString())
                        ->where('status', '=', 1);

                })->where(['cms_id' => $cms->id])->get();

                return $data;
            }
        }
        if ($category_id != '0') {
            $categories = Manage_category::where('type', '=', Config::get('constants.MANAGE_CATEGORY_TYPE_ADVERTISEMENT'))->where('category_id', '=', $category_id)->get();
            $cat_array = array();
            foreach ($categories as $category) {
                $cat_array[] = $category->term_id;
            }

            $adv_list = Advertisement::whereIn('id', $cat_array)->where('status', '=', Config::get('constants.COMMON_STATUS_ACTIVE'))->get();
            return $adv_list;
        }
    }

    public static function setting($key, $default = null)
    {
        // if (is_null($key)) {
        //     return new \App\Setting\Setting();
        // }

        // if (is_array($key)) {
        //     return \App\Setting\Setting::set($key[0], $key[1]);
        // }

        // $value = \App\Setting::get($key);

        // return is_null($value) ? value($default) : $value;
    }

    public static function collegeList()
    {

        $data = DB::table('colleges')->get();
        return $data;
    }

    public static function createLog($input = array())
    {


        if(Auth::check()){

        $log_exist = \App\UserLogs::where(['viewed_by_user_id' => Auth::User()->id, "channel_id" => $input['channel_id'], "channel_type" => $input['channel_type']])->first();

        }elseif(isset($_SESSION['pragmarx/phpsession']['tracker_session']['uuid'])){
            $log_exist = \App\UserLogs::where(['uuid' =>$_SESSION['pragmarx/phpsession']['tracker_session']['uuid'], "channel_id" => $input['channel_id'], "channel_type" => $input['channel_type']])->first();


        }
        $user_log_id = 0;

        if (isset($log_exist) && $log_exist) {

            $user_log_id = $log_exist->id;
        }

        $user_log_data['channel_id'] = $input['channel_id'];
        $user_log_data['channel_type'] = $input['channel_type'];
        $user_log_data['other_channel_relation_id'] = isset($input['other_channel_relation_id']) ? $input['other_channel_relation_id'] : 0;


        $user_log_data['viewed_by_user_id'] =Auth::check() ?  Auth::User()->id : 0;
        $user_log_data['uuid'] =isset($_SESSION['pragmarx/phpsession']['tracker_session']['uuid']) ? $_SESSION['pragmarx/phpsession']['tracker_session']['uuid'] :'';

        $user_log_data['type'] = isset($input['log_type']) ? $input['log_type'] : 0;

        $userprofileview = \App\UserLogs::updateOrCreate(["id" => $user_log_id], $user_log_data);

        // $position=Location::get();

        $user_log_details_data['user_log_id'] = $userprofileview->id;

        $user_log_details_data['ip_address'] = \Request::ip();

        \App\UserLogsDetails::create($user_log_details_data);
    }

    public static function cms_data($slug)
    {
        $data = Cms::where('slug', $slug)->where('status', Config::get('constants.COMMON_STATUS_ACTIVE'))->first();
        return $data;
    }

    public static function saveDeviceToken($input, $user)
    {

        $device_token = isset($input['device_token']) ? $input['device_token'] : '';
        $device_type = isset($input['device_type']) ? $input['device_type'] : '';
        $user_id = isset($user->id) ? $user->id : 0;

        if (trim($device_token) != '' && trim($device_type) != '') {

            $user_device_token_model = new \App\UserDeviceToken();

            $user_device_token = $user_device_token_model->where('user_id', $user_id)->first();

            $uer_dive_cnt = \App\UserDeviceToken::where('user_id', '!=', $user_id)->where('device_token', $device_token)->count();

            if ($uer_dive_cnt > 0) {

                \App\UserDeviceToken::where('user_id', '!=', $user_id)->where('device_token', $device_token)->delete();
            }

            $data['device_token'] = $device_token;
            $data['device_type'] = $device_type;

            $data['user_id'] = $user_id;
            $device_token_id = isset($user_device_token->id) ? $user_device_token->id : 0;
            $ret = $user_device_token_model->updateOrcreate(['id' => $device_token_id], $data);
            return $ret;
        }
    }

    public static function totalCourse()
    {
        $totalCourses = Course::where([
            ['is_approved', '=', Config::get('constants.COMMON_STATUS_APPROVED')],
            ['status', '=', Config::get('constants.COMMON_STATUS_ACTIVE')],
        ])->count();
        return $totalCourses;
    }

    public static function totalUser()
    {

        $user_types = [Config::get('constants.USER_TYPE_STUDENT'), Config::get('constants.USER_TYPE_GUARDIAN')];
        $totalUsers = User::where([
            ['is_verified', '=', Config::get('constants.COMMON_STATUS_VERIFIED')],
            ['status', '=', Config::get('constants.COMMON_STATUS_ACTIVE')],
        ])->whereIn('role', $user_types)->count();
        return $totalUsers;
    }

    public static function totaltrainingOrganization()
    {
        $user_types = [Config::get('constants.VENDOR_TYPE_FREELANCER'), Config::get('constants.VENDOR_TYPE_TRAINING_ORGANIZATION')];
        $totalTrainingOrganization = User::where([
            ['role', '=', Config::get('constants.USER_TYPE_VENDOR')],
            ['is_verified', '=', Config::get('constants.COMMON_STATUS_VERIFIED')],
            ['is_approved', '=', Config::get('constants.COMMON_STATUS_APPROVED')],
        ])->whereIn('user_type', $user_types)->count();
        return $totalTrainingOrganization;
    }
    public static function totalCorporate()
    {

        $totalCorporates = User::where([
            ['role', '=', Config::get('constants.USER_TYPE_VENDOR')],
            ['user_type', '=', Config::get('constants.VENDOR_TYPE_CORPORATE')],
            ['is_verified', '=', Config::get('constants.COMMON_STATUS_VERIFIED')],
            ['is_approved', '=', Config::get('constants.COMMON_STATUS_APPROVED')],
        ])->count();
        return $totalCorporates;

    }

    public static function showDateTime($datetime, $format = "d-m-Y")
    {
        //  return \Carbon\Carbon::createFromFormat(strtotime($datetime))->diffForHumans();
        return \Carbon\Carbon::parse($datetime)->format($format);

        //return  Carbon::createFromFormat('d/m/Y', $datetime);

    }
    public static function showPrice($price)
    {

        return "INR " . number_format($price, 2);

    }

    public static function romanToInteger($roman)
    {

        $romans = array(
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1,
        );

        $result = 0;

        foreach ($romans as $key => $value) {
            while (strpos($roman, $key) === 0) {
                $result += $value;
                $roman = substr($roman, strlen($key));
            }
        }
        return $result;

    }
    public static function integerToRoman($number)
    {

        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;

    }

    public static function getAmountInWords($amount) {
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');
        $amountTempArray = array();
        $amountTempArray = explode('.', $amount);
        $totalAmountInRupees = $numberTransformer->toWords($amountTempArray[0]);
        $totalAmount="";
        if($amount==0){
            return " ";
        }
        if(sizeof($amountTempArray) > 1) {
            if($amountTempArray[0] > 0){
                $totalAmount = $totalAmountInRupees." Rupees ";
            }
            $totalAmountInPaise = $numberTransformer->toWords($amountTempArray[1]);
            $totalAmount .=$totalAmountInPaise." Paise Only";
        } else {
            $totalAmount = $totalAmountInRupees." Rupees Only";
        }
 
        return ucwords($totalAmount);
    }

    public static function Notifcation($token, $title, $body)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $option = $optionBuilder->build();

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => $title, 'body' => $body]);
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($token, $option,null, $data);
    }

    public static function showPriceWithGST($amount){
        $gst = Self::setting('gst');
        return number_format((float)(($amount * $gst/100) + ($amount)), 2, '.', '');
    }
    public static function numberFormat($number){
        return number_format((float)$number, 2, '.', '');
    }


}
