<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

//Use flugger exception handler to format the response correctly
//use Flugg\Responder\Exceptions\Handler as ExceptionHandler;


class Handler extends ExceptionHandler
{
    use \Flugg\Responder\Exceptions\ConvertsExceptions;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if ($exception instanceof MethodNotAllowedHttpException) // which is use method not allow exception
        {
            return redirect()->back();
        }

        //Flugger exception handling here
        if ($request->wantsJson()) {
            if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
                return responder()->error(401, 'AuthenticationException')->data(['error_description' => 'authentication failed for current transaction','error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]])->respond(401)->setStatusCode(200);
            }
            if ($exception instanceof \Illuminate\Auth\Access\AuthorizationException) {
                return responder()->error(401, 'AuthorizationException')->data(['error_description' => 'resource/collection not found','error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]])->respond(401)->setStatusCode(200);
            } 
            if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
            {
                return responder()->error(404, 'NotFoundHttpException')->data(['error_description' => 'resource/collection not found','error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]])->respond(404);
            } 
            if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
                return responder()->error(404, 'ModelNotFoundException')->data(['error_description' => 'requested model not found','error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]])->respond(404);
            } 
            if ($exception instanceof \Illuminate\Database\Eloquent\RelationNotFoundException) {
                return responder()->error(404, 'RelationNotFoundException')->data(['error_description' => 'requested relation not found','error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]])->respond(404);
            } 
            if ($exception instanceof \Illuminate\Validation\ValidationException) {
                return responder()->error(422, 'ValidationException')->data(['error_description' => 'validation for requested resource/collection failed','error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]])->respond(422)->setStatusCode(200);
            }

            if ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
                return responder()->error(405, 'MethodNotAllowedHttpException')->data(
                    ['error_description' => 'requested resource/collection cannot be retrieved using given method call', 
                    'error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]])->respond(405);
            }   

            if ($exception instanceof \BadMethodCallException) {
                return responder()->error(400, 'BadMethodCallException')->data(['error_description' => 'error occured in retrieval of requested resource/collection', 'error_details' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTrace()
                ]
            ])->respond(400);

            }
            if ($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
                return responder()->error(500, 'HttpException')->data(['error_description' => 'server error occured', 'error_details' => [
                    'message' => $exception->getMessage(),
                        'file' => $exception->getFile(),
                        'line' => $exception->getLine(),
                        'trace' => $exception->getTrace()
                    ]
                ])->respond(500);
            }
            if ($exception instanceof \Exception) {
                return responder()->error(500, 'Exception')->data(['error_description' => 'server error', 'error_details' => [
                    'message' => $exception->getMessage(),
                        'file' => $exception->getFile(),
                        'line' => $exception->getLine(),
                        'trace' => $exception->getTrace()
                    ]
                ])->respond(500);
            }
            return responder()->error(500, 'Exception')->data(['error_description' => 'server error', 'error_details' => [
                    'message' => $exception->getMessage(),
                        'file' => $exception->getFile(),
                        'line' => $exception->getLine(),
                        'trace' => $exception->getTrace()
                    ]
                ])->respond(500);
            $this->convertDefaultException($exception); 
        }        
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            /** return response()->json(['error' => 'Unauthenticated.'], 401); */
            return responder()->error('unauthenticated')->data(['error_description' => 'wrong user credentails'])->respond(401)->setStatusCode(200);
        }
        $guard = array_get($exception->guards(), 0);

        
        switch ($guard) {
          // case 'vendors':
          //   $login = 'vendors.login';
          //   break;
          default:
            $login = 'login';
            break;
        }

        return redirect()->guest(route('expire'));

        return $request->expectsJson()
                    ? response()->json(['message' => $exception->getMessage()], 401)
                    : redirect('/login');
    }    
}
