<?php
namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Matches  extends Model
{
    

    protected $table = 'matches';
    //protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['series_id', 'team1_id', 'team2_id','match_datetime','status','winner_team_id','result','updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


  
    public function team1()
    {
      return $this->hasOne('App\Teams','id','team1_id');
    }

    public function team2()
    {
      return $this->hasOne('App\Teams','id','team2_id');
    }

    public function series()
    {
      return $this->hasOne('App\Series','id','series_id');
    }

    public function totalmatches()
    {
      return $this->hasMany('App\matches','series_id','series_id');
    }





}
