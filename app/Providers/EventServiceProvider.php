<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        // //revoke old tokens when new access tokens are created
        // 'Laravel\Passport\Events\AccessTokenCreated' => [
        //     'App\Listeners\RevokeOldTokens',
        // ],

        // //prune old tokens when new refresh tokens get created
        // 'Laravel\Passport\Events\RefreshTokenCreated' => [
        //     'App\Listeners\PruneOldTokens',
        // ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
