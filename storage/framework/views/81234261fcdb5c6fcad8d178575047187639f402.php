<?php $__env->startSection('content'); ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content content-no-padd">
        <div class="banner">
            <img src="<?php echo e(asset('images/cricket-banner.jpg')); ?>" alt="Cricket APP" />
        </div>
        <div class="content">
            <h2 class="page-header">IPL 2019 Teams</h2>
            <div>

                <?php 
                if(isset($teams) && count($teams)>0){
                foreach($teams as $team){?>
                    <div class="col-sm-12 col-md-6">
                        <div class="box box-solid team-box">
                            <div class="box-body">
                                <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                                    <?php echo e($team->team_name); ?>

                                </h4>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:;" class="ad-click-event">
                                            <?php if($team->image!=''){ ?>
                                                    <img src="<?php echo e(asset($team->image)); ?>" alt="team logo" class="media-object" style="height: 125px;  width:150px; border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <div class="clearfix">                                        
                                            <p class='text-justify'><?php echo e(substr($team->description,0,200)); ?></p>
                                            <p class="pull-right">
                                                <a href="<?php echo e(url('team-details').'/'.$team->id); ?>" class="btn btn-success btn-sm">
                                                    VIEW TEAM DETAILS
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                <?php }

                }?>
                    <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.web.layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>