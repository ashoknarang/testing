<?php echo $__env->make('layouts.admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php if(Auth::user()->role==Config::get('constants.USER_TYPE_ANALYSIS_MANAGER') || Auth::user()->role==Config::get('constants.USER_TYPE_CALL_CENTER')  ): ?>	
<?php echo $__env->make('layouts.analytics.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php else: ?>
<?php echo $__env->make('layouts.admin.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>
<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->make('layouts.admin.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


