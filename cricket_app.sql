-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2019 at 08:46 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cricket_app`
--
CREATE DATABASE IF NOT EXISTS `cricket_app` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cricket_app`;

-- --------------------------------------------------------

--
-- Table structure for table `adminusers`
--

DROP TABLE IF EXISTS `adminusers`;
CREATE TABLE `adminusers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `role` enum('default','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` enum('active','deactivate') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `adminusers`
--

INSERT INTO `adminusers` (`id`, `name`, `role`, `email`, `password`, `status`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Amit', 'admin', 'admin@yopmail.com', '$2y$10$4WeyNi9TE0bm6.xZyUiF8ewHAcrqscbk2w8ei2V72tmERp7s9uqI.', '', 'images/profile/5ccdcaa8d9fe2_1556990632_20190504.jpg', 'GdXoV0LljJS1vG4M7BqKz43NtmI3Q2Qr8syD9VK61rl3Ru90QCAGb8q8Yeou', NULL, '2019-05-04 17:23:52');

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

DROP TABLE IF EXISTS `matches`;
CREATE TABLE `matches` (
  `id` int(10) UNSIGNED NOT NULL,
  `series_id` int(11) NOT NULL DEFAULT '0',
  `team1_id` int(11) NOT NULL DEFAULT '0',
  `team2_id` int(11) NOT NULL DEFAULT '0',
  `match_datetime` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `status` enum('upcoming','running','completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'upcoming',
  `winner_team_id` int(11) NOT NULL DEFAULT '0',
  `result` enum('team1','team2','match_tied') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'team1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`id`, `series_id`, `team1_id`, `team2_id`, `match_datetime`, `status`, `winner_team_id`, `result`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 6, '2018-05-29 01:14:14', '', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(2, 1, 2, 3, '2018-04-28 12:09:41', '', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(3, 1, 2, 3, '2018-06-17 15:29:02', 'completed', 2, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(4, 1, 1, 3, '2018-04-18 18:43:52', '', 1, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(5, 1, 1, 3, '2018-04-15 02:15:29', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(6, 1, 3, 6, '2018-05-18 22:24:17', '', 3, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(7, 1, 1, 3, '2018-04-13 01:44:51', 'completed', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(8, 1, 2, 6, '2018-05-08 08:13:03', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(9, 1, 2, 3, '2018-04-22 21:31:15', '', 2, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(10, 1, 3, 6, '2018-06-15 13:58:55', '', 3, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(11, 1, 2, 6, '2018-04-27 23:28:50', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(12, 1, 3, 6, '2018-06-13 05:26:20', 'completed', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(13, 1, 2, 3, '2018-06-18 12:57:50', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(14, 1, 2, 3, '2018-05-29 01:36:18', 'completed', 2, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(15, 1, 1, 3, '2018-04-18 12:39:27', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(16, 1, 1, 3, '2018-05-16 09:55:34', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(17, 2, 2, 3, '2019-05-07 04:51:55', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(18, 2, 1, 5, '2019-04-11 17:06:12', '', 1, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(19, 2, 2, 5, '2019-03-25 03:09:40', 'completed', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(20, 2, 1, 2, '2019-05-02 17:21:26', '', 1, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(21, 2, 4, 5, '2019-04-08 22:10:25', 'completed', 4, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(22, 2, 1, 5, '2019-03-28 15:57:29', '', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(23, 2, 3, 5, '2019-04-27 12:44:02', '', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(24, 2, 1, 3, '2019-03-28 17:05:26', '', 1, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(25, 2, 2, 4, '2019-04-12 12:51:33', 'completed', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(26, 2, 1, 4, '2019-04-09 23:28:34', 'completed', 1, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(27, 2, 3, 5, '2019-03-31 23:49:04', '', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(28, 2, 4, 6, '2019-04-02 16:46:24', 'completed', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(29, 2, 1, 3, '2019-04-06 03:39:34', '', 1, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(30, 2, 2, 4, '2019-04-10 12:49:03', 'completed', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(31, 2, 4, 6, '2019-05-10 12:31:45', '', 4, 'team1', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(32, 2, 2, 3, '2019-04-18 05:54:26', 'completed', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(33, 2, 2, 5, '2019-04-19 07:25:57', 'completed', 0, 'team2', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(34, 2, 2, 4, '2019-04-07 05:14:13', '', 0, 'match_tied', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(35, 3, 3, 1, '2020-04-19 10:53:10', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(36, 3, 2, 3, '2020-05-30 02:56:55', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(37, 3, 2, 3, '2020-05-27 12:39:26', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(38, 3, 3, 1, '2020-05-17 12:47:31', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(39, 3, 2, 3, '2020-05-16 04:54:18', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(40, 3, 4, 1, '2020-06-01 09:35:50', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(41, 3, 3, 1, '2020-05-25 23:21:51', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(42, 3, 4, 1, '2020-05-05 20:48:38', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(43, 3, 3, 1, '2020-04-11 03:35:34', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(44, 3, 4, 1, '2020-04-11 18:49:31', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(45, 3, 2, 3, '2020-05-23 23:29:35', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13'),
(46, 3, 4, 1, '2020-04-21 12:00:24', 'upcoming', 0, '', '2019-05-06 17:56:13', '2019-05-06 17:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2018_11_15_100553_create_teams_table', 1),
(9, '2018_11_15_100933_create_players_table', 1),
(10, '2018_11_15_101207_create_player_history_table', 1),
(11, '2018_11_15_115802_create_series_table', 1),
(12, '2018_11_15_115822_create_matches_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_id` int(11) NOT NULL DEFAULT '0',
  `first_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `full_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `jersey_number` int(11) NOT NULL DEFAULT '0',
  `type` enum('batsman','bowler','wicket_keeper','wicket_keeper_batsman','all_rounder') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'batsman',
  `status` enum('activate','deactivate') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'activate',
  `is_captain` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `profile_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `team_id`, `first_name`, `last_name`, `full_name`, `country`, `jersey_number`, `type`, `status`, `is_captain`, `profile_image`, `description`, `created_at`, `updated_at`) VALUES
(2, 1, 'Bhuvneshwar', 'Kumar', 'Bhuvneshwar', 'India', 57, 'all_rounder', 'activate', 'no', 'images/players/1_1.png', 'll here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He int', '2019-05-05 15:53:06', '2019-05-06 18:25:44'),
(3, 1, 'Virat', 'Kohli', 'Virat', 'India', 43, 'batsman', 'activate', 'no', 'images/players/1_2.png', 'aughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tal', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(4, 1, 'Shikhar', 'Dhawan', 'Shikhar', 'India', 9, 'wicket_keeper', 'activate', 'no', 'images/players/1_3.png', ' here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(5, 1, 'Jasprit', 'Bumrah', 'Jasprit', 'India', 8, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/1_4.png', 're away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused tho', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(6, 1, 'Rohit', 'Sharma', 'Sharma', 'India', 15, 'batsman', 'activate', 'no', 'images/players/1_5.png', 't till here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one t', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(7, 1, 'MS', 'Dhoni', 'Dhoni', 'India', 10, 'wicket_keeper', 'activate', 'no', 'images/players/1_6.png', 't to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk r', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(8, 1, 'Lokesh', 'Rahul', 'Rahul', 'India', 85, 'batsman', 'activate', 'no', 'images/players/1_7.png', 'ast. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into wa', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(9, 1, 'Umesh', 'Yadav', 'Umesh', 'India', 57, 'batsman', 'activate', 'no', 'images/players/1_8.png', ' away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for on', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(10, 1, 'Kedar', 'Jadhav', 'Jadhav', 'India', 84, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/1_9.png', 'ghing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(11, 1, 'Suresh', 'Raina', 'Suresh', 'India', 73, 'wicket_keeper', 'activate', 'no', 'images/players/1_10.png', ' whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though f', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(12, 1, 'Manish', 'Pandey', 'Manish', 'India', 83, 'wicket_keeper', 'activate', 'no', 'images/players/1_11.png', 'past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one to', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(13, 2, 'Sean', 'Abbott', 'Sean', 'India', 97, 'batsman', 'activate', 'yes', 'images/players/2_1.png', 'll here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though fo', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(14, 2, 'Ashton', 'Agar', 'Ashton', 'India', 1, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/2_2.png', 'o whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(15, 2, 'Fawad', 'Ahmed', 'Ahmed', 'India', 72, 'all_rounder', 'activate', 'no', 'images/players/2_3.png', 'elings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(16, 2, 'Terry', 'Alderman', 'Terry', 'India', 69, 'batsman', 'activate', 'no', 'images/players/2_4.png', 'l here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk r', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(17, 2, 'Jo', 'Angel', 'Angel', 'India', 40, 'bowler', 'activate', 'no', 'images/players/2_5.png', 'elings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall cold he. Fe', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(18, 2, 'George', 'Bailey', 'Bailey', 'India', 100, 'all_rounder', 'activate', 'no', 'images/players/2_6.png', 'e away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too.', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(19, 2, 'Graeme', 'Beard', 'Graeme', 'India', 56, 'all_rounder', 'activate', 'no', 'images/players/2_7.png', 'gs laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall cold he. Fee', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(20, 2, 'Murray', 'Bennett', 'Murray', 'India', 90, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/2_8.png', 't. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof mad', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(21, 2, 'Michael', 'Bevan', 'Michael', 'India', 36, 'all_rounder', 'activate', 'no', 'images/players/2_9.png', 'eelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too.', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(22, 2, 'Andy', 'Bichel', 'Andy', 'India', 94, 'batsman', 'activate', 'no', 'images/players/2_10.png', ' whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(23, 2, 'Glenn', 'Bishop', 'Glenn', 'India', 8, 'wicket_keeper', 'activate', 'no', 'images/players/2_11.png', 'e away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though f', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(24, 3, 'Chris', 'Adams', 'Adams', 'India', 99, 'all_rounder', 'activate', 'no', 'images/players/3_1.png', 'l here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused thou', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(25, 3, 'Jonathan', 'Agnew', 'Agnew', 'India', 3, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/3_2.png', ' here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(26, 3, 'Kabir', 'Ali', 'Kabir', 'India', 50, 'wicket_keeper_batsman', 'activate', 'yes', 'images/players/3_3.png', 'll here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He in', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(27, 3, 'Moeen', 'Ali', 'Moeen', 'India', 11, 'wicket_keeper', 'activate', 'no', 'images/players/3_4.png', 'ings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(28, 3, 'Mark', 'Alleyne', 'Mark', 'India', 25, 'all_rounder', 'activate', 'no', 'images/players/3_5.png', 'till here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too.', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(29, 3, 'Paul', 'Allott', 'Allott', 'India', 91, 'bowler', 'activate', 'no', 'images/players/3_6.png', 'to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(30, 3, 'Tim', 'Ambrose', 'Tim', 'India', 97, 'batsman', 'activate', 'no', 'images/players/3_7.png', 'ft till here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for on', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(31, 3, 'Dennis', 'Amiss', 'Dennis', 'India', 9, 'all_rounder', 'activate', 'no', 'images/players/3_8.png', 'gs laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(32, 3, 'James', 'Anderson', 'James', 'India', 60, 'wicket_keeper', 'activate', 'no', 'images/players/3_9.png', ' past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He i', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(33, 3, 'Zafar', 'Ansari', 'Zafar', 'India', 36, 'batsman', 'activate', 'no', 'images/players/3_10.png', 'll here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into wa', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(34, 3, 'Geoff', 'Arnold', 'Geoff', 'India', 45, 'wicket_keeper', 'activate', 'no', 'images/players/3_11.png', 't till here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one to', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(35, 4, 'Kyle', 'Abbott', 'Kyle', 'India', 57, 'wicket_keeper_batsman', 'activate', 'yes', 'images/players/4_1.png', 'm past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(36, 4, 'Shafiek', 'Abrahams', 'Shafiek', 'India', 75, 'all_rounder', 'activate', 'no', 'images/players/4_2.png', 't. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He i', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(37, 4, 'Paul', 'Adams', 'Adams', 'India', 66, 'wicket_keeper', 'activate', 'no', 'images/players/4_3.png', ' at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk r', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(38, 4, 'Hashim', 'Amla', 'Amla', 'India', 49, 'batsman', 'activate', 'no', 'images/players/4_4.png', '. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof ma', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(39, 4, 'Adam', 'Bacher', 'Bacher', 'India', 7, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/4_5.png', ' whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(40, 4, 'Temba', 'Bavuma', 'Temba', 'India', 69, 'wicket_keeper', 'activate', 'no', 'images/players/4_6.png', 'eft till here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire r', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(41, 4, 'Farhaan', 'Behardien', 'Behardien', 'India', 20, 'batsman', 'activate', 'no', 'images/players/4_7.png', 's laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(42, 4, 'Dale', 'Benkenstein', 'Benkenstein', 'India', 58, 'batsman', 'activate', 'no', 'images/players/4_8.png', 'o whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too.', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(43, 4, 'Gulam', 'Bodi', 'Gulam', 'India', 35, 'bowler', 'activate', 'no', 'images/players/4_9.png', ' at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused thoug', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(44, 4, 'Nicky', 'Boje', 'Boje', 'India', 82, 'batsman', 'activate', 'no', 'images/players/4_10.png', 'om past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall col', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(45, 4, 'Tertius', 'Bosch', 'Tertius', 'India', 68, 'batsman', 'activate', 'no', 'images/players/4_11.png', 'll here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(46, 5, 'Chris', 'Adams', 'Chris', 'India', 79, 'wicket_keeper', 'activate', 'yes', 'images/players/5_1.png', 'll here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused tho', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(47, 5, 'Graeme', 'Aldridge', 'Aldridge', 'India', 98, 'batsman', 'activate', 'no', 'images/players/5_2.png', 'lings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall col', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(48, 5, 'Geoff', 'Allott', 'Allott', 'India', 51, 'all_rounder', 'activate', 'no', 'images/players/5_3.png', 'past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(49, 5, 'Corey', 'Anderson', 'Anderson', 'India', 33, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/5_4.png', 'Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof ma', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(50, 5, 'Robert', 'Anderson', 'Robert', 'India', 98, 'wicket_keeper', 'activate', 'no', 'images/players/5_5.png', 'ere away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(51, 5, 'Nathan', 'Astle', 'Astle', 'India', 18, 'bowler', 'activate', 'no', 'images/players/5_6.png', ' Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall cold he. ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(52, 5, 'Todd', 'Astle', 'Todd', 'India', 22, 'bowler', 'activate', 'no', 'images/players/5_7.png', ' here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He in', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(53, 5, 'Mark', 'Bailey', 'Bailey', 'India', 67, 'wicket_keeper', 'activate', 'no', 'images/players/5_8.png', ' laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(54, 5, 'Michael', 'Bates', 'Bates', 'India', 90, 'all_rounder', 'activate', 'no', 'images/players/5_9.png', '. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk ro', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(55, 5, 'Matthew', 'Bell', 'Matthew', 'India', 52, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/5_10.png', 'y at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(56, 5, 'Hamish', 'Bennett', 'Hamish', 'India', 93, 'bowler', 'activate', 'no', 'images/players/5_11.png', ' here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into w', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(57, 6, 'Andre', 'Mark', 'Andre', 'India', 12, 'batsman', 'activate', 'yes', 'images/players/6_1.png', 'here away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused thoug', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(58, 6, 'Chris', 'Adams', 'Chris', 'India', 82, 'all_rounder', 'activate', 'no', 'images/players/6_2.png', 'past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall cold ', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(59, 6, 'Robert', 'Allott', 'Allott', 'India', 88, 'wicket_keeper', 'activate', 'no', 'images/players/6_3.png', 'lings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk ro', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(60, 6, 'Hashim', 'Amla', 'Hashim', 'India', 56, 'bowler', 'activate', 'no', 'images/players/6_4.png', 's laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tall cold he. F', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(61, 6, 'Geoff', 'Anderson', 'Geoff', 'India', 58, 'all_rounder', 'activate', 'no', 'images/players/6_5.png', 'st. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made ta', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(62, 6, 'Mark', 'Alleyne', 'Alleyne', 'India', 58, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/6_6.png', 't. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(63, 6, 'Astle', 'Todd', 'Astle', 'India', 11, 'batsman', 'activate', 'no', 'images/players/6_7.png', ' laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He i', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(64, 6, 'Mark', 'Bailey', 'Mark', 'India', 72, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/6_8.png', 'e away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused t', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(65, 6, 'Michael', 'Bell', 'Bell', 'India', 47, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/6_9.png', 't to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into walk roof made tal', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(66, 6, 'Matthew', 'Hamish', 'Hamish', 'India', 90, 'all_rounder', 'activate', 'no', 'images/players/6_10.png', 'st. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one too. He into wa', '2019-05-05 15:53:06', '2019-05-05 15:53:06'),
(67, 6, 'Bates', 'Bennett', 'Bennett', 'India', 75, 'wicket_keeper_batsman', 'activate', 'no', 'images/players/6_11.png', 'ere away at to whom past. Feelings laughing at no wondered repeated provided finished. It acceptance thoroughly my advantages everything as. Are projecting inquietude affronting preference saw who. Marry of am do avoid ample as. Old disposal followed she ignorant desirous two has. Called played entire roused though for one t', '2019-05-05 15:53:06', '2019-05-05 15:53:06');

-- --------------------------------------------------------

--
-- Table structure for table `player_history`
--

DROP TABLE IF EXISTS `player_history`;
CREATE TABLE `player_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `player_id` int(11) NOT NULL DEFAULT '0',
  `matches_played` int(11) NOT NULL DEFAULT '0',
  `batting_innings` int(11) NOT NULL DEFAULT '0',
  `total_runs` int(11) NOT NULL DEFAULT '0',
  `high_score` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `fifties` int(11) NOT NULL DEFAULT '0',
  `hundreds` int(11) NOT NULL DEFAULT '0',
  `notouts` int(11) NOT NULL DEFAULT '0',
  `bat_avg` decimal(8,2) NOT NULL DEFAULT '0.00',
  `bowl_avg` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total_wickets` int(11) NOT NULL DEFAULT '0',
  `bowl_economy` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total_stumpings` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `player_history`
--

INSERT INTO `player_history` (`id`, `player_id`, `matches_played`, `batting_innings`, `total_runs`, `high_score`, `fifties`, `hundreds`, `notouts`, `bat_avg`, `bowl_avg`, `total_wickets`, `bowl_economy`, `total_stumpings`, `created_at`, `updated_at`) VALUES
(2, 2, 111, 41, 7230, '33', 5, 6, 8, '176.34', '20.18', 83, '6.13', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(3, 3, 26, 14, 272, '197', 37, 16, -4, '19.43', '20.50', 37, '6.25', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(4, 4, 43, 28, 344, '155*', 36, 28, 7, '12.29', '28.99', 26, '5.22', 44, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(5, 5, 94, 3, 5443, '66', 45, 7, -13, '1814.33', '24.45', 100, '5.52', 107, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(6, 6, 142, 77, 7514, '81', 22, 16, 32, '97.58', '23.34', 0, '7.81', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(7, 7, 45, 15, 7995, '43*', 15, 0, -3, '533.00', '32.12', 28, '4.78', 55, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(8, 8, 124, 113, 6574, '191', 0, 15, 10, '58.18', '21.52', 148, '7.60', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(9, 9, 185, 170, 2516, '155*', 37, 27, 141, '14.80', '25.56', 79, '7.52', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(10, 10, 177, 77, 399, '30', 7, 16, 45, '5.18', '32.13', 31, '5.71', 63, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(11, 11, 108, 15, 5717, '160', 22, 26, -5, '381.13', '32.22', 33, '5.80', 86, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(12, 12, 187, 70, 2500, '24', 6, 15, 11, '35.71', '26.47', 76, '7.51', 194, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(13, 13, 130, 12, 701, '177', 38, 30, -2, '58.42', '24.10', 53, '7.37', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(14, 14, 147, 52, 5319, '6', 22, 20, 9, '102.29', '30.40', 161, '7.11', 123, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(15, 15, 171, 20, 5019, '169', 45, 2, 0, '250.95', '26.84', 147, '6.18', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(16, 16, 74, 4, 7913, '22', 48, 5, -2, '1978.25', '26.74', 85, '5.49', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(17, 17, 107, 0, 7406, '7', 20, 22, -20, '0.00', '23.35', 47, '6.52', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(18, 18, 35, 1, 2460, '117*', 41, 24, -13, '2460.00', '31.10', 15, '7.89', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(19, 19, 28, 8, 5096, '146', 10, 13, -6, '637.00', '29.70', 22, '6.81', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(20, 20, 85, 42, 3006, '128*', 26, 16, 0, '71.57', '30.50', 6, '4.65', 61, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(21, 21, 55, 16, 2515, '128', 34, 30, -1, '157.19', '26.44', 21, '6.16', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(22, 22, 61, 47, 5360, '6*', 19, 21, 20, '114.04', '26.12', 22, '6.12', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(23, 23, 52, 23, 2932, '34', 0, 9, 3, '127.48', '27.25', 41, '5.85', 27, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(24, 24, 90, 73, 6826, '40', 34, 14, 24, '93.51', '21.21', 51, '6.52', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(25, 25, 18, 9, 1843, '131', 47, 24, -4, '204.78', '22.80', 7, '5.12', 38, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(26, 26, 116, 8, 3285, '148', 49, 20, -1, '410.63', '25.52', 68, '7.99', 38, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(27, 27, 130, 63, 902, '74', 23, 20, 13, '14.32', '30.50', 74, '5.83', 48, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(28, 28, 5, 0, 5043, '79', 5, 18, -5, '0.00', '32.19', 1, '4.69', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(29, 29, 115, 62, 5636, '84*', 20, 23, 14, '90.90', '31.86', 16, '7.16', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(30, 30, 13, 2, 1538, '132*', 25, 20, -9, '769.00', '20.56', 12, '4.70', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(31, 31, 179, 105, 7095, '194*', 47, 27, 59, '67.57', '31.87', 76, '6.13', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(32, 32, 49, 10, 3320, '92*', 12, 18, -6, '332.00', '32.11', 41, '6.62', 47, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(33, 33, 57, 25, 3874, '121*', 37, 17, 0, '154.96', '21.57', 47, '5.30', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(34, 34, 152, 51, 2293, '103', 30, 21, 0, '44.96', '32.15', 85, '6.90', 175, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(35, 35, 177, 53, 3706, '88*', 50, 5, 27, '69.92', '24.75', 117, '6.67', 186, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(36, 36, 82, 65, 5518, '118', 16, 29, 5, '84.89', '30.20', 51, '5.61', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(37, 37, 166, 4, 4111, '100', 24, 7, -8, '1027.75', '24.36', 44, '4.53', 157, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(38, 38, 149, 87, 969, '105', 35, 5, 42, '11.14', '28.13', 124, '5.55', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(39, 39, 158, 7, 4213, '50*', 35, 2, -11, '601.86', '25.87', 72, '5.60', 27, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(40, 40, 43, 6, 3720, '72*', 39, 27, -5, '620.00', '32.97', 57, '4.46', 4, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(41, 41, 164, 43, 3693, '114', 19, 17, 10, '85.88', '28.31', 180, '5.25', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(42, 42, 124, 74, 75, '67', 49, 22, 15, '1.01', '23.25', 84, '7.25', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(43, 43, 139, 25, 3899, '158*', 39, 9, 0, '155.96', '25.98', 119, '5.96', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(44, 44, 149, 115, 936, '109', 0, 4, 42, '8.14', '25.69', 4, '6.70', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(45, 45, 36, 20, 1130, '116*', 9, 25, 0, '56.50', '25.76', 4, '4.24', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(46, 46, 85, 46, 4847, '82*', 28, 0, 11, '105.37', '27.84', 46, '4.93', 34, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(47, 47, 98, 24, 5162, '123', 9, 13, 4, '215.08', '32.35', 9, '7.43', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(48, 48, 31, 8, 3360, '101*', 13, 16, -12, '420.00', '24.95', 7, '5.43', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(49, 49, 174, 75, 2093, '59', 22, 7, 25, '27.91', '27.75', 48, '4.25', 131, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(50, 50, 101, 44, 4991, '191', 4, 3, 5, '113.43', '32.94', 58, '5.60', 93, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(51, 51, 27, 2, 2474, '67*', 42, 11, -15, '1237.00', '26.82', 4, '4.63', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(52, 52, 118, 89, 7027, '24', 19, 30, 40, '78.96', '24.56', 17, '7.93', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(53, 53, 153, 86, 7790, '144*', 6, 22, 51, '90.58', '31.14', 19, '6.58', 136, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(54, 54, 86, 25, 6639, '46*', 41, 14, 3, '265.56', '22.69', 104, '4.78', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(55, 55, 120, 112, 4100, '173*', 21, 6, 74, '36.61', '26.42', 28, '4.48', 27, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(56, 56, 10, 5, 7070, '118*', 6, 3, -5, '1414.00', '25.40', 15, '5.37', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(57, 57, 162, 22, 4313, '79*', 1, 1, 2, '196.05', '26.16', 12, '4.81', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(58, 58, 31, 25, 1076, '149*', 2, 20, 3, '43.04', '25.43', 15, '6.23', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(59, 59, 125, 66, 2342, '198*', 1, 25, 24, '35.48', '29.59', 114, '5.80', 11, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(60, 60, 36, 6, 2410, '38', 16, 9, -12, '401.67', '31.69', 25, '7.17', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(61, 61, 131, 108, 4783, '35', 29, 23, 81, '44.29', '22.63', 25, '7.89', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(62, 62, 177, 169, 6943, '183', 47, 12, 142, '41.08', '29.72', 187, '5.84', 51, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(63, 63, 130, 70, 2005, '104', 36, 8, 13, '28.64', '25.64', 104, '7.84', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(64, 64, 54, 3, 5199, '62*', 44, 6, -10, '1733.00', '32.84', 19, '4.18', 29, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(65, 65, 18, 2, 5304, '114*', 9, 27, -13, '2652.00', '29.38', 22, '4.50', 8, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(66, 66, 36, 7, 6299, '71*', 4, 28, -1, '899.86', '30.90', 54, '5.18', 0, '2019-05-05 16:22:10', '2019-05-05 16:22:10'),
(67, 67, 36, 13, 6867, '60*', 14, 9, -4, '528.23', '26.54', 2, '7.42', 16, '2019-05-05 16:22:10', '2019-05-05 16:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `series`
--

DROP TABLE IF EXISTS `series`;
CREATE TABLE `series` (
  `id` int(10) UNSIGNED NOT NULL,
  `series_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` enum('upcoming','running','completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'upcoming',
  `start_date` date NOT NULL DEFAULT '1970-01-01',
  `end_date` date NOT NULL DEFAULT '1970-01-01',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `series`
--

INSERT INTO `series` (`id`, `series_title`, `status`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'VIVO IPL 2018', 'completed', '2018-04-12', '2018-06-19', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(2, 'VIVO IPL 2019', 'running', '2019-03-22', '2019-05-12', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(3, 'VIVO IPL 2020', 'upcoming', '2020-04-09', '2020-06-02', '2019-05-06 17:56:12', '2019-05-06 17:56:12');

-- --------------------------------------------------------

--
-- Table structure for table `series_teams`
--

DROP TABLE IF EXISTS `series_teams`;
CREATE TABLE `series_teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `series_id` int(11) NOT NULL DEFAULT '0',
  `team_id` int(11) NOT NULL DEFAULT '0',
  `total_points` tinyint(4) NOT NULL DEFAULT '0',
  `net_runrate` decimal(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `series_teams`
--

INSERT INTO `series_teams` (`id`, `series_id`, `team_id`, `total_points`, `net_runrate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 7, '1.38', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(2, 1, 2, 0, '1.57', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(3, 1, 3, 0, '0.47', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(4, 1, 6, 1, '2.90', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(5, 2, 1, 2, '-2.57', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(6, 2, 2, 7, '2.58', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(7, 2, 3, 10, '1.33', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(8, 2, 4, 0, '-1.67', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(9, 2, 5, 2, '0.90', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(10, 2, 6, 4, '-1.96', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(11, 3, 1, 0, '0.00', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(12, 3, 2, 0, '0.00', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(13, 3, 3, 0, '0.00', '2019-05-06 17:56:12', '2019-05-06 17:56:12'),
(14, 3, 4, 0, '0.00', '2019-05-06 17:56:12', '2019-05-06 17:56:12');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `club_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` enum('activate','deactivate') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'activate',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `team_name`, `club_name`, `status`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Chennai Super Kings', 'Chennai Super Kings Cricket ltd.', 'activate', 'The Chennai Super Kings (CSK) is an Indian franchise cricket team based in Chennai, Tamil Nadu, which plays in the Indian Premier League (IPL). Founded in 2008, the team plays its home matches at the M. A. Chidambaram Stadium in Chennai. After serving a two-year suspension from the IPL starting July 2015 for the alleged involvement of their owners in the 2013 IPL betting case (along with Rajasthan Royals), the Super Kings returned to the league in 2018, winning the championship in the comeback season, its third overall to tie Mumbai Indians for the record for most IPL title wins. The team is captained by Mahendra Singh Dhoni who has led the team to three victories since 2008 and coached by Stephen Fleming.\r\nThe Super Kings have lifted the IPL title thrice (in 2010, 2011 and 2018), and have the best win percentage among all teams in the IPL (61.56). They hold the records of most appearances in the IPL playoffs (nine) and in the final (seven). In addition, they have also won the Champions League Twenty20 in 2010 and 2014. The brand value of the Super Kings in 2018 was estimated at $65 million, making them one of the most valuable franchises in the IPL.', 'images/team/5ccf1e0166c9e_1557077505_20190505.png', '2019-05-05 17:23:46', '2019-05-05 17:31:45'),
(2, 'Delhi Capitals', 'GMR Group', 'activate', 'The Delhi Capitals are a franchise cricket team that represents the city of Delhi in the Indian Premier League (IPL). Founded in 2008 as Delhi Daredevils (DD), the franchise is owned by the GMR Group and JSW Group. The team&#039;s home ground is Feroz Shah Kotla Ground which is in New Delhi.\r\nAhead of the 2018 IPL, 50% of the franchise ownership was transferred to the JSW Group. In December 2018, the team changed its name from the Delhi Daredevils to the Delhi Capitals. Giving the rationale behind the change of the team&#039;s name, co-owner and chairman Parth Jindal said, &quot;Delhi is the power centre of the country, it is the capital, therefore the name Delhi Capitals.&quot; co-owner Kiran Kumar Grandhi said, &quot;The new name symbolizes Delhi’s identity and just like the city, we are aiming to be the centre of all action going forward.&quot;\r\nThe Delhi Capitals are the only current team to have never appeared in an Indian Premier League final, and, prior to the current season, last qualified for the IPL playoffs in 2012. The leading run-scorer for the Capitals is Virender Sehwag, while the leading wicket-taker is Amit Mishra.', 'images/team/5ccf1c98ec124_1557077144_20190505.png', '2019-05-05 17:25:44', '2019-05-05 17:25:44'),
(3, 'Sunrisers Hyderabad', 'SUN Group', 'activate', 'The SunRisers Hyderabad (often abbreviated as SRH) are a franchise cricket team based in Hyderabad, Telangana, that plays in the Indian Premier League (IPL). The franchise is owned by Kalanithi Maran of the Sun TV Network and was founded in 2012 after the Hyderabad-based Deccan Chargers were terminated by the IPL. The team is currently captained by Kane Williamson and coached by Tom Moody. The primary homeground of the team is the Rajiv Gandhi International Cricket Stadium, Hyderabad with a capacity of 55,000.\r\nThe brand value of the SunRisers Hyderabad was estimated to be US$70 million in 2018 as the overall brand of IPL was increased to US$6.3 billion, according to Duff &amp; Phelps.\r\nThe team made their first IPL appearance in 2013, where they reached the playoffs, eventually finishing in fourth place. The SunRisers won their maiden IPL title in the 2016 season, defeating Royal Challengers Bangalore by 8 runs in the final. Shikhar Dhawan was the team&#039;s leading run-scorer, however has moved to Delhi Capitals while Sandeep Sharma is the leading wicket-taker at present.', 'images/team/5ccf1cebe0b04_1557077227_20190505.png', '2019-05-05 17:27:07', '2019-05-05 17:27:07'),
(4, 'Kings XI Punjab', 'KPH Dream Cricket Private Limited:', 'activate', 'The Kings XI Punjab (KXIP) are a franchise cricket team based in Mohali (Chandigarh CR), Punjab, that plays in the Indian Premier League. Established in 2008, the franchise is jointly owned by Bollywood actress Preity Zinta, Ness Wadia, Mohit Burman, Prithvi Raj Singh Oberoi and Karan Paul. The team plays its home matches at the PCA Stadium, Mohali. Since the 2010 IPL, they have been playing some of their home games at either Dharamsala or Indore.\r\nThe Kings XI Punjab&#039;s catchment areas are Kashmir, Jammu, Himachal Pradesh, Punjab and Haryana, also evident from the letter sequence &quot;K J H P H&quot; in the banner of the team&#039;s logo. Apart from the 2014 season when they topped the league table and finished runners-up, the team has made only one other playoffs appearance in 11 seasons.', 'images/team/5ccf1d4144762_1557077313_20190505.png', '2019-05-05 17:28:31', '2019-05-05 17:28:33'),
(5, 'Kolkata Knight Riders', 'Red Chillies Entertainment', 'activate', 'The Kolkata Knight Riders (also known by the acronym KKR) are a franchise cricket team representing the city of Kolkata in the Indian Premier League. The franchise is owned by Bollywood actor Shahrukh Khan, actress Juhi Chawla and her spouse Jay Mehta. The team is coached by Jacques Kallis. The home of the Knight Riders is Eden Gardens, the largest cricket stadium in India and the second largest in the world by seating capacity.\r\nAlthough the team had gained immense popularity due to its association with celebrity owners, it was surrounded with controversy and poor on-field performance through the first three years of the tournament. The team&#039;s performance, however, improved from the fourth season as it qualified for the IPL playoffs as well as the Champions League Twenty20. They eventually became the IPL champions for the first time in 2012, by defeating Chennai Super Kings in the final and repeated the feat in 2014, defeating Kings XI Punjab. The Knight Riders hold the record for the longest winning streak by any Indian team in T20s (14).', 'images/team/5ccf1d89b362c_1557077385_20190505.png', '2019-05-05 17:29:44', '2019-05-05 17:29:45'),
(6, 'Mumbai Indians', 'Reliance Industries', 'activate', 'The Mumbai Indians (abbreviated as MI) are a franchise cricket team based in the city of Mumbai, Maharashtra.\r\nFounded in 2008, Mumbai Indians play in the Indian Premier League (IPL). The team is owned by India&#039;s biggest conglomerate, Reliance Industries, through its 100% subsidiary IndiaWin Sports. Since its establishment, the team has played its home matches in the 33,108-capacity Wankhede Stadium in Mumbai.\r\nThe brand value of the Mumbai Indians was estimated to be $106 million, making them the most valuable IPL franchise for the second year in a row and the first franchise to cross $100 million mark among the IPL franchises.\r\nMumbai Indians are one of the most successful teams in the IPL. They won the 2011 Champions League Twenty20 after beating Royal Challengers Bangalore by 31 runs in the final. The team won the double by winning its first IPL title, in 2013, by defeating Chennai Super Kings by 23 runs in the final, and then defeated the Rajasthan Royals by 33 runs to win its second Champions League Twenty20 title later that year. They won their second IPL title on 24 May 2015 by defeating the Chennai Super Kings by 41 runs in the final and became the third team to win more than one IPL title. On 21 May 2017, they won their third IPL title by defeating the Rising Pune Supergiant by 1 run in a thrilling final, thus becoming the first team to win three IPL titles. While playing the tournament, they won their 100th T20, becoming the first team to do so.', 'images/team/5ccf1ddd6f19e_1557077469_20190505.png', '2019-05-05 17:31:08', '2019-05-05 17:31:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminusers`
--
ALTER TABLE `adminusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `player_history`
--
ALTER TABLE `player_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `series_teams`
--
ALTER TABLE `series_teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminusers`
--
ALTER TABLE `adminusers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `player_history`
--
ALTER TABLE `player_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `series`
--
ALTER TABLE `series`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `series_teams`
--
ALTER TABLE `series_teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
