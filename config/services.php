<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

  

   
    'user_status' => [
        '' => array("title"=>"Status"),
        'activate' => array("title"=>"Active"),
        'deactivate' => array("title"=>"Deactive"),
    ],


    'player_type' => [
        '' => array("title"=>"type"),
        'batsman' => array("title"=>"Batsman"),
        'bowler' => array("title"=>"Bowler"),
        'wicket_keeper' => array("title"=>"Wicket keeper"),
        'wicket_keeper_batsman' => array("title"=>"Wicket keeper and batsman"),
        'all_rounder' => array("title"=>"All rounder"),
        ],
    'series_status' => [
        '' => array("title"=>"Status"),
        'upcoming' => array("title"=>"Upcoming"),
        'running' => array("title"=>"Running"),
        'completed' => array("title"=>"Completed"),
        ],

   
    'common_status' => [
       '' => array("title"=>"Status"),
        '1' => array("title"=>"Active"),
        '2' => array("title"=>"In-active"),
    ],

   
];
